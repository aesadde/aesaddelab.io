---
title: Awesome & Useful Links
style: default.css
---

<div id="links-content" class="mainContent">

Over the years I've been collecting concepts, ideas, books, articles and videos
that have shaped my thinking and, in some cases, changed my life. What follows
is a summary of that collection. Think about it as a "best of the best" list.

I hope that you find some insightful nuggets that spark your curiosity.

If you like it, you should subscribe to my [weekly
email](http://eepurl.com/gv6g0j), I promise, you will
like it!

<hr>
<center>
## Nassim Taleb
</center>

In the past 8 years, there's no one I've learned most from than Nassim Taleb.
His books —especially
[Antifragile](http://aesadde.xyz/notes/bibliography/2015-AntiFragile)—
completely changed the way I look at things. Taleb also taught me to prefer the
old and time-tested to the new. Remember, time is the ultimate tester.

[Nassim Taleb: my rules for
life](https://www.theguardian.com/books/2012/nov/24/nassim-taleb-antifragile-finance-interview).
At the risk of being superficial, this article from The Guardian provides a nice
introduction to Taleb's character and life.

#### [Antifragility](https://en.wikipedia.org/wiki/Antifragile)
This is the subject of my favorite book from Taleb. It might also be because it
was the first one I read and it changed my life. Taleb argues that we need a
word for the opposite of fragile which is not robust. Robust things are not
harmed by stress or disorder but don't gain anything either. Antifragile
systems, on the other hand, actually get better when they're subjected to stress
and variability.

Examples of antifragile systems:

- Commercial aviation. Every mistake, that is, every plane crash makes the
  overall system better.
- Entrepreneurship. Lots of small risk with infrequent very high rewards.
- Evolution. This is trial and error process in which every error is beneficial.
  The system learns and adapts to its environment.

**Favorite Links:**

- [A conversation between Taleb and Daniel
  Kahneman](https://www.youtube.com/watch?v=MMBclvY_EMA). My favorite
part starts [here](https://youtu.be/MMBclvY_EMA?t=2269). Taleb discusses the
necessity for antifragility.

- [Living an antifragile
  life](https://fs.blog/2014/10/an-antifragile-way-of-life/) by Farnam Street.

- [How Things Gain from Disorder. Talk at
  Stanford](https://www.youtube.com/watch?v=B2-QCv-hChY&list=PL5my1e_rQIXA8xXb7VrXFN3-mdiZ_oi42&index=3).

#### [Skin in the
game](https://en.wikipedia.org/wiki/Skin_in_the_Game_(book))

Actions, not words matter. When facing a decision or asking for advice, only
listens to those that have something to lose, that have some vested interest.
Words are cheap.

**Favorite Links:**

- [This chapter from the
  book.](https://medium.com/incerto/what-do-i-mean-by-skin-in-the-game-my-own-version-cc858dc73260)

- [Skin in the Game Talk at
  Google](https://www.youtube.com/watch?v=uv6KLbkvua8).

- [The Intelectual Yet
  Idiot](https://medium.com/incerto/the-intellectual-yet-idiot-13211e2d0577)

- [Fireside Chat: Nassim Nicholas Taleb & Naval Ravikant](https://www.youtube.com/watch?v=Da0aXfshlxM&feature=youtu.be). Two insanely smart guys discussing important ideas. This is a must watch!

#### [The Lindy Effect](https://en.wikipedia.org/wiki/Lindy_effect)

Simply put, the Lindy effect says that the life expectancy of non-perishable
items is proportional to their current age. So, a book that has been in print
for 100 years has at least another 100 to be in print. Time is the ultimate
tester. Things that have survived the test of time are better than things that
haven't. I use this concept every day to make decisions.

**Favorite Links:**

- [An Expert Called
  Lindy](https://medium.com/incerto/an-expert-called-lindy-fdb30f146eaf). This
  is Taleb's own description of the Lindy effect.

- [The winner-take-all effect in longevity](
  https://fs.blog/2012/06/the-winner-take-all-effect-in-longevity/). Another
  thoughtful article from Farnam Street.

#### [Minority
rules](https://medium.com/incerto/the-most-intolerant-wins-the-dictatorship-of-the-small-minority-3f1f83ce4e15)

This is the —very logical but controversial theory— that the most intolerant
minority in a society is the one that actually rules it, mostly because the rest
of the society is nice and making a few adjustments in their lifestyle doesn't
make any difference for them.

**Favorite Links:**

- [The dominant minority](https://en.wikipedia.org/wiki/Dominant_minority).


<hr>
<center>
## Ryan Holiday
</center>

Ryan Holiday is one of the most inspiring people to me. He's a prolific and
wonderful writer, always sharing great advice.

[Stop Chasing Quick Wins: What Ryan Holiday Wants You To Know About
Success](htm://www.marieforleo.com/2018/05/ryan-holiday-interview/?utm_campaign=coschedule&utm_source=twitter&utm_medium=marieforleo).
This interview is a very good introduction to Holiday.

[The Obstacle Is The
Way](.//notes/bibliography/2017-08-17-TheObstacleIsTheWay) is my favorite
book from him. The book is about Stoicism. Every time I am under stress I try my
best to apply the principles there.


**Favorite Links:**

- [How Do You Make Life-Changing
  Decisions?](https://ryanholiday.net/how-do-you-make-life-changing-decisions).
  Understand that your problems are probably *priviliged* people problems (this
  is especially true if you're white, male and living in the USA).

- [Your Work Is the Only Thing That
  Matters](https://humanparts.medium.com/your-work-is-the-only-thing-that-matters-26a47ccf778c)

- [Alive Time Vs. Dead Time: Which Are You
  In?](https://thoughtcatalog.com/ryan-holiday/2014/09/alive-time-vs-dead-time/)
  In every situation we have a choice: make the most out of it —see it as a
  learning opportunity— or complain about your situation, get angry and have a
  bad time.

- [Why You Do What You Do? Because You Better
  Know](https://thoughtcatalog.com/ryan-holiday/2014/10/why-do-you-do-what-you-do-because-you-better-know/).
  Think about your job, your career. Think about what you care about and
  interests you. Don't just let life pass by.

- [40 Ways To Live A Full Life (And Leave Nothing On The Table) By Age
  30](https://ryanholiday.net/40-ways-to-live-a-full-life-and-leave-nothing-on-the-table-by-age-30/amp/).
  I love this list. The best part is that Holiday has [skin in the
  game](#skin-in-the-game). He lives his advice.

<!-- <center>
## Learning
</center>

<center>
## Advice
</center>

## Inspiration -->
</div>
