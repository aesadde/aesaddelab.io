#!/usr/bin/env bash


stack build --fast -v

echo "Cleaning web server ..."
stack exec web clean
echo "Starting web server ..."
stack exec web watch
