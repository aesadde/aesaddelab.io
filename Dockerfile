FROM haskell:latest

LABEL maintainer="albertosadde@gmail.com"

WORKDIR /app

ADD . /app

RUN stack setup && stack install --only-dependencies

RUN chmod +x entrypoint.sh

CMD ["./entrypoint.sh"]
