---
title:  "Underwater what?! Underwater Rugby!"
created: 2020-20-26
tags: sports, hobbies, water
kind: article
published: 2020-02-27
author: Alberto Sadde
style: post.css
---


Let’s talk about Underwater rugby. Yes, you read that right, *Underwater rugby!*

Whenever I meet new people or join a conversation about sports, Underwater rugby makes an appearance. This is because it is my favorite sport, one that I have been practicing for more than a decade.

In case you think I'm making things up—I know that playing rugby underwater seems weird— let me assure you that UWR is a real sport It has a worldwide community and an international body that even hosts a World Cup every 4 years.

As is usual for people that hear about Underwater Rugby for the first time, at this point,  you might be thinking that UWR is either some form of Waterpolo or that it is a dangerous and very difficult sport. But it is neither. The only similarity with Waterpolo is that we play it in a pool and wear swimsuits and caps. In terms of danger, UWR is as safer than most full-contact sports. All you have to do is follow some basic rules and you're set!

So, if you  are interested in knowing more and if I’ve failed to explain and describe things correctly, here is the *definitive guide to Underwater Rugby.*

Let’s dive in!

![[Source](https://www.sfuwr.org/)](https://lh6.googleusercontent.com/xlZJU4r0-7zzqk9UKTrp8u_cPbJWKmNcNpjv2wACb1Hm16W_kw2rFn3mmCR9pcCZfR40WX5rQ83ltjIL0qlUPDIikpPw8nn8fbwdKhnhRVqoFQ1YLUevwcYuVGKyCSLdwdGTGJ4x)

---

## What is Underwater Rugby?

I didn't know until I was invited to play as a freshman at Universidad Simón Bolívar in Venezuela. A friend, who knew I Iiked to swim, invited me to a practice. I was given a mask, snorkel, a waterpolo cap and a pair of fins and got right into the action with the team. From the moment I was underwater passing a ball and going up and down I was hooked. I knew it was a sport I would enjoy for years to come.

But what is Underwater Rugby exactly?

It’s kinda like Basketball, except you’re putting a ball—filled with salted water— inside a basket that is 12 feet underwater. The keyword here is *putting.* Underwater, balls don't travel very far so the safest way to score is to actually put the ball all they in the basket.

![[Source](https://divenewzealand.co.nz/upimg/uwr_002.jpg)](https://divenewzealand.co.nz/upimg/uwr_002.jpg)

Like Basketball, it is also a team sport with unlimited player rotations. But unlike it, UWR is a full-contact sport. Players holding the ball can grab and tackle each other and fight for possession.

If at this point you are thinking about [scrums](https://en.wikipedia.org/wiki/Scrum_(rugby)),—like the ones in "land" rugby—fear not, they also exist in UWR. They usually arise on the surface when a player holding the ball goes up to get some air. But they are not as common and most teams avoid it as they are very tiring.

All this is exciting, but my favorite feature of the game is that you play in all [three dimensions!](https://www.usauwrwomen.com/) Players can move back and forth and up and down in the pool. This makes for a complex and fast-paced dynamic not possible in other sports.

Another beautiful aspect of UWR is the community around the sport. Players and clubs all over the world are very friendly and welcoming. In fact, I've had the opportunity to play in a few countries and make new friends while traveling.

And, when a few years ago I landed in California, the first thing I did was to look for local teams. Lucky for me I found  the  [San Francisco Giants Sea Bass team](https://www.facebook.com/pg/sfuwr/posts/). All my travels carrying my equipment were worth the effort.

---

## **Equipment**

As I mentioned above, good UWR equipment is hard to find. It requires a few specialized items as well as—hard to find—diving wells, or pools between 12 and 15 feet deep!

In order to play, players must wear all of the following:

- **A cap with ear protectors.** This is the same as a Waterpolo cap. It protects your ears and helps identify your teammates while underwater. Similar to Waterpolo, teams play either in white o blue and referees wear a red cap.
- **A mask and a snorkel.** Players are going up and down all over the pool and holding their breath. A good mask with a wide range of view is important. The mask should be sturdy enough so that an occasional kick won’t damage it. For the snorkel there aren't any restrictions but I prefer simple ones without valves and short tubes so that water can be expelled faster and with less effort. Also important are [non-elastic mask straps](https://www.canamuwhgear.com/CanAm-Mask-Strap-p/mask-canamstrap.htm). These help your mask stay in place and avoid getting water in while you’re wrestling for the ball!
- **A swimsuit.** Men were briefs and women full swimsuit. Avoid elastic ones, they will tear easily in a scrum. While not compulsory, rash guards have been gaining popularity and most teams wear them during tournaments. They protect the skin from the rough material of some pools’ floors.
- **A pair of fins.** Together with a decent mask, fins are the most important piece of equipment. A comfortable pair will go along way in helping you have fun underwater. Avoid scuba diving fins that are long and soft. UWR fins should be hard so they will propel you forward with just a few kicks. Most experienced players wear [carbon fins](https://finswimworld.com/product/rugby-fins-carbon/): these are very hard and require some training to get used to them but provide a lot of thrust with just a kick or two!

<center>
![My current equipment](https://lh5.googleusercontent.com/DpUTZElg8n06toBQYAsIDKkPMxhtnylEuS-im1bIXIUiLq5oWGsw59gkKW8WVwVy9rL1ZWdgy89Nr0N9ciFDFSrwrf3udbHYk_WcKNDGVCHj4b0g0SOwFsQqNpsYQRjpaNc0WHq9){width=50% height=auto}
</center>

---

## **Basic Rules and Setup**

Now that you have a better idea about the sport and are better equipped, it is time to introduce the rules of the game.

UWR is  played in a pool between 11.5 and 16 feet deep and 8.75-13 yards wide. Official games—like [World Cup games](https://www.youtube.com/watch?v=NSRmeP1td4Q)—consist of 15 minutes halves with a 5-minute break in between them. In most other tournaments, the duration of the game is decided by the organization committee and usually is between 10 to 12 minutes per half.

Teams must have a minimum of 6 players and a maximum of 15. Only 12 are allowed to play in one game: 6 players in the water with 6 substitutes. Substitutions are unlimited and can happen at any time during the game, provided they exchange in the designated exchange zones (see figure below). Usually each team ha 2 forwards, 2 defenders and 2 goalies in the water. Positions come in pair so that one player can be on the surface breathing while the other is holding their position below them.

![[Source](https://www.wikiwand.com/en/Underwater_rugby)
](https://lh3.googleusercontent.com/nIrS1vOeBy316PEuRXFgKWKg_L0StbBz4Cm-iCSFOSkNbEPyYTsgn6Gai47X3caINY9P4uyvTRSE_DGDLSOtrGTfjUe325DzKs2UsM_ZkOG7S3anAX5B92TsswcdkIUipWJO0DZf){height=auto, width=700px}


The game starts with the ball in the middle and at the bottom of the pool with both teams on their side.

Once the game starts, UWR rules are fairly simple:

- Don’t grab a player’s equipment.
- Don’t choke, kick or hit another player.
- The ball must always be below the surface of the water.

As a rule of thumb, you should avoid doing anything that might threaten the safety of the other players. Also, unsportsmanlike conduct is not allowed and usually punished heavily.

You can also read the full rules manual [here](https://www.cmas.org/document?sessionId=&fileId=5336&language=1).

I’ve mentioned that UWR is a full-contact game. This is true, but only to the extent that you or the other player are carrying the ball. When players are not holding the ball they are not allowed to grab and tackle each other.

In every game, there are three referees: one deck referee who stands outside the pool and is in charge of starting/stopping the game and ensuring player exchanges and surface play are legal, and two water referees that control the game below the surface. These two referees wear oxygen tanks.

Referees can award free throws, warning and 2-minute penalties to players. Also, penalty throws can be awarded when a goal was stopped or an illegal played was made near the goal.

If you are wondering how penalty throws work, it is best to watch a [video](https://www.youtube.com/watch?v=AZjo7BUHO2g), words can't explain how fun and hard they are!

---

## **Watching the game**

So far I've told you what the game is and how is it played. But you might be wondering "How the heck do you watch a game?" This is a very common question and the answer is that you have two options, none of which are ideal: either jump in the pool or hope for a live camera feed.

In fact, UWR is not a spectators' sport and, in my opinion, this is one of the reasons ti is not more popular. It is not easy watch.

In most local tournaments, the only way to watch a game is to jump in the pool with a snorkel and a mask and watch the game from the sidelines. This works but it requires a lot of effort. If you don't feel adventurous or if the water is too cold for you—usually the case for me— there's always the option of watching it from the surface, but mind you, it will look something like this:

![[Source](https://www.youtube.com/watch?v=skWLJZ-m6mQ&t=2s)
](https://lh5.googleusercontent.com/RYraZAilqQVgkMPjSpO1laLNo8scWExCgGkHYH1FM7uPOhHoyJVHx7EVAVjPraYjmCwM08pG8aoxbxlqk6I5htelMfR_A_P9IzlVJfncvNtRocNfnNbNtaRlOAMEpRXvx055w89d)


As you can tell from the picture, being a spectator doesn't seem like a lot of fun. Unless there is a live stream I avoid inviting friends and family to tournaments. They already think the sport is weird and I wouldn't want them to think it is also boring!

Watching international and official games is a much more pleasant experience. Thanks to waterproof cameras and fast Internet, most big tournaments set up live streams feeds so all you need to do is open a browser and head to YouTube.

---

## Let's Play!

We have covered a lot of ground so far. I've you made it this deep in the article, I hope you are as excited about Underwater Rugby as I was when I first started playing!

If you are interested in trying it out, check [this interactive map](http://uwr1.de/vereine) that tracks UWR clubs around the World. Hopefully, there's one close to you! And if you are in the Bay Area in California, please come play with us! Just send me a Tweet [\@aesadde](http://twitter.com/aesadde) or check our [website](https://sites.google.com/view/sfuwr) for more information.

See you underwater!
