---
title:  Random Fun 4
published: 07/22/19
tags: newsletter
kind:      article
author:    Alberto Sadde
style:     post.css
---

Hi All!

Some good news! You can now read the past emails directly on my [blog][blog].

### What I am reading 📚

Last week I created a SPAR! challenge to force myself to read a bit more. It has
worked so far. I am slowly regaining my habit of reading at least an hour a day.

For the challenge I picked up a fiction book, the classic 1984 by George
Orwell. I dived directly into it and couldn't stop for a day or two until
I finished it.

I knew, roughly, what the book was about: a dystopian society ruled by
a dictatorial regime (sadly not so far from what our families are living in
Venezuela). But, what I was not expecting was the plot, a love affair in the
middle of such desperation and loneliness. It was the perfect way to tell the
story and describe a not so imaginary society.

### Nerd Corner (Code For Venezuela) 🤓

Speaking of 1984, as I write this, Venezuela has suffered another major
blackout, possibly the worst yet. Not long ago, in such situations, I would
worry and get angry at the thought that I couldn't do anything to help. But,
thanks to some friends, I realized that there's always a way.

These friends had the same worries and the same thoughts so they came together
and started  [Code For Venezuela][code], an organization with the only objective
of helping Venezuela in any way possible. I eagerly joined them and, after one
Hackathon and many meetups, [there are now more than 5 ongoing
projects][projects]!

Of all of them, I've been mostly involved in two:

- [MediTweet][medi]: This is a Twitter Bot that tries to match people donating
  or offering medicines with people needing them. We also keep track of
  medicines requests and work with [Medicos Por La Salud][salud].

- [Blackouts Tracker]: This project uses the Twitter hashtag `#SinLuz`,
  [NetBlocks][net] and other sources to keep track of blackouts throughout the
  country. You can see the results [here][data].

Please help us spread the word 🎙! The more people know about these projects,
the more impact they will have and the more we can do to help!

### Also interesting 🤯

- I love data and statistics in general. This [calculator][time] helps you
  estimate how much free time you actually have!

- If you know me, you know that, for me, a trip to Costco is almost like a trip
  to Disney World. Still, [this presentation][costco] made me realize just how
  big and awesome the company is.

- If you've ever wondered how a boring machine works (a machine to drill
  tunnels), this [video][boring] is for you.


<!-- Links -->
[blog]: <http://aesadde.xyz/tag-pages/newsletter.html>
[medi]: <https://twitter.com/MediTweetVE>
[salud]: <https://twitter.com/medicosxlasalud>
[code]: <https://codeforvenezuela.org>
[projects]: <https://www.codeforvenezuela.org/projects>
[net]: <https://netblocks.org>
[data]: <https://datastudio.google.com/reporting/1SFeDmugZXrNIrHwib_I-Ehc4ypE1t-9Z/page/Ijmu?s=ovOrOMlc>
[time]: <https://erikrood.com/Posts/free_time_calc.html>
[costco]: <http://minesafetydisclosures.com/blog/2018/6/18/costco>
[boring]: <https://www.youtube.com/watch?v=qx_EjMlLgqY>
