---
title: Short reflections on 6 months of running
created: 2020-07-03
tags: habits, running
kind: article
published: 2020-07-03
author: Alberto Sadde
style: post.css
---

![](https://images.unsplash.com/photo-1577526249559-fa8064d89be3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2250&q=80)

This morning I realized I’ve been running for the past 6 months. While I’ve skipped a few days I’m still on track to accomplish the goal I set in December 2019. Back then I decided that 2020 would be my running year. I set myself the goal of running at least 5 times per week a minimum of 2 miles.

Now, 6 months into it I’m starting to see a clear pattern and a system that can be applied to other habits or goals you may have.

### **Identity**

Benjamin Hardy says, "your identity follows your behaviors", so the first thing you need to do is transform your identity.

Whatever you plan on doing, change your identity. If you don’t believe in yourself and the person you want to become, why should anyone else?

You shouldn’t set a goal and go:

“I’m doing this because I want to be a runner”.

Instead, shift your mindset:

“I’m doing this because I’m a runner, and this is what runners do every day, they run!”

It may sound cliché, but it works. I’m not a professional runner and while I still aspire to one day run like Cameron Hanes (he runs 15+ miles per day!!),  I consider myself a runner. I read books about running, documentaries, videos. I even talk about it more than I should.

### **Commit to not quit**

This is one of the most difficult parts of trying to build a new habit or achieving a new goal.

It will not be easy. There will be times when you think failure is inevitable. But you must endure.

For this, it’s best to have a system. In my 6 months of running, I’ve had many off days. Days in which I just wanted to lie in bed laze around and not go out because of the cold or rain. I know that without a system I wouldn’t have made it this far.

My system is very simple, I learned it from Haruki Murakami in his book [“What I talk about when I talk about running”](https://aesadde.xyz/books/2020-02-17-What-I-Talk-About-When-I-Talk-About-Running/):

> “Never skip two days in a row”.

Commitment to this law means that you may falter, you may slow down, but you will always be on track to hit your target.

It also works to have someone that can hold you accountable. I have my girlfriend for this. The catalyst. If I’m on the brink of throwing in the towel one day, she encourages me to go out. On most days, her urging is all I need. Once I’m out the door, the running takes care of itself.

### **Mornings count**

It has been said over and over again: *"win the day by winning your morning”.*  This sounds true, but you don’t appreciate its power until you try it for yourself for a while.

I’m not a habits expert like [James Clear](https://jamesclear.com/) or [Charles Duhigg](https://charlesduhigg.com/), but after years of self-experimentation, I’ve found that you win or lose the day in your morning.

When I started running, I would go out in the evenings after a full day of work. So late in the day it was easy to find an excuse and not run. After all, you’re tired from a full day and can trick yourself into thinking that you deserve a break.

Willpower is finite, we have plenty in the morning.

Switching from evening to morning runs was a game-changer. Even if the day gets away from me afterwards, knowing I've bagged an early win feels great.

### **Patience**

Naval says “Impatience with actions, patience with results”.

It took me a while to realize this, to be patient and enjoy the small wins.

When I started running, I expected quick results. During the first few weeks, I continued to improve my pace and my mileage, but then I hit a plateau. It took me 4 to 6 more weeks of painful running to realize that good things take time.

The point here is to understand what George Leonard has said in his book "Mastery":

> The real juice of life, whether it be sweet or bitter, is to be found not nearly
so much in the products of our efforts as in the process of living itself, in
how it feels to be alive.

I stopped counting miles and sessions. Now I only focus on enjoy every single run. Each morning I get a new chance to start afresh, to feel alive and win the day. That's all that matters.

Simply get out of the door and results will follow.

### **Enjoy**

This should be obvious. Whatever you are trying to achieve or whatever habit you’re trying to build, you should enjoy it.

But keep in mind there will be days in which you may want to stop. When this happens ensure you're not confusing a simple obstacle with displeasure and do as Steve Jobs did every morning:

> “For the past 33 years, I have looked in the mirror every morning and asked myself: ‘If today were the last day of my life, would I want to do what I am about to do today?’ And whenever the answer has been ‘No’ for too many days in a row, I know I need to change something.”

Not all goals are meant to be achieved. Once you realize you don't enjoy something anymore, stop. Focus your energy on something else.

When I started running I didn’t know if I was going to like it. For me it was a challenge because I used to hate running, I didn’t see the point of it.

But I changed my identity, I committed to the goal and had patience—some times not much to be honest. And now, I love running. I don’t even look forward to rest days, I prefer going out for a run than staying home doing nothing.

If this is what it means to be a runner, then I get to be one every day.
