---
title:  Random Fun 3
published: 07/14/19
tags: newsletter
kind:      article
author:    Alberto Sadde
style:     post.css
---

Hello folks!

This is the third email. I'm enjoying it, so will probably carry on for a while,
so if you've had fun so far, [spread the word!][up]

### What I am reading 📚

As I mentioned in a previous email, reading has probably been the best
investment of my life. But I regularly struggle to find time to read. I struggle
because I procrastinate or more likely, because I get distracted while browsing
the Internet. Luckily, there's help out there 😄. This [article][ryan] by Ryan
Holiday (one of my favourite authors by the way) and this [post][farnam] by
Shane Parrish have helped me time and time again to get back on track with my
reading.

This time, I decided to experiment a bit and be more deliberate. I created
a SPAR! [challenge][challenge] (this link will only open on your phone)to force
myself to read at least 25 pages a day for the next 2 weeks! I encourage you to
join the challenge and do some reading. If you stick to the plan you might even
make a few 💵 during the process!

For those of you who don't know, [SPAR!][spar] is a "social habit building" app.
When you join a challenge you need to prove that you did it with a short video.
If you fail to "check in" you'll be charged a small amount of money that goes to
a pot. At the end of the challenge those who had the least misses split the pot
in equal parts.

### Nerd Corner (Health Edition) 🤓

For a while now I've been experimenting with various form of cold exposure and
as of last week, I've been starting my days with a [cold shower][shower] first
thing in the morning.

Why you ask? Because cold exposure seems to provide many health benefits. See
[here][pd] and [here][fast]. It might even help in [treating
depression][depression]. Not only that, but after the struggle of convincing
yourself 🥶 to get in the shower and turn on the cold water, you'll feel
invincible 💪. After this brief moment of courage, any other obstacle during the
day will feel like a piece of cake!

### Also interesting 🤯

- A [TED][ted] talk about climate change and swimming on Mt. Everest.
- This [blogpost][coffee] describing new research about the health benefits of
  coffee ☕️.
- This [Vice][vice] documentary on Wim Hof, a pioneer on cold exposure
  techniques.

<!-- Links -->
[ryan]: <https://ryanholiday.net/how-to-read-more-a-lot-more/>
[spar]: <https://getspar.com>
[farnam]: <https://fs.blog/2013/09/finding-time-to-read/>
[ted]: <https://www.youtube.com/watch?v=QISHX5UKky0>
[pd]: <https://roguehealthandfitness.com/cold-thermogenesis-hype-and-reality/>
[coffee]:
<https://elemental.medium.com/coffee-even-a-lot-linked-to-longer-life-75d23e56f5a8>
[shower]: <http://www.cold-showers.com/about/>
[vice]: <https://www.youtube.com/watch?v=VaMjhwFE1Zw>
[depression]:
<https://www.sciencedirect.com/science/article/abs/pii/S030698770700566X>
[fast]:
<https://www.fastcompany.com/3043767/the-scientific-case-for-cold-showers>
[up]: <http://eepurl.com/gv6g0j>
[challenge]:<https://getspar.com/invite?invitedby=5fcb2f0c-59ed-4cbb-9773-0463a728c6e7&slug=read-at-least-25-pages-per-day-ec40eb0b>
