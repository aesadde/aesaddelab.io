---
title: "Navigating the wealth of misinformation"
subtitle: "COVID-19 edition"
created: 2020-05-29
tags: mental models, psychology, stoicism
kind: article
published: 2020-05-29
author: Alberto Sadde
style: post.css
---

If you’ve ever had a hard time navigating the **wealth of misinformation** that we find all around ourselves, you’re not alone. During this pandemic, the amount of information, fake news, conspiracy theories, and weird stories has been so overwhelming that it is all too hard to discern the likely from the probable.

Stories about the origin and the causes of the pandemic abound all over the place. While [one story](https://nypost.com/2020/04/13/roger-stone-bill-gates-may-have-created-coronavirus-to-microchip-people/) claims that the pandemic was a carefully planned strategy for world domination carried out by the world’s *elite* (think Bill Gates), [another](https://www.breitbart.com/health/2020/03/13/ayatollah-khamenei-there-is-evidence-chinese-coronavirus-is-a-biological-attack/) claims that it all was a Chinese bio-warfare attack to gain control over the world.

<center>
![](../../files/images/pinky-brain.png){width=80%}
</center>

So what should you do? Who should you believe?

To answer these questions we first need to understand that regardless of the actual content or "theory", all these stories are almost definitely wrong.

Why?

Simply because most of them are too convoluted and far fetched. When we first encounter these stories we should remember [Hanlon’s Razor](https://en.wikipedia.org/wiki/Hanlon%27s_razor): "*Never attribute to malice that which is adequately explained by stupidity."* And also Albert Einstein: *"make everything as simple as possible, but not simpler."*

Second, there are tools and frameworks we can use to better navigate the oceans of misinformation without going crazy. In what follows, I’ll guide you through three different mathematical and psychological concepts —randomness, patterns, and control—that can help you debunk conspiracy theories.

### 1 - Randomness

The world is much more random than we think. That an event is very unlikely to happen doesn’t mean it will never happen. Probabilities depend on scale and long-term horizons.

Suppose for example that there’s a 5% probability of an error that causes a biology lab to leak a virus like the COVID-19 to the outside world in any given year (for simplicity let’s also suppose that the events are independent of each other). While this means that the probability of the lab not leaking the virus is 95%, over a long enough time scale the probability of a leakage increases. In our example, over a span of 20 years the probability that a virus leaks will be around 60%.

This, of course, doesn’t mean that we are guaranteed that such an event will happen, only that it is probable. The event might happen every year or might not happen for a very long time.

Now, I’m not making any claims about the origin of the virus and the causes of the pandemic. The above argument just points out that certain events—however unlikely they might seem—are plausible and need to be taken seriously.

This is why epidemiologists around the world — and even probability experts like [Nassim Taleb](https://aesadde.xyz/posts/taleb/) — have been warning about potential pandemics for years. To put it bluntly, a pandemic such as the one we’re going through is not a "Black Swan", a once-in-a-lifetime and highly improbable event; it is a "White Swan" all around. We may not have the tools to predict the outbreak but we did have the tools to know that such an event was not as improbable and to prepare better for it. In fact, there were at least three epidemics that had the potential to become pandemics within the last twenty years.

Lastly, we can also look at history and learn from it. Pandemics have been a recurring theme [all throughout](https://www.history.com/topics/middle-ages/pandemics-timeline). To think that our modern world could be somehow free from them is just wishful and naive thinking.

### 2 - Patterns

We are hardwired to see patterns all around ourselves. From patterns in the night sky to holy figures in stones and food, we have an innate urge to make sense of everything we see and experience in this world. Likewise, we are programmed to find explanations for everything and draw conclusions as quickly as possible.

![Source: [https://www.stormchasingusa.com/blog/face-in-the-clouds/](https://www.stormchasingusa.com/blog/face-in-the-clouds/)](../../files/images/pattern-clouds.png)

These traits were helpful as hunter-gatherers to quickly discern the difference between a harmless rustling of the bushes and a hyena. But in our modern world, we are often misled by the patterns we see and the conclusions at which we arrive.

Moreover, [confirmation bias](https://en.wikipedia.org/wiki/Confirmation_bias) dictates that we tend to favor news and information that confirms our long-held beliefs. We trust and believe what people we like say but we outrightly discard what people we don’t trust say, regardless of the actual facts and truth.

Put together, these two concepts—patterns and confirmation bias—explain why we might believe a certain conspiracy theory about the COVID-19 and discard scientific facts about the true causes of the pandemic.

We should be more careful and understand when we might be falling prey to our own minds. Remember what Richard Feynman said: *"The first principle is that you must not fool yourself and you are the easiest person to fool".*

### 3 - Control

Finally, let’s suppose for a moment that the conspiracy theories are true. Let’s pretend that there is, in fact, an *elite* or *government* of powerful men and women capable of creating a deadly virus, creating and hiding a vaccine, and creating a pandemic that started in China and spread all over the world.

If this were the case we would be in the presence of such a powerful force that there would hardly be something we could do about it. But you shouldn’t feel helpless or despair.

In this scenario, we would do well to remember the [Stoic teachings of Epictetus](http://classics.mit.edu/Epictetus/epicench.html) about what things are under our control and which aren’t. We should acknowledge the situation and try to navigate it the best we can: don’t complain, be vocal about it, and most importantly **help others**.

--

### Conclusion

Now that we’ve seen that pandemics are not only likely but are bound to happen, there are things you can do to protect yourself and learn to navigate the wealth of misinformation that hits us every day.

First, understand that we are fallible. Our minds trick us into finding quick explanations that confirm our beliefs. When confronted with new information always ask: What are the facts and the evidence supporting the claims? Am I rejecting this new information just because it goes against my beliefs? What biases am I being subjected to?

Finally, read History. Pandemics are nothing new. Just 100 years ago the Spanish Flu shook the world just after the biggest war the world had seen. History humanizes and humbles us. As Mark Twain used to say "History doesn’t repeat itself but it rhymes".

Stay safe.
