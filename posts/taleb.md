---
title: "Antifragility and Skin in the game"
subtitle: "Timeless lessons from Nassim Taleb"
created: 2020-04-13
tags: philosophy, mental models
kind: article
published: 2020-04-13
author: Alberto Sadde
style: post.css
---
![](../../files/images/time.jpeg)

There is no author whose ideas have influenced me more than Nassim Taleb's—an options trader turned writer and philosopher—Taleb is the ultimate example of a wise man. He has spent his life learning first hand about risk-taking—on the Wall Street trading floor. Through his studies in linguistics, mathematics, history, and philosophy, he has created a coherent set of rules and mental models that we should all learn and implement in our lives if we want to live better and understand the world.

Taleb describes his ideas in the *Incerto,* his philosophical treatise. There are four volumes in it (with more to come, I understand): *Fooled By Randomness, The Black Swan, Antifragile,* and *Skin in the Game.* The first two describe uncertainty and risk. In this piece, I want to focus on *Antifragile* and *Skin in the Game.* They are my favorite books in the series and have guided my thinking and decision making ever since I read them.

What I found most striking and compelling about his ideas, is what Taleb describes and explains about risk and risk-management has deep historical roots. For example, when he talks about "skin in the game" he bases his theory on Hammurabi's code and other ancient Codes of Law. Similarly, antifragility is inspired by heuristics and rules of thumb that have been developed and refined through the centuries, rules that your grandmother probably knows and applies each day. After all, the best ideas are those that stand the test of Time. For millennia, civilizations have had cultural and religious methods—traditions—to cope with uncertainty. Only now we are understanding the science behind these concepts.

We'll look and distill these two ideas. For each book, I'll first define and describe the central concepts, then I'll give you examples of each idea—there are many in the real world. Then I'll give you my takeaways and how I apply the ideas to my own thinking and life. Lastly, I'll give you a list of links and resources where you can read more about Taleb's work.

Let's get started!

---

## **Antifragility**

**Definition:** the opposite of fragile. An *antifragile* object or system is one that gets better under stress and disorder.

Taleb coined this term because he thought it impractical not to have a term for the *opposite* of fragile. If fragile means something that can't withstand stress—something that breaks under pressure—and *robust* means something that is not harmed nor improved by stress, then *antifragile* must be something that gains from stress, something that improves when things get crazy.

![Fragile vs Antifragile](../../files/images/fragile-anti.png)

"In the fragile category, mistakes are rare and large when they occur, hence irreversible; to the right the mistakes are small and benign, even reversible and quickly overcome. They are also rich in information. So a certain system of tinkering and trial and error would have the attributes of antifragility. **If you want to become antifragile, put yourself in the situation "loves mistakes"—to the right of "hates mistakes"—by making these numerous and small in harm.**"

Antifragility is closely related to two other concepts, *hormesis* and *via negativa*. Together they form a triad of mental models which can help us navigate better this chaotic world.

**Hormesis** is the property of certain systems to respond in different ways depending on the dosage of a stimulus. In terms of antifragility, hormetic systems benefit from stressors up to a point, after which they are harmed by it. This is why lifting very heavy weights a few times a week is more beneficial than lifting lighter weights every day, for example. On the other hand, ***via negativa*** is the process of elimination. We are better at removing than at adding things. By removing we reduce risk and potential errors. You've probably heard the saying "less is more". According to Taleb, *"less is more and usually more effective"* because things are simpler to reason about. In terms of antifragility, via negativa is helpful because it resolves randomness and unknowns from a system.

### **Examples**

- **Muscle growth**. Weightlifters know that to build muscles it is necessary to lift heavy weights. These weights act as the stressor required to become stronger. That is, muscles benefit from stress. Beware though, antifragility is about proportion (hormesis). Too much stress and we break our muscles, too little and we don't make progress. But once you find the right balance—usually heavy weights a couple of times per week—you'll become stronger.

- **Learning.** It's an antifragile process. Learning is not meant to be easy. The whole point of it is to make something difficult become easy or manageable over time. When you are learning something, you initially suck at it. Then, by receiving feedback and lots of trial and error, you start improving, you become better. This is central to the process of *deliberate practice* which I always use when studying something new. Learning is antifragile because it involves randomness—what you are trying to learn is initially unknown— and because the feedback you receive, you resolve some of that randomness and you improve. You learn not only what you were trying to learn but you become better at learning itself: you become antifragile.

- **Stressed plants.** [David Sinclair](https://en.wikipedia.org/wiki/David_Andrew_Sinclair) likes to describe plants and berries that are filled with minerals and vitamins which are beneficial to us as *[stressed plants](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4775249/)*. To survive, these plants have evolved to develop deep colors and a variety of mechanisms that make them better, stronger, and it turns out, good for humans. In fact, "some researchers hold that the benefits of vegetables may not be so much in what we call the "vitamins" or some other rationalizing theories (that is, ideas that seem to make sense in narrative form but have not been subjected to rigorous empirical testing), but in the following: **plants protect themselves from harm and fend off predators with poisonous substances that, ingested by us in the right quantities, may stimulate our organism—or so goes the story. Again, limited low-dose poisoning triggers healthy benefits.**"

- **Commercial aviation.** This is one of the best modern examples of an antifragile system. Airplanes are less and less dangerous because with every accident or plane crash comes a wealth of knowledge and feedback that gets directly injected back into the system. The accident is harmful and deadly but beneficial at the system-wide level. The system learns from its mistakes to avoid them in the future.

- **Restaurants.** *"Food in New York improves from bankruptcy to bankruptcy"*. We all know that the restaurant industry is wild and almost never a good investment. People start restaurants mostly because they enjoy them, not because they will get rich from them. While any individual restaurant will most likely suffer and go bankrupt, the system as a whole improves with each bankruptcy and customers —you and I— are rewarded as a result, with each closure comes a new opening and new and better options (or so we hope).

- **Startups.** By their very own nature, startups are fragile. When a company is just getting started it doesn't have huge amounts of cash to weather bad times nor it has all the right objectives and incentives figured out yet. If you ask me, this is what makes them both exciting and stressful. But startups when seen globally as a system, are antifragile. With every failure, startup founders and early employees learn from their mistakes and are able to improve and generate new ideas for the next venture.

### **Takeaways**

*"If you are not a washing machine or a cuckoo clock—in other words, if you are alive—something deep in your soul likes a certain measure of randomness and disorder."*

Antifragility changed my life. Ever since reading and learning about the concept, I've been looking for ways to become more *antifragile* in how I do things and live my life. How? By living Taleb's advice. For example, he argues that *"We are largely better at doing than we are at thinking, thanks to antifragility. I'd rather be dumb and antifragile than extremely smart and fragile, any time"*, so I try to start doing as soon as possible. I used to get stuck when starting new projects because I had to plan and know everything beforehand, so in the end, I never started. Now, I tend to have a bias towards action. Whenever I am starting something new I just jump into it, I learn by doing. This is how I started [my weekly newsletter](https://aesadde.xyz/join/), without a clear objective for it other than to try something new and use it to improve my writing. That's what I've been doing and after a couple of iterations it's starting to get traction.

I used to be scared of errors and being seen as the dumbest person in the room. While I still need to fight my *impostor syndrome* constantly*,* I've been learning to welcome mistakes and errors. After all, *"errors and their consequences are information",* they are the only feedback by which we can improve. Plus, *"avoidance of small mistakes makes the large ones more severe".* I'd rather fail early and fail often than avoid failure at all costs until it's inevitable. In my work, I look for opportunities to learn new things and receive feedback. When given the option I always choose the project that I feel least familiar with. Learning requires a certain amount of sacrifice. The more I get used to being in an uncomfortable situation the more comfortable I am overall.

I also apply *via negativa* any time I can. In fact, when I don't really know what I want or what I should do, *via negativa* comes into action. By eliminating options—by thinking about what **I don't want to do** or what **I don't want**— I am able to make the right decisions and actions. Most decisions are better done by a process of elimination.

Another aspect of antifragility that initially caught me by surprise is its direct relation to stoicism. The same way the Stoics are all about the domestication of emotions and learning to cope with stressful situations and episodes that are outside our control so is antifragility. Both give us a mental framework to operate with. For example, death is one of the Stoics' favorite topics. They have many mental exercises to prepare for it. It can come at any time. The tenet *memento* *mori—*remember you will die—is antifragile because the realization of our own mortality catapults us into action, we become alive.

Antifragility is not an end goal, but one of the best tools we have to lead a better life.

"What makes life simple is that the robust and antifragile don't have to have as accurate a comprehension of the world as the fragile—and they do not need forecasting."

### **Resources**

- [A conversation between Taleb and Daniel Kahneman](https://www.youtube.com/watch?v=MMBclvY_EMA). My favorite part starts [here](https://youtu.be/MMBclvY_EMA?t=2269). Taleb discusses the necessity for antifragility.

- [Living an antifragile life](https://fs.blog/2014/10/an-antifragile-way-of-life/) by Farnam Street.

- [How Things Gain from Disorder. Talk at Stanford](https://www.youtube.com/watch?v=B2-QCv-hChY&list=PL5my1e_rQIXA8xXb7VrXFN3-mdiZ_oi42&index=3).

---

## **Skin in the Game (SITG)**

![The Code of Hammurabi](https://lh4.googleusercontent.com/YYyHTyecVeodr3Wt0gamg5rV_3UjcmcDUoGagPUlxjlx3hcLHjGz1T5c7sPcgsGc6rRhGOb93cbYQXoD8UDvAtTO6RqOGOZVX_c_1dY-BupZSDG342slMh4_qSXTbZCl8GiL1Gsj)

**"If a builder builds a house. and the house collapses and causes the death of the owner of the house—the builder shall be put to death" - Hammurabi's Code**

**Definition:** to be rewarded or punished proportionately to one's actions and deeds. In Taleb's own words, skin in the game is "a risk management tool by society, ingrained in the ecology of risk-sharing in both human and biological systems."

Skin in the game therefore is about accountability, symmetry, and example.

All throughout history one had to be *in the game*  to be trusted and attempt to succeed, or as Taleb says, one had to **own** one's risk. Credentials didn't matter much. Actions did. Emperors and knights were on the front lines of battles, they had to lead by example. From Alexander to Napoleon, leaders derived their status and their authority "from a disproportionate exhibition of courage in previous campaigns", they constantly risked their own lives. This is what *Skin in the Game,* Taleb's volume 4 of the *Incerto* is all about.

### **Examples**

- **Plumbers.** Plumbers and 99% of other workers are judged not by other plumbers but by their clients. Without skin in the game, without owning the risk of doing a bad job, bad plumbers won't survive long in the market.

- **Pilots**. "Bad pilots end up in the bottom of the Atlantic Ocean". This is Taleb's way of saying that pilots are not judged by other pilots, but by the system themselves. By definition almost, pilots have skin in the game because they are the first ones to risk their lives when they are piloting an airplane. If something goes wrong—if the airplane goes down— they go down with it and the rest of the passengers.

- **Entrepreneurs.** They are risk-takers by nature. An entrepreneur is not only putting his reputation on the line but, more importantly, is putting his money behind an idea however crazy it might look. Elon Musk is perhaps the best real-life example of this. He has built his fortune and his companies by risking all his fortune.

![](https://lh4.googleusercontent.com/igy_DDXQn7HpYqfSvVPNdJhiE5RITuV-WKvYtUuzECHo_rOyBsk7hUn6UjGIreEZvjL2H-DCe3ninDl6u9Aeq3QrYP1fLklUZfkSTQL88-mJuhV422-yp_b1AioF08gdgiye1CKB){height=20%}

- **Intellectual Yet Idiots.** It is difficult to explain this better than Taleb himself: "it is hard to tell if macroeconomists, behavioral economists, psychologists, political scientists and commentators, and think-tank policymakers are experts. Bureaucrato-academics tend to be judged by other bureaucrats and academics, not by the selection pressure of reality. **This judgment by peers only, not survival, can lead to the pestilence of academic citation rings.** The incentive is to be published on the right topic in the right journals, with well-sounding arguments, under easily contrived empiricism, in order to beat the metrics."

### **Takeaways**

Skin in the game is about doing and example. Rather than listening to what people say, we should pay attention to what people do. Your grandmother taught you this. The elders have a way of recognizing and calling out fake people, not by what they say or don't say but by how they act and lead themselves. Next time your parents or grandparents tell you something about someone, you should pay attention, most likely they will be correct; they have more experience and have uncovered more fake people than you. I always try to use this heuristic in new situations, especially when meeting new people. I try to approach them with as clear a mind as possible —there are subconscious biases that are hard to escape— and focus on how the person behaves and acts. **"True intellect should not appear to be intellectual".** Words are less important than attitude and manners.

Skin in the game is about leading an honorable life. By doing your work and putting your words where your actions are, you can become both free and successful. Both plumbers and pilots are an example of this. Their reputation and life depend on how they do their job not how they say they do it. In fact, Taleb's definition of success is based on this: "I have no other definition of success than leading an honorable life." And the honorable way is always the longest one, the one in which you need to do the work. Remember, **"Compendiaria res improbitas, virtusque tarda—the villainous takes the short road, virtue the longer one."**

Skin in the game is also about action. Since "we are much better at doing than at understanding", skin in the game leads us to act, to learn by doing. This is where theory and practice defer. We shouldn't be surprised if something works but we can't find an explanation for it. Most scientific innovation happens this way: first, you build something that works, then you explain it. According to Taleb, "by definition, what works cannot be irrational", but "about every single person I know who has chronically failed in business shares that mental block, the failure to realize that if something stupid works (and makes money), it cannot be stupid." In my experience as a programmer, I've come to realize that [programming is a craft](https://aesadde.xyz/books/2020-03-30-coders-at-work/): you learn by doing. In most cases, you first experiment and implement some new idea and only after it works, you try to explain it and clarify your understanding.

Another powerful idea about skin in the game—one that could've been easily said by [Jocko Willink](https://www.instagram.com/jockowillink) – is that <b>"Freedom entails risks—real skin in the game. Freedom isn't free."</b> This is why entrepreneurs are the ultimate example of people with skin in the game. They risk everything to build something, make money and be free. Compare this to most of us who live for a salary. We're not really free because we must cope with things that we don't like about our job, bosses, peers, etc. If we were really free we wouldn't change our behaviors to keep our salary.

Ultimately, the idea of *skin in the game* completely changed the way I look at the world. This concept gave me a framework for life. First, I must own my own risk. I can't have an opinion about something if I haven't put in the work. Second, I try to always have a *bias towards action;* it is very easy to fall in the trap of planning and theorizing but this world only rewards action and doers. Third, Time is the ultimate tester, so I rely more on things that have stood the test of time so far. This is not to say that I'm a Luddite—quite the opposite, in fact, I'm usually an early adopter of new tech and toys—but I approach new things with skepticism. Understand: <b>Replacing the "natural", that is age-old, processes that have survived trillions of high-dimensional stressors with something in a "peer-reviewed" journal that may not survive replication or statistical scrutiny is neither science nor good practice."</b>

### **Resources**

- [The Intellectual Yet Idiot](https://medium.com/incerto/the-intellectual-yet-idiot-13211e2d0577) by Nassim Taleb.

- [Filtering as Skin in the game](https://twitter.com/nntaleb/status/1168548402721972229) by Nassim Taleb.

- [Skin in the game definition](https://www.investopedia.com/terms/s/skininthegame.asp) by Investopedia.

- Brent Beshore's [Skin in the Game highlights and notes.](https://twitter.com/BrentBeshore/status/990663990639378432)

### **A final word**

Modernity has created the illusion that we can replace the Natural, that risks are not necessary, and that we can avoid them. We should know better. This is exactly what Taleb tries to explain and describe to us throughout the *Incerto.*  This is why the lessons of History are so important; it might not repeat itself but it certainly rhymes, as Mark Twain would say. While modernity might be giving us ways to accelerate certain evolutionary processes and means to reduce risks—through technology—we can't expect the world to be less chaotic and random. After all, it is through randomness and resolving uncertainties with antifragility that we got here. Only Time will tell.

--

Thanks to [Fabiola Rosato](https://www.instagram.com/fcrosato/) and [Alexandra Olivieri](https://www.instagram.com/aleolivieris/) for reading drafts of this post.
