---
title: Stop Optimizing and Start Living
created: 2020-09-04
tags: habits, lifestyle, routines
kind: article
published: 2020-09-04
author: Alberto Sadde
style: post.css
---

<br>
<center>
![](../../files/images/optimizing.jpeg)
</center>
<br>

How many hours have you spent optimizing your working environment? Tweaking your calendar or email workflow? I've spent more hours than I dare to count.

On its own, improving one's personal systems is not bad. But compared to "good enough" optimization usually has diminishing returns.

As a Software engineer I've seen this pattern everywhere. Programmers have an urge to perfect every bit of code they can touch. But unless there is a pressing need—like an app being super slow for the user—there is no need for optimization. It is a waste of precious programming hours.

## Procrastination

Optimization is all but a different form of procrastination. Brad Fitzpatrick says in ["Coders at Work",](https://aesadde.xyz/books/2020-03-30-coders-at-work/) that *"Optimization is fun because it's not necessary. If you're doing that, you've got your thing working and nothing else is more important".*

Indeed, if you find yourself working to perfect something, think about why you're doing it? Aren't there other things that need to be fixed? Isn't there another project you can make progress on?

Then there's also premature optimization. Software engineers tend to look for efficiencies from the get go. But, as Donald Knuth says, *"premature optimization is the root of all evil"*. He might be referring to algorithms—the subject of his life's work—but it also applies to our daily lives.

I've experienced this with note-taking apps. Every time there appears a new one—Roam is the latest example—claiming to have solved note-taking, I somehow end up spending lots of time testing the new app. And then, almost by chance, I start optimizing my current system without thinking about the reasons first. This is not to say that optimization is never the answer. In competitive sports, optimization makes the difference between winning or losing. Formula 1 is a great example of this. Teams perfect the art of Pit Stops to squeeze those few seconds that might give them the win. And in track racing runners are always looking for lighter shoes to gain the milliseconds needed to win.

<br>
<center>
![](../../files/images/pit-stop.jpeg)
</center>
<br>

When it comes to our daily lives then, it is OK for us to tweak our systems, to improve them until they feel right. I know that the hours I've spent learning new keyboard shortcuts will save me time in the future. But trying to learn every single shortcut is futile. As Leo Babauta author of Zen Habit says,*"Optimizing is a focus on what's not important. Coming up with the perfect productivity system, the perfect todo list software — it's not important. It's procrastination on the things that are truly important"*

The problem is that many times we start optimizing for the wrong reasons. We start fixing and improving something because we don't want to actually tackle the actual task at hand. While you're working on optimizing your processes, your garage gym, your coffee-brewing process (I've spent lots of time making sure that the temperature of my brewing machine is at the perfect temperature when I wake up in the morning), you feel good. You believe you're making progress and that you're not wasting time. But this is of course an excuse your brain comes up with.

Optimization is a modern way of being lazy, period. On the opposite side of the spectrum, it is also where we go when there's nothing left to do. If we follow Pareto's rule of 80/20—80% of the benefits come from 20% of the results—then optimization has no place. Not to mention the opportunity costs: time spent optimizing something is time we won't be able to spend on our newest project or ideas. As Leo Babauta again says, "The savings never get realized. When you try to optimize, you are spending some of your precious life moments trying to find the perfect setup"

### **Start Living: How?**

So, we need an antidote to optimization.

As David Perell says, we must "ditch perfection" and "embrace randomness". Rather than optimizing all our social events, and managing our calendar like a perfect puzzle, we must allow for serendipity. Chance encounters are what spark new ideas, new partnerships and collaborations. We can't optimize this.

**Stop focusing on developing the perfect routine**. As Ryan Holiday says in ["It's Not About Routine, but About Practice",](https://ryanholiday.net/practice/) focus on practices instead. Routines are fragile. They depend on a series of events happening one after the other in perfect sync. But life is complicated, sh*t happens. Rather than optimizing for your "9 AM CrossFit session" focus on exercising regularly. When it comes to develop healthy habits, doing something "good enough" every day is better than doing it perfect a few times.

**Don't live by your calendar.** Most of us have work commitments. Schedule those, but don't try to fill in every single minute of your day. Not only leave room for chance encounters, but give yourself leisure time. In ["The Bed of Procrustes"](https://aesadde.xyz/books/2019-10-12-The-Bed-Of-Procrustes/) Nassim Taleb says that *"Your brain is most intelligent when you don't instruct it on what do do"*. We can't be all about business all the time. Our brains need time off to recharge and digest ideas. Optimization for the sake of optimization can be harmful.

Finally, **be OK with imperfection**. Perfection is the enemy of good. "Good enough" is what you need 99% of the time. Don't fall in the trap of optimizing for the last 1%. When optimization creeps in, we are often paralyzed. I've put off things many times because *"it's not the right time"*. But, as Jason Fried says in [Rework](https://aesadde.xyz/books/2019-06-29-Rework/)

> "The *perfect* time never arrives. You're always too young or old or busy or broke or something else. If you constantly fret about timing things perfectly, they'll never happen."

Choose wisely what you spend your time on. Start living.


