---
title:  My Favorite Apps of 2019
published: 2019-12-24
kind:      article
author:    Alberto Sadde
style:     post.css
---

Here is the list of apps that I tried throughout this year and have
become part of my routine and toolbox!


### [Gyroscope](https://gyrosco.pe/)
I first posted about this app back in August. It was recommended to me by my friend Marcos during a discussion about health trackers.
At the beginning I really liked the design and the philosophy of building a "passport" of places I visited, but the app was very buggy. I took me a while to get used to it and really enjoying. But soon the app started improving and in the last few months especially they've added a cool set of features that allow you to track different aspects of your life like sleep, fitness, diet, productivity, mood, etc.
I usually check it once a day and I really enjoy it. It aggregates all my health information in a single place and the design is hard to beat!

### [Memex](https://worldbrain.io/)
I've spent many hours thsi year looking for a web annotation app, an app that would allow me to highilght text on any website and write notes for later review. In this process I tried many different tools: Liner, Highly, Hypothes.is, Evernote Web Clipper, Notion Web Clipper. None of these tools felt quite right for me. For example, Liner and Highly are focused on sharing your highlights with freidns. Evernote's and Notion's web clippers don't have options to highlight, they just let you save pages or parts of them.

Memex in contrast is an open source Chrome extensions (it is compatible with Brave). What I like about it is that it is fully local, you can sync all your highlights and annotations to your computer (I keep the Memex folder inside Dropbox so I can keep a backup of the information as well). Memex allows you to highlight and annotate almost anything on the web, I am waiting for the PDF feature that is in their roadmap.

Other than that, Memex is now the app that I use for pretty much all my "information capture" process. I review all my notes and hihglights once a week and extract only the parts that I find relevant to my personal Notes.

### [Grammarly](https://www.grammarly.com/)
I tried this app after reading an article on [TechCrunch](https://techcrunch.com/2017/05/08/grammarly-raises-110-million-for-a-better-spell-check/). I didn't know how successful the app was so I decided to check it out. It's Chrome extension (also compatible with Brave) adds grammar and spelling corrections to everything that your write in your browser. There are some privacy concerns—everything you type is essentially checked by them—but the app is so useful that I don't think I'll delete it any time soon. The app also has some amazing features like detecting the tone  of your writing and other premium features that while the look interesting, they don't seem worth the hefty monthly subscription that unlocks them.

### [Brave](https://brave.com/).
I've already mentioned Brave a couple of times in this article. Brave is a privacy focused web browser based on Chromium. Chromium is the powerful JavaScript engine that powers Chrome. What this means is that you get the look and feel of Chrome but without the eyes of Google tracking everything you do online. Also, because it blocks ads by default (similar to Firefox), Brave claims to be much faster than Chrome.
While I haven't noticed much speed improvements (probably because my internet connection is already pretty fast), the privacy features were enough for me to make the switch from Chrome almost a year ago. And I haven't looked back.

### [Oak](https://www.oakmeditation.com/)
I've been practicing meditation for the past 5 years or so. I've cycled through different apps including the famous Headspace and Calm but always found myself dropping those apps and simply use a timer and focus on my breath. Until I found Oak. I think of this app as a glorified Timer that keeps count of how many times you've meditated. It free and has only a few options. This is exactly what I like, its simplicity. I regularly used it for my morning meditations merely as a lazy way of keeping track of my practice. I totally recommend it!

### [VSCode](https://code.visualstudio.com/)

Now we're in programing-land. Up until this year, I managhed to do 99% of my coding directly on the Terminal using [Vim](LINK TO POST) with a few plugins/ But, seeing all the work that has been put into VSCode, I decided to give it a try. After installing its Vim extension (I can't live without VIM keybindings), I found myself using it quite often, to the  point that now, especially at work, I do all my coding on it.
What I like the most about it is its versatility. The number of extensions you can install keeps growing so you can have a complete programming environment no matter if you are programming in Haskell, Golang, Python, to name a few or if you are writing configuration files for Terraform, Kubernetes, etc.

## Notable mentions

### [Notion](https://www.notion.so/)

Notion is the new Evernote. I just started using it and I really enjoy its design, the templates it ships with and, very importantly, the ability to export all the data to Markdown. This allows me to periodically backup all my data and keep it locally in plain text format.o

I see myself using Notion more and more for my personal projects and notes in the coming year. I hope they keep adding cool features and publish an API so users can add integrations and automation workflows to it!

### [Command E](https://getcommande.com/)

Command E creates a local index of files and links you access often over the Internet. You install it and connect it to your Dropbox, Google Drive, GitHub, etc. and then just by typing CMD+E on your Desktop you enter a full-text search box. Type a few letters and then press Enter to open on your browser the file/link that you were looking for.

I find it very handy especially when I need to access Google Drive files. I've always found Google's UX bad for this type of things and Command E allows me to only access what I need. This app hasn't become a fundamental part of my workflow but I think it has potential. Another interesting feature is that it builds the index locally and stores it there without transmitting your data to the web. This is a similar philosophy to Memex. I expect to see more and more apps taking this approach in the future. People are starting to value their privacy so we'll enter an era of  "de-clouding".

Finally, I'd love to hear about your favorite apps not matter if they're old or new! Let's keep the conversation going on [Twitter](https://twitter.com/aesadde).
