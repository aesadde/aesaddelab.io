---
title:  Concentrate Like a Roman
created: 01/27/20
tags: habits,productvitity
kind: article
published: 2020-01-27
author: Alberto Sadde
style: post.css
---

![](https://cdn.pixabay.com/photo/2016/02/17/14/36/lego-1205178_1280.jpg)

As a software engineer, my job requires me to be able to concentrate for long periods of time and think hard about the programs and algorithms I want to implement. But, it is all too easy to get distracted when sitting in front of a computer: not only are there meetings and calls, there are also direct messages, emails and the normal distractions from the Internet: Twitter, Instagram, YouTube, etc.

I am not the first one to struggle with this. The problem is so widespread that there are entire books ([Digital Minimalism](http://aesadde.xyz/notes/bibliography/2019-02-14-Digital-Minimalism) and [Deep Work](http://aesadde.xyz/notes/bibliography/2016-04-30-DeepWork) come to mind) devoted to uncovering practices for deep work and avoid distraction. I've tried many of these.

Over the years I've experimented with keeping track and trying to maximize my productive hours using RescueTime, a service that, once installed, tracks everything you do on your computer and helps you understand where and when you are wasting your time, or focusing on the wrong things.  I've also tried to block all my social media accounts, going as far as deleting or suspending accounts, removing the apps from my phone and blocking the sites from my laptop (using apps like Freedom, etc.).

All these things work, but only for a limited time. After a while my brain seems to find ways to get around these limitations. And to make matters worse, I end up being distracted tweaking and changing the settings of the services that are supposed to block the distractions.

A few weeks ago, with work ramping up after the Christmas break, I was about to stop all my concentration efforts when I read an old note I had saved:
 “Concentrate every minute like a Roman— like a man—on doing what’s in front of you with precise and genuine seriousness, tenderly, willingly, with justice.” - Marcus Aurelius

This gave me the idea of going old school. I printed the quote and glued it next to my computer monitor. Now, this quote is in front of my eyes most of the day and every time I'm about to get distracted it reminds me to concentrate, to do my work and to choose ["alive time over dead time"](https://thoughtcatalog.com/ryan-holiday/2014/09/alive-time-vs-dead-time/).
