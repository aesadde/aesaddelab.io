---
title:  My Personal Filesystem
kind: article
created: 08/03/19
published: 08/13/19
tags: personal, productivity, organization
---

Earlier this year I read [one of the best articles][wolfram] I've ever read
on personal filesystems and organization. For one who has struggled over the
years to keep a well organized digital archive that is easily searchable and
can work on pretty much *any* computer, this article written by Stephen
Woflram was a godsend. Inspired by it, I took some of Wolfram's tricks and
started experimenting with different hierarchies and tools until I got to
something that suited my needs.

What follows is the description of the filesystem that I've been happily
and productively using for the past 6 months or so.

## Requirements

Before jumping right in, let me give you some context. Based on my struggles
and usage I decided that my filesystem, had to fulfill the following
requirements.

1. It needed to be agnostic to operating systems (as far as Unix-based
Operating Systems go).

2. It needed to work without any external tools. This meant that not third party
   apps, or services were allowed. (In the past I kept a lot of my files in [Evernote][evernote]).

3. It needed to be based around plain-text files. This is mostly because I am
   used to [Vim][vim] a terminal-based text editor without which I feel I almost don't
   know how to use a computer. And also because plain-text files are [Lindy][lindy].

## The Filesystem

Requirements out of the way, lets dive into the details of my system.

At the root of my filesystem I only keep five folders:

1. **Life**: This directory contains all my personal information. From school
   transcripts to medical reports.

2. **Pictures**: I like pictures a lot and tend to produce quite a few of
them. This is where all my pictures, screenshots, memes, etc. live.

3. **Biblioteca** (i.e. Library): This is were I keep all the ebooks, papers and
   reference materials that I've collected.

4. **Projects**: In terms of my daily routine and work this is the most
   important directory. It contains all my current and past projects.

5. **dotfiles**. This is one that I believe every programmer should have. In it
   I keep all the configuration files I need for my terminal and editor. I keep
   a copy of it hosted on [GitHub][dotfiles].

Inside each of these folders I keep a directory named `ARCHIVE` where I dump
anything that's not relevant anymore but that I don't want to through away.
I took this from Wolfram's system and I have to stay it is very useful. For
example I know now that all my previous projects are somewhere in my `ARCHIVE`.
I also follow strict naming conventions as detailed below.

### Life

The Life directory doesn't have much of a structure.

I just keep a folder for my personal documents and one for other members of my
family. I also have top-level folders for documents related to possessions such
as my home and car.

### Pictures

This folders starts to get more organized.

In it I keep separate folders for pictures of specific things. These can be
places that I frequently visit, screenshots, etc.

Most of the pictures are kept inside a folder named `Events`. This is another
trick I copied from Wolfram. Inside it I keep one folder for each year.
Inside each year I keep one folder per event using the naming convention
`YYYY-MM-DD-event_name`.

What I like about this folder is that I now open a given year and see all the
events and pictures in it. This has helped me remember quite a few cool events
and has brought good memories as well.

One thing I started doing for events of this year is to add a `summary` file
where I write a small review of the event. I look forward to reading these in
some years!

### Biblioteca

This folder is named in Spanish because `Library` is a folder name that macOS
reserves for itself. While this might seem to break requirement (1) above, it is
a minor thing that doesn't really bother me and does not affect anything else.

In this folder I keep a loose category of reference material, mostly pdfs of
papers and books.

![][biblioteca]

This has been the folder that has taken the most time and effort to get used
to. For a long time I kept all my research and reference material in [Papers
3][papers]. This was a very good app but after it was acquired by Springer the
version that I used stopped being maintained and changed to an online
subscription that seem to restrictive and in violation of requirement (1).

After the initial struggle of getting al my files out of the Papers app, I
went through the effort or renaming all the files using the convention
`YYYY_Author_Title`. By naming it in this way I can now just use macOS'
search tool `grep` in the command line to quickly access any paper or book.

### Projects

As I said above this is the folder that I use on a daily basis. It is the
most organized of all the folders in my system.

At a high level it looks as follows:

![][projects]

I keep the usual `ARCHIVE` folder. Each active project (a project I am
currently working on) sit in its own folder.

Notable in this folder are:

- `TODO.md` this is my master TODO list that I review on a weekly basis. I
try to follow [GTD][gtd] principles for this.

- `IDEAS.md` this a brain dump of ideas that come to my mind from time to
  time. I end up working on some but most of them just sit there forever.

- `PLAN`. This is a folder inside which I keep my weekly plans. I always try to
  plan my week in advance and block off hours for productive work. This is
  a system that I learned from [Cal Newport][newport] and it has served me well over the
  years, especially when I was in college. It help me be organized and
  schedule all the things I want to do.

Each project has also its well-defined structure:

- `00.md` This is the "roadmap" of the project. In here I detail what the
  project is about, what is the status (starting, active, ongoing) and what
  are the next actions (periodically I translate the next actions into
  entries in the master TODO list so I can plan my weeks accordingly).

- `01.md` and beyond. Each new file is named using numbers in an increasing
  fashion. This lets me keep track of how the project progresses over time.

## Backups

It wouldn't be a robust system without backups! I keep two backups:

1. A Local hard-drive that's attached to my dock-in station so that whenever
   I plug my laptop while at home the backup is done automatically.

2. I keep all the folders inside `iCloud Drive` with `symlinks` to my home
   folder. This enables me to have a second copy of all my files as well as
   accessing them from my phone and other devices.

## Onwards

While I'm pretty sure this system will evolve over time with new and different
requirements, I feel I finally have a solid starting base. I feel I am now more
productive and spend less time looking for the information that I need and I am
more focused on actual projects.


<!-- Links -->
[wolfram]: <https://blog.stephenwolfram.com/2019/02/seeking-the-productive-life-some-details-of-my-personal-infrastructure/>
[evernote]: <https://evernote.com>
[vim]: <https://www.vim.org>
[lindy]: <https://en.wikipedia.org/wiki/Lindy_effect>
[dotfiles]: <https://github.com/aesadde/dotfiles>
[biblioteca]: /files/images/biblioteca.png {#id .class width=90%}
[papers]: <https://www.papersapp.com>
[projects]: /files/images/projects.png {#id .class width=60%}
[gtd]: <https://en.wikipedia.org/wiki/Getting_Things_Done>
[newport]: <http://www.calnewport.com/blog/2014/08/08/deep-habits-plan-your-week-in-advance/>
[reddit]: <https://www.reddit.com/r/productivity/comments/7fejpa/how_do_you_organise_your_digital_files/>
