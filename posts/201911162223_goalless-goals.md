---
title:  Goalless Goals
created: 11/17/19
tags: goals
kind:      article
published: 2019-11-17
author:    Alberto Sadde
style:     post.css
---

![](https://images.pexels.com/photos/1001682/pexels-photo-1001682.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260
"Photo by Kellie Churchman from Pexels")


If you're like me you have struggled with your objectives in the past. You've tried a thousand different things —apps, books, spreadsheets— all to end up feeling a failure, unable to make progress towards what matters to you.

The same with productivity. We do all sorts of tricks just to “feel busy” and
build a mental image about ourselves. But, let me tell you, 90 percent of that
busy time is wasted. We don't even stop to think about what is really
*essential* for us so we end up being very efficient but not very effective.

## Goals are overrated

In my experience, in order to avoid feeling like a failure and evade the “busy trap”, we need to change our view with respect to goals and productivity at a fundamental level.

First of all, it is very hard to come up with concrete and actionable goals. Must of us end up with things like “lose 20 pounds” or “read 100 books”. These are not goals, they are just wishes, too vague to be goals, not big enough to be dreams.

I've struggled with this. I've kept endless lists of “goals” like these. Other than the lack of concreteness a problem with these types of statements is that you don't know what the next action is, or worse, because there's no actual objective we end up stressing about the fact that we never complete them. Why do you want to lose 20 pounds? Or why exactly do you want to read 100 books?


> **“Most people overestimate what they can do in one year and underestimate
what they can do in ten years.” -Bill Gates**

Instead, we should focus on building habits, on working on those [areas](https://praxis.fortelabs.co/the-p-a-r-a-method-a-universal-system-for-organizing-digital-information-75a9da8bfb37/) of our life that will make us better over the long term no matter what, I call these **goalless goals**. You don't do them for the sake of a short-term reward, there's no end goal, there's no finish line. They become practices ingrained in your own self that compound over time. Examples of these goals are:

*   No refined sugars (health)
*   Write one page a day (writing)
*   Do 100 pushups a day (fitness)
*   Read 1 hour each day (wisdom)

Once you understand that there's no end, that these goals are not a means but an end in themselves, your mind will change. You will get better, healthier and wiser over time.

Another way to think about goalless goals is as habits, habits you need to build. In fact, I've used apps such as Done and Way of life to build up chains or streaks of consecutive days in which I practice the above items. Over time though, once you develop the habit, I find that the apps become less important and I stop using them. I only pick them up if I feel I've been slacking off.

Finally, understand that the key here is the lack of deadlines and purpose. Just do the work, be in the present and over time you'll be surprised about the results.


## Output

You've been here before. You get an idea, you start planning and writing down your goals, you think through every little detail but somehow you get stuck. You don't finish your project, you don't even start it. I've certainly been there. I get caught up with the minutiae of the project I'm devising all to just leave it there and never do it.

What I propose —something  I've learned from successful people— is to be
*“impatient with actions, patient with results”*. Rather than getting caught up
in the details of your never-to-be-born project, act! Start doing and see where
that goes. I guarantee you that you will get somewhere good.

By focusing on execution —on output over input, as Ben Hardy would say— something changes inside you. You start enjoying the project, you start finding time for it. In my case, I start getting a sense of accomplishment that drives me forward, no matter what.

> **You have the most information when you're doing something, not *before*
you've done it. -[Greg McKeown in
Essentialism](http://aesadde.xyz/notes/bibliography/2018-12-03-Essentialism)**

This approach works every single time. On one hand, if you are really curious and into the project, you will do more and you will be able to plan your next steps better —now armed with information about the previous steps. On the other, you will understand pretty quickly if you don't like the project, allowing you to stop it and move on rather than keeping lots of projects and ideas in your todo list.

So, the next time you're thinking about a new project or idea, don't plan it all the way until the end. Things will change. Rather, try to work on it whenever you can so you can iterate quickly and understand what you like and what works. And remember, a snowball is the smallest at the top of the mountain.

> **If discovery is a lottery, the greatest discoverers buy lots of tickets. -
Kevin Kelly in [What  Technology
Wants](http://aesadde.xyz/notes/bibliography/2019-10-30-What-Technology-Wants)**


## References

The above concepts are not new. I've just been reading and learning from others until something seems to work for me. If you want to know more, start here:



*   [Zen Habits](https://zenhabits.net/goal-less/)
*   [Minimalist Goals](https://www.theminimalists.com/no-goals/)
*   [Goalless
    Actions](https://www.scotthyoung.com/blog/2007/09/20/goalless-action/)
