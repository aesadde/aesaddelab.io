---
title: "Write of Passage Takeaways"
created: 2020-04-06
tags: writing, learning
kind: article
published: 2020-04-06
author: Alberto Sadde
style: post.css
---

![](https://images.unsplash.com/photo-1560092057-552a70533807?ixlib=rb-1.2.1&auto=format&fit=crop&w=2252&q=80)

It's been a few weeks since *Write of Passage* ended. It was such a *rite of passage* for me that I needed more than a few days to let everything sink in and really internalize what happened over the past 5 weeks.

What follows are my reflections and takeaways from the course.

### Between Oxford and Zoom Rooms

First, let's talk about the course format.

The course is 5 weeks long with lots of assignments and live sessions over Zoom. If this is your first “live" online course—like it was for me—you'll be blown away. The level of engagement is much greater than the more passive MOOCs (traditional online courses) because you need to be much more present. The course is arranged so that lectures can be watched offline, on your own time, and the live sessions focus on having deep discussions about that week's topics.

Each session felt like a workout. David, the course leader, is so motivated and enthusiastic that it was a challenge to keep up with him. He encouraged deep participation to the point where I was constantly flipping my attention between the video and the chat. There was always so much going on. After each session I had to sit down and let everything think in and take the time to do a recap: check my notes, skim interesting articles that were shared in the chat and process all the ideas that David shared with us.

No two sessions were alike. This encouraged me to attend as many as I could. Some were feedback sessions: you jump into a video with only one or two other students, share your writing in Google Docs and give each other feedback. This helped me tremendously. I always felt encouraged and the feedback always helped me improve my writing. I realized that even more experienced writers need to fight their impostor syndrome and struggle to find the best words to express their ideas.

Other sessions were more like lectures but I always felt back at Oxford. During my time there, we switched between big and passive lectures—you sat in the room and just consumed information— and smaller tutorial classes with lively and engaging discussions. As in Oxford, I made most of my learning in the second type of session. It was during these tutorials—or breakout rooms as we called them during Write of Passage—that I felt more at ease to share ideas, and learn from the direct experience of others. And, being more intimate, I connected more with the other students with whom I'm still in touch.

The weirdest and most revolutionary session though was the Saturday “Crossfit for Writing", a really intense hour and a half during which we trained and struggled to get from zero to an article draft. I never really managed to complete an article during these sessions but I always ended up with words that I could then polish and publish.

---

### Playing Legos with Ideas

WoP is not a traditional writing course. David is a young writer—a millennial like myself if we need labels—he was born when the Internet was already growing and changing the world, so the course focuses on writing and publishing on it. The most powerful idea that stuck with me is that **"The Internet is a place. It is not flat, it is more like a mountain range**,**"** so we ought to explore it and learn within it and from it.

Together with leveraging the power of the Internet comes “writing from abundance", the idea that we shouldn't encounter writer's block because we should never start writing from scratch. The whole point of leveraging the Internet is to be constantly learning, reading and curating information. We should treat “information as food" and only eat the best and most nutritious food out there. And, we should also “read with the intent of writing and write with the intent of reading". So, when it is time to write, we assemble the pieces together: **writing is like playing legos with ideas**.

David's idea is not new—history is filled with writers who used [archives](https://zettelkasten.de/posts/zettelkasten-improves-thinking-writing/), [commonplace books](https://ryanholiday.net/how-and-why-to-keep-a-commonplace-book/) and other means to store and retrieve ideas for their stories. But it is a powerful one, especially in an age where information—and noise—are abundant and easily accessible. David spent quite some time explaining his whole *information capture* process—largely based on [Tiago Forte's](https://www.buildingasecondbrain.com/) *[Building a Second Brain](https://www.buildingasecondbrain.com/)*. He encouraged all students to set up a similar system to start capturing, digesting and curating information. This was super helpful for me and I've since rethought my [personal filesystem](https://aesadde.xyz/posts/2019-08-13-filesystem/).

Overall, **leveraging the Internet and assembling your writing** are the central aspects of the course. They're the backbone of it all. In fact, according to David “writing on the Internet is the single best way to accelerate your career." Having seen his blog and Twitter following skyrocket, he is a living example of this which makes his lessons so much powerful and substantiated. So, this should encourage us to not be afraid to write and share our ideas. Wonderful things will happen if we get started!

---

### Personal Monopoly

The internet might be a place but it can be a big and scary one. There's so much going on, so many ideas shared every second that it is easy for us to feel little and insignificant. This was certainly the case for me. I've always thought: with all the smart people writing on the Internet, what do I have to say? Like most, I struggle with my ideas. Even after writing thousands of words, about a topic or an experience, I feel things are not good enough to put out there. I'm scared and paralyzed to the point of not publishing.

This is where the “personal monopoly" part of the course comes in. David argues that **we all have something to say.** Our human nature dictates that we have a unique perspective of the world. Each of us has a different set of characteristics and life experiences that makes us all unique, so our ideas and opinions will be as well. Again, the idea itself is not new and it relates to our constant search for meaning and purpose. The Japanese even have a concept for this, [*ikigai*](https://en.wikipedia.org/wiki/Ikigai), "the reason for being". Part of discovering and building our personal monopoly then is to write and discover our meaning, what we enjoy, and what we want to do.

Throughout the course, David encouraged us to think deeply about our personal monopoly and to understand that we should share our unique view with the world. We learn so much from it, and we live in it, that this is only fair. And we do this by writing. After all, “writing is thinking". Through writing, we make confusing ideas and thoughts clearer, first for us, later for our readers.

Overall, the idea of the Personal Monopoly is one that has been echoed by other writers and thinkers and goes contrary to the common teaching of “following your passion". Cal Newport, for example, echoes in his book “So Good They Can't Ignore You" what David teaches in the course: we build our personal monopoly by writing and doing, not just by waking up one day and deciding to “follow our passion". As humans we are in constant evolution, our “passion" might be different in ten years. So, the fundamental lesson to understand is that **your personal monopoly is not a goal but a journey,** and this journey is better traveled with writing. The more you write and publish the more you will understand what you want and what you have to offer to the world, the more you will connect to other people and learn. Writing becomes your own [personal flywheel](https://www.jimcollins.com/concepts/the-flywheel.html).

---

### Rite of Passage

At first, I was so hesitant to join the course, that it was my girlfriend who made the decision: she bought the course and told me just as it was about to start. I am grateful that she did because it turned out to be a very rewarding experience. If this is how the future of learning looks like, I want to go back to school!

I may still struggle with my writing and publishing habit, but the course has upgraded me to a whole new level. I now treat information differently and try to see my experiences and everything around me as potential writing ideas. The real world is so high-resolution that you just have to look around and you'll see that interesting things are hiding in plain sight.

---

Thanks to John Lanza for the feedback on this article and for the great morning writing sessions we've had.


