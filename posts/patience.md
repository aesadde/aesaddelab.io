---
title: "On Patience"
subtitle:
created: 2020-04-20
tags: philosophy
kind: article
published: 2020-04-20
author: Alberto Sadde
style: post.css
---

One of the quotes that guides my thinking and acting every day is from [Naval Ravikant](https://twitter.com/naval/status/1029300574997499910?s=20):

> ***"Impatience with action. Patience with results".***

At first, it is difficult to grasp. We have become so accustomed to instant gratification, that it sounds almost impossible to do. To me, patience was inconceivable until recently. I thought it was only for the old and tired, and I'm filled with energy. I want to do something, I want to act now. Who has the time for patience?

But then, it clicked. The point of patience is not to sit still. It's not to avoid or delay doing. The point of patience is to detach our actions from results. The goal of being patient is to remain objective even when you're putting all your energy towards something. Perhaps, Peter Diamandis put it best when he said that ***"Patience is a virtue, but persistence to the point of success is a blessing".***

We need to rethink patience. I know I've had to do it. Once I understood its essence, I began to see things in a different light. Patience stopped being something to abhor and evade and became the most important aspect of everything I do.

Patience must be a result of doing. For you to practice it, you must be working on something and hoping for results. But beware of hustling to the point of exhaustion and burn-out. You must remember that "it is only through consistency over time, through multiple generations, that you get maximum results."

Patience allows you to detach from outcomes. You can only control a few things: your effort, your focus, and your attitude. But you can't control the results. If you are writing something—like I'm writing this piece right now—you can only control the words and the ideas you put on paper, not the readers' reactions. Will somebody like what I have to say? Will people actually read it? All you can do is to keep at it. In the end, as Bill Walsh would say, ***"the score takes care of itself"***.

Being patient is not easy. Like any other skill, it must be learned. This is why some older people seem to be better at it, they just have more experience in practicing it. If it were simple, we would all be top-performers in one field or another. Patience is difficult precisely because it demands repetitive and constant action. It is what Haruki Murakami tells us when he talks about his writing experience: ***"Repeat. Patience is a must in this process, but I guarantee the results will come."***

Even though patience is not easily achieved, it's rewarding. It is a compounding skill. You won't see results early on in the process but eventually they will come. Perhaps it was Seneca who understood this perfectly when he claimed that ***"Luck is what happens when preparation meets opportunity."*** You can't predict when opportunity will arise, but you can be ready for it through hard work and patience.

And, if you're still thinking that you don't have time for patience because life is short, then you've missed the point. You've got time. Good things don't come in an instant. Understand what Ryan Holiday states: "the world is like muddy water. To see through it, we have to let things settle. We can't be disturbed by initial appearances, and if we are patient and still, the truth will be revealed to us." Once you understand this, you will realize what Gary Vee has been saying for years: ***"Patience is practical".***
