---
title:  Random Fun 1
created: 06/26/19
tags: newsletter
kind:      article
published: 2019-06-26
author:    Alberto Sadde
style:     post.css
---

Hello folks!

For a long time I've been considering sharing what I read and find interesting
with others but until now, have always been too shy to do it.

What follows is an experiment!  I'll send you an email once a week for the next
3 weeks. If you like it and I enjoy writing them, then we'll keep going! You can
sign up to the email list [here][signup].

### What I am reading 📚

Reading might be the best investment of my life. I mostly read to learn, not
purely for pleasure, so  I tend to prefer non-fiction to fiction books. But
I enjoy a novel from time to time.

I tend to read only one book at a time (can't really cope with too much context
switching). Right now I am reading ["The Undercover Economist"][undercover] by
Tim Harford. It tries to explain Economics by using everyday situations and
analogies. I have mixed feelings about it. Somehow [Nassim Taleb's views on
Economists][taleb] keep popping up in my mind as I read this book.

By the way, I transcribe passages that I find interesting from the books I read,
you can find them [here][books].

### Nerd Corner 🤓

It wouldn't be me without `"nerding"` a bit.

For the past several weeks I've been experimenting with automating everything in
my apartment. This is nothing new, [there are tons of "smart" devices][smart] out there
that let you control stuff with your smartphone.

But, I recently found out about [home-assistant.io][home-assistant]. This is an
open source project that is very flexible and "scriptable" (you can write your
own programs and automations). On top of this, you can use home-assistant to
expose devices that are not compatible with Apple's HomeKit so you can still
control them with Siri (in case you're wondering, yes, I prefer
Siri over other voice assistants).

Automating my home has been a very cool but also time consuming hobby. I can't
really complain since [I was warned about this on Twitter!][tweet]. And now I'm
warning you, so you can't blame me if you go down this rabbit hole 😅!

So far I've added automations to my bedroom lights and espresso machine using
a couple of [WeMo][wemo] plugs. For example the coffee machine turns itself on
at 7:25 AM and turns itself off 5 minutes after the house is empty
(home-assistant lets you track phones connected to your WiFi so it "knows" when
someone is not home anymore).

On another note, I set my phone to [Grayscale][gray] in an attempt to [cut back on my
screen time][screen]. My phone feels much more boring now but I'm not yet
confident that I've reduced its use. I'll expand on this experience soon.

### Also interesting 🤯

- I stumbled on [this list of mental models][models], and its pretty awesome! If
  you are not familiar with the concept, [James Clear's blog][mental] is a good
  place to start.

- This [infographic][world] matches each US state with a country with comparable
  GDP. It opened my eyes to just how big and powerful the US still is.

- I use lots of emojis 😜 while on my phone but up until now I had a hard time
  using them on my laptop until I found that on a Mac you can access all the
  emojis by pressing `CTRL+CMD+SPACE`.

<!-- Links -->
[home-assistant]: <https://www.home-assistant.io/>
[tweet]: <https://twitter.com/aesadde/status/1141060454292156416>
[taleb]: <https://www.businessinsider.com/nassim-taleb-on-economists-2013-12>
[wemo]: <https://www.amazon.com/Smart-Enabled-Google-Assistant-HomeKit/dp/B01NBI0A6R/ref=sr_1_3?crid=39FQGYJCCPN0Q&keywords=wemo+smart+plug&qid=1561656120&refinements=p_85%3A2470955011&rnid=2470954011&rps=1&s=gateway&sprefix=WeMo%2Caps%2C191&sr=8-3>
[world]: <https://www.gzeromedia.com/the-graphic-truth-the-50-countries-worth-of-america>
[models]: <https://medium.com/@yegg/mental-models-i-find-repeatedly-useful-936f1cc405d>
[mental]: <https://jamesclear.com/mental-models>
[undercover]: <http://timharford.com/etc/undercovereconomist/>
[signup]: <http://eepurl.com/gv6g0j>
[books]: <http://aesadde.xyz/books.html>
[screen]: <https://observer.com/2018/05/grayscale-can-cure-smartphone-addiction/>
[smart]: <https://thewirecutter.com/home-garden/smart-home/>
[gray]: <https://gogray.today/#go-gray>
