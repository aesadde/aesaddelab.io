---
title: "How To Be A Friend"
subtitle: A personal summary of Cicero's "De Amicitia"
created: 04/26/20
tags: philosophy, advice, life
kind: article
published: 2020-04-26
author: Alberto Sadde
style: post.css
---

![](https://images.unsplash.com/photo-1488956322016-1e7554142097?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2250&q=80)

What follows is a personal summary of the lessons I learned by reading ["How To Be A Friend: An Ancient Guide To True Friendship"](https://amzn.to/2Sa3m5j) by Marco Tullius Cicero.

--

We need to rethink friendship. Today we are so consumed by social media that even though we have thousands of connections or "followers" and we share with them all sorts of superficial details about our lives, we seldom have the kind of relationship with others that allow us to share our deepest fears and to nurture our biggest dreams.

As [research shows](https://guilfordjournals.com/doi/pdf/10.1521/jscp.2018.37.10.751), despite our ever-increasing interconnectedness we are more alone than ever. This is a disaster because there's nothing more important in life than friendship.

It turns out that we can turn to history to give us a lesson about what true friendship really is. Marco Tullius Cicero, an ancient Roman writer and politician, who lived more than 2000 years ago, already knew that true friendship was elusive but so important that he wrote the ultimate guide about it, "De Amicitia"—or "How To Be A Friend"— dedicated to his best friend Titus Pompoinus Atticus.

The book is an imaginary dialogue between Laelius and his two sons-in-law Fannius and Scaevola. During the conversation, Laelius tells the lessons and the importance of friendship by providing examples of his lifelong friendship with Scipio, one of the greatest military men and politicians of Ancient Rome.

The dialogue starts with the importance of friendship and why we shouldn't live without it: *"Nature loves nothing that is solitary, but always inclines toward some sort of support. **And the sweetest support is a very dear friend.**"* Centuries later, C.S Lewis would echo this by saying that *"friendship is unnecessary, like philosophy, like art, like the universe itself… It has no survival value; **rather it is one of those things which give value to survival.**"*

Rather than being glued to our phone screens, we should consider this advice and cultivate deep and meaningful friendships in our lives, not because they serve any particular purpose, but precisely because they don't serve one. True, deep and meaningful friendships allow us to give our best selves to another being. Cicero knew this and through the voice of Laeilus urged us to *"strive for virtue, for without it friendship cannot exist. **And friendship, aside from virtue, is the greatest thing we can find in life.**"*

Cicero carries on and reminds us that the fruits of life and hard work are best enjoyed when shared: *"There is no meaning in achieving goals, success, fame if you are all alone."* This was as true thousands of years ago as it is now when we spend endless amounts of time sharing mundane aspects of our lives on social media. True friends don't need to know every little detail about your day. They are there to share your joys, successes, pains, and sorrows. And remember, *"Nothing brings friends more joy than returning kindness and helping each other."*

We must actively seek meaningful friendships. This is what Cicero, long before electricity and computers was trying to instruct us: *"Seek from friends only what is honorable and do for friends only what is just—but don't wait to be asked."* Even [Seneca](https://aesadde.xyz/books/2019-06-28-The-Greatest-Empire/), one of the great Stoic philosophers, advised his readers to *"seek out the company of those with whom they can be their best selves."* This is easier said than done. Many people have ulterior motives so some friendships might not be real but be at the service of some particular objective.

About this last point, Laelius warns his sons-in-law that many people will have ulterior motives. After all, Ancient Rome was a corrupt civilization built on favors and paybacks. So he tells them that ***"true friendship can't exist except between good people,"*** so they must use love as a guiding principle when seeking friendship, <b><i>for it is love [amor] from which the word "friendship" [amicitia] comes, and this is the origin of goodwill."</b></i> In the end, he tells them that they will know when they've found a true friend because ***"A friend is, quite simply, another self."*** 

True friends complement each other and nurture each other's souls. But they take time to build and develop. Like most good things in life, true and long-lasting friendships require [patience](https://aesadde.xyz/posts/patience/) and care. So take the time to talk to your friends, to help them if they are in need. Take the time to enjoy one of life's best gifts.

The "De Amicitia" is a fantastic and short read that will leave you longing for friends and thoughtful conversations. It will make you reminisce about those moments that you've had with your friends. It will bring memories back and will hopefully guide you to make new ones. After all, as the English poet David Whyte beautifully wrote,

> ***the ultimate touchstone of friendship is not improvement;*** ***the ultimate touchstone is witness, the privilege of having been seen by someone and the equal privilege of being granted the sight of the essence of another, to have walked with them and to have believed in them, and to have accompanied them on a journey impossible to accomplish alone.***
