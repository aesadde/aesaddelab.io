---
title: "India"
subtitle: "Personal Perspectives"
created: 2020-05-03
tags: travel
kind: article
published: 2020-05-04
author: Alberto Sadde
style: post.css
---

![](../../files/images/india/india_logo.jpg)

### Why India?

- India is one of those countries that is in everybody's bucket list. It was definitely on mine for a long time. I was living in York, in the UK when I first thought about going to India. Some of my best friends in college were from India and I felt a strange but deep familiarity. Venezuela and India are far apart geographically but culturally they share similarities: people seem always happy despite adversities, mangoes are everywhere and we all missed the sunshine while in England.
- I had planned unsuccessfully a few trips to India around 2015-2016 just when I was graduating from college ([I made my way to South Africa instead](https://aesadde.xyz/posts/2017-01-03-year_review/#travel)). So, when my friend Francisco announced that he was getting married, I knew my India trip was finally going to happen. I was not going to miss a Mexican-Indian wedding, that's once-in-a-lifetime kind of experience!

### Why this post?

The whole trip was a blast of experiences. It was one of the first times that my girlfriend and I experienced such a huge amount of familiarity, novelty and excitement all at once. India was such an incredible experience that the only way to make sense of everything I saw and did was to write about it.

What follows is a collection of personal perspectives that I wrote on my travel log. This is not meant to be an objective account of Indian culture, gastronomy, history or politics. But simply a humbling account of what my mind went through while walking, touring, dancing and eating some of the best food in the world.

Let's dive in.

### Index
- [Traffic](#traffic)
- [Food](#food)
- [Pollution](#pollution)
- [Religion](#religion)
- [Arts and Crafts](#arts-and-crafts)
- [Random Nuggets](#random-nuggets)
- [Parting Thoughts](#parting-thoughts)



### Traffic

To drive in India you need three things: good brakes, good horn and good luck. - Indian proverb

When landing in Mumbai, I thought I was going to be prepared for the amount of life and chaos in the streets. After all, I come from a developing country as well —Venezuela— and while growing up there, traffic was at its worst. It could take anywhere between 20 minutes and a full 2 hours to cover the 8 miles between my home and my University.

In the 2000s Caracas had a lot of traffic. It was by far the worst traffic in the country. So much so that for a brief time the local government tried to enforce stop days for cars and even increase the price of gas. Nothing worked. So, when I landed in Mumbai I thought to myself "how crazy can it be?!"

I was surprised, in a good way.

First, there is way more traffic than you could ever expect. Once you get out of the airport area and into Mumbai proper, you see cars, vehicles all over the place: taxis, tuk-tuks, buses, motorcycles. They all travel the streets and highways in a chaotic but orderly way. In contrast with Caracas, I never saw people swearing at each other, nor fighting in the midst of traffic. People just go, mind their own business and use the horn every few seconds, but not to complain to other drivers, rather to let them know where they are. This seems to work: during my 11 days in India I only saw one accident, a very small traffic bump in the market street of Jaipur.

Second, what's most striking about Indian traffic is the sheer number of vehicles on the street at any point in time. Lanes are optional (as they're in my own country) and at any point in time you can have anything between 2 to 6 lanes, most people just make their own.

The other surprising and amusing aspect of the traffic is the variety of vehicles. In San Francisco, where I now live, one sees mostly cars, a few motorcycles and some bikes and electric scooters. In Venezuela we mostly have cars—big and small— and motorcycles, thousands of them. In India it is common to see carts pulled by ox, camels, horses or donkeys, cars of all sizes, buses, trucks and tractors, motorcycles, tricycles, bicycles, tuk-tuks, all in a continuous flow of people on the streets. I have to say though, that in Mumbai I saw mostly motor vehicles, it was when we travelled north —to the golden triangle of Jaipur, Delhi and Agra— that the variety of transport media started to grow.

Then there is the noise. I already mentioned how Indians use the horn to let other drivers know their position. Horns are ubiquitous, their sound permeates everything. In Mumbai I can't remember quiet moments, any instant was marked by a background honk. People are so used to this never ending rumble that there are [websites that stream the sounds of Mumbai's streets](https://soundsofmumbai.in/) for people that live in quieter places and miss its familiar chaos.

In terms of cities, Mumbai was the most chaotic one. I attribute this to the fact that it has a huge population, it is very dense —as in one of the densest cities in the world dense— and I was there over New Years; way too many tourists and people partying. The traffic in Jaipur was also bad, but only in a few places near the pink city walls. Delhi, in contrast, seemed almost ordered and fluid compared to Mumbai. This may be because the streets there are much wider, with lots of roundabouts — probably thanks to the British, [they love roundabouts in England](https://aesadde.substack.com/p/3800621_week-27)—and while there are an estimated 7 million cars on Delhi streets every day, the traffic was always fluid. In Mumbai there were some times when our cab wouldn't move for many minutes, simply stuck in an ocean of cars.

### Food

![](../../files/images/india/9BAF31F8-EF0F-4000-9EC8-A004EEC1AAA4.jpeg){style="width: 30.0%;margin-left: auto;margin-right: auto;"}\ ![](../../files/images/india/28C552D1-6E52-4C32-9B55-58069224DB90.jpeg){style="width: 30.0%;margin-left: auto;margin-right: auto;"}\ ![](../../files/images/india/E9670A6D-1598-4562-956D-09393BEAD354.jpeg){style="width: 30.0%;margin-left: auto;margin-right: auto;"}

Back when I lived in the UK one of my favorite things to do was to go out and try all the curry houses in the town I was living in. In York this was always epic, my Indian flatmates knew what to eat and where to go. In Bristol, our team at BAE had a policy to try a new curry house at least every month.

I knew then that although the food was awesome, it was not the real thing. In order to try the real Indian food I would have had to actually go there. And there I went.

In Mumbai the food was very different from what I had expected. Lots of dishes that I had never tried like [panipuri](https://en.wikipedia.org/wiki/Panipuri). Then there were things that I knew about, like the dosas, but, as expected, these dosas were way better. The quality of the dough, the spices, everything was just more flavorful!

There were also the drinks. And I'm not talking about Old Monk rum that seems to be everywhere, but non-alcoholic refreshing drinks. My favorite by far was the Sweet Lime Soda: Sparkling water, lime juice and sugar. Nothing fancy but in the warmth of Mumbai it was always refreshing to sit down and have one.

The good thing about Mumbai was that because it seemed very cosmopolitan and modern, it was very easy to find good places directly on Google Maps. We would just look for a restaurant with lots of reviews and head there. We were never disappointed. This was not true in other cities we visited. In the Golden Triangle most of the restaurants we found online seemed very touristy and a couple of times we had food that we didn't enjoy.

Now, a word about spiciness. I never had a dish that was so hot and spicy that I couldn't finish it. Perhaps the waiters just saw my white tourist face and knew better, but the hottest curries I've had, I've had in the UK. This was a bit of a surprise, I was actually expecting to feel the punch of spices and chili in every dish. Perhaps I missed out, perhaps I was spared.

Then of course, was the wedding. The main event of our trip. I was told this was not a common Indian wedding —there was a lot of alcohol. Common or not, I had a lot of fun. And the food was there, always there. There were many dishes, from different parts of India, but what I liked the most were the tandoori dishes. I had tandoori chicken, lamb and naan bread, almost non-stop for 3 days. I don't regret it. Actually I still crave them. Perhaps I should build my own tandoori on my balcony.

One of the few things I missed out were mangoes. Growing up in Venezuela mangoes were everywhere and to this day they're my favorite fruit, so I eat them every chance I get. I was looking forward to eating mangoes in their natural habitat — mangoes are an Indian fruit— but they were not in season, so the few things that had mangoes in them were juices and desserts and I'm not very fond of them. Alas, I'll have to go back during mango season.

### Pollution

I was not prepared for it.

I had been told by friends that the air in Delhi was particularly bad but I imagined that was something that only lasted a day or two. I didn't even think about the possibility of the air being polluted in Mumbai or the other places I visited.

The sad reality of pollution hit me as soon as we landed. We had a layover in Delhi before going to Mumbai and the airplanes were having difficulty landing because of the smog in the air. The visibility was so bad that our pilots had to perform a "CAT-III" landing —a fully automated landing because they couldn't see outside the airplane. Up to then, my only other automated landings had been in London Heathrow, but the UK is known for its thick fog.

Once out of the airplane we could tell that the fog was not common fog. It was not misty and cold, but thick and with an odor of smoke. Essentially it was smoke. I was shocked.

One of the things that first impressed me was that people acted normal. Nobody seemed to mind the odor. Nobody seemed to care to use masks to protect their lungs. Even inside the airport terminal the smell followed us. We later found out that the terminal doors were left open so all the air also entered the terminal.

Delhi is one of the most polluted cities in the world. But the air is not always so bad. Friends and family that have visited in other times of the year have never encountered the kind of air we faced. The issue seems to be that in winter most farmers burn the remaining of their crops to regenerate the soil and a lot of the smoke remains in Delhi because of the lack of westerly winds during that time of the year. Add to the smoke, the 7 or so million cars that transit the city every day and you get an unhealthy combination of gases that you breathe.

Mumbai was better, way better. It's proximity to the sea means that the air gets cleaned up more often but even then, the air was far from clean. In the mornings it was good, you couldn't see much fog, but as the day went by the air always started to deteriorate to the point where you would stop seeing the top of the tall buildings. My last hope for clean air was when we visited Alibaug, a coastal town not far from Mumbai. But even there, the air was bad. In fact, our whole ferry trip from Mumbai was covered in smog.

![Ferry trip to Alibaug](../../files/images/india/068E7B5C-4043-49E9-BACE-945184D4737D.jpg)

India was beautiful and we liked and enjoyed everything we saw. But during the entire trip I couldn't stop thinking how much better and more beautiful it would have been without the smog. Many of the historical sites we visited would've been clearer and more impressive (we had an almost perfect sighting of the Taj Mahal though!). It is sad that such a beautiful place is submerged in such pollution. I hope that the government and the people realize how bad that is for them and find ways to solve it.

### Religion

India is synonymous with religion and superstition.

Everywhere you look there is a temple of some sort. **Most of the temples are hindus as 2/3 of the population practices Hinduism. The remaining 1/3 are mostly muslim with a few percent being Christian.**

The temples are very pretty, all are different but feature the same styles and design, at least the ones we visited. In Mumbai, where we first encountered the temples, we tried to enter a few but because it was New Years' Day, the queues were awfully long. In one of the temples we wanted to go, the line started a few kilometers before the temple. We decided to just roam around the city instead.

You can feel religion everywhere. Some times it's the muslim Imam calling for prayer many times throughout the day. But you mostly feel religion by walking down the streets. There's people preparing flowers for the temple goers. There are figures of **Ganesha, Vishnu,** and other gods everywhere.

And then you have animals. Cows are everywhere as they are considered sacred. But there are also pigeons, stray dogs, monkeys, etc. Hindus believe in reincarnation so everybody feeds and takes care of them, it is "good karma" one of our tour guides said to us in Jaipur.

![A religious figure on a street wall](../../files/images/india/8338352C-672B-4FDE-9F4B-F4737C8412EF.jpg){style="width: 50.0%;display: block;margin-left: auto;margin-right: auto;"}

Speaking of good karma, this brings us to superstition. I thought that Italians were superstitious: many don't walk across a street if they just saw a black cat walking by, or don't go out if the date is the 13th. But Indians take superstitions to another level. Even today, many families go to their priest who is in charge of deciding if the horoscopes of the people about to get married match. If they don't, the marriage doesn't go through, families break up. As simple as that.

There are those, though, less religious, who try to choose their own fate. We were told that it is not hard to bribe the priests to tell the families that the horoscopes match.

Indians believe in so many things that they worship and believe in things even outside their religion. **The Aji Ali temple in Mumbai houses the tomb of a muslim martyr but most of the visitors are Hindus. Why is this so?**

And then there's the days of the week. There is one god for each day and many go to a different temple depending on the day. Others mostly worship one god. On our trip around the Golden Triangle, our driver would go to the temple every morning before starting the drive. He told us that he doesn't remember ever skipping a day.

Then there's also the cars. All cars have a religious figure on the dashboard. This reminded me of vehicles in Venezuela. Most of them have a cross and a Virgin Mary's stamp stuck to the windows, or hanging from the rear-view mirror. Here, there is usually a figure of Ganesh, the god of good luck and good things, on the dashboard. But I didn't see the same figure twice. People choose their own style and colors and in many cars the figures were decorated with blinking lights and representations of small temples.

### Arts and Crafts

One of the most impressive things I experienced in India was the detailed arts and crafts that you find all around.

From clothes to carpets, to paintings, Indians seem to have an endless patience for dealing with the smallest of details.

When we visited the Golden Triangle our tour, of course, included visits to certain traditional shops. This was a nice experience because we got to see how certain crafts are done, but in most places the sellers were not happy with us. We were not good customers: we had limited space in our bags and, by design, we didn't want to buy much.

We learned about the threading work of Kashmir carpets. They are handmade and are so elaborate that some of the bigger ones take up to 10 years to be finished. This was humbling. In a world where everything seems instant—thanks to the Internet—the idea of spending 10 years doing the same exact thing—the movements of the threading work need to be very precise over and over—seems almost impossible.

![Hand made Kashmir carpet](../../files/images/india/652F21B3-0218-4DD4-8B04-69EBC15B90BA.jpg){style="width: 50.0%;display: block;margin-left: auto;margin-right: auto;"}

In one of these shops we also saw how marble and jewelry art is done. Workers first wash and polish the stones they're going to use, then they use certain manual cutting tools to break the stones into smaller pieces which then they manually cut into the tiniest of pieces. These pieces then go into carvings on the marble. One by one they assemble figures and scenes inside the marble. Here also, the patience and mastery of the tools were impressive. Some small pieces take only a couple of days to be made, but the large marble tables take months and even years.

![Detail of engraved jewels on Taj Mahal's walls](../../files/images/india/F937F15D-664E-4F0C-88DE-B1415E46F16B.jpg){style="width: 50.0%;display: block;margin-left: auto;margin-right: auto;"}

This is also how the carved art in the Taj Mahal and other palaces was made. It is one of those things that you need to see for yourself if you have the opportunity. Words and pictures don't do justice to them.

And then there are paintings. All over our tour, the guides and workers we met always reminded us that the Indian paintings are so durable because their colors are "all natural". They take big pride in explaining that they have been using the same techniques to create painting colors for centuries. Most of the colors come from combining certain stones, spices and leafs and this seems to make them more permanent and durable. While I am not sure if this is actually true or just a story artists tell to tourists to sell their work, their mastery was still impressive. We saw how they combined the colors and in one place, in Agra, we learned about the small brushes they use —brushes with one hair or thread only— to paint the tiniest of details.

Most of the art was very charged, very heavy on the eyes but somehow pleasing. From tigers to Buddhas to elephants, everything we saw revolved around mysticism and religion. Colors were bright and mixed, mostly to represent luck and wealth.

### Random Nuggets

During the 10 days that we spent in India there were some random bits that I learned about that turned out to be very fun and interesting.

#### "Use Diapers at Night”

We traveled a lot by car. One morning while we were going from Jaipur to Delhi I was staring outside the car window when I started noticing lots of trucks. The trucks had a variety of drawings and words painted on their sides and back and this vaguely reminded me of the religious art and wording that most buses have in Venezuela. But, as the car went on, I started noticing that most big trucks had the same phrase written in their back: "Use diapers at night”. This didn't make any sense to me but I was intrigued. I asked myself "why diapers?”, "how does this make any sense?”. It was not until after I saw some more trucks that I realized that I was reading everything wrong, the crucial word "diapers” was not there, "dippers” was the correct one.

Must trucks said "Use Dippers at Night”. Still, this didn't make much sense to me, so as soon as I got a signal on my cell phone I decided to [Google it](https://economictimes.indiatimes.com/can-tata-motors-wordplay-on-use-dipper-at-night-catchphrase-help-drivers-curb-hiv-in-india/articleshow/52850640.cms).

It turned out that the incidence of HIV amongst truck drivers used to be very high, mostly due to poor hygiene and lack of knowledge. So the TATA truck company partnered with a condom manufacturer and ran a marketing campaign called "use dippers at night”. They paid truck drivers to paint the slogan in their trucks. The idea was that this would make them more aware to use condoms. According to some statistics it seems to have worked out. The rate of HIV infections started to drop after the campaign started. Impressive!

#### Mumbai Buildings

Mumbai's architecture is chaotic, like the city. By looking around you can tell that the whole place is in constant transformation. Nothing ever seems to stop or end.

The city is very diverse in its constructions. You can find anything from NYC-style high-rise buildings to colonial-era houses. What caught my eye though, were some buildings that were decently high —about 15 stories I estimate— and had a very distinct architecture, no two were alike.

<center>
![E913FAB5-2C4B-4A81-9256-23F69434C59B.jpg](../../files/images/india/E913FAB5-2C4B-4A81-9256-23F69434C59B.jpg){width=40%}\ ![D2B7F251-0CED-4BF8-8C76-BB5DCF7B6A0E.jpg](../../files/images/india/D2B7F251-0CED-4BF8-8C76-BB5DCF7B6A0E.jpg){width=40%}

![7B94B33A-58CC-4EC3-BC59-C224B9E20B1F.jpg](../../files/images/india/7B94B33A-58CC-4EC3-BC59-C224B9E20B1F.jpg){width=40%}\ ![9D5970F6-8421-48C4-8A57-49ED5F4EAB82.jpg](../../files/images/india/9D5970F6-8421-48C4-8A57-49ED5F4EAB82.jpg){width=40%}
</center>


Most of these buildings featured very wide stories near the top with huge balconies overlooking the city. They reminded me of the Jetsons' homes: a single tube that acted as the elevator, leading to a very wide top where the actual homes were. The only difference with the Jetsons is that these Mumbai buildings can't go up and down to avoid the weather. Perhaps one day they will be able to.

I wonder if these constructions are a strategy to evade the noise and pollution of the city below them.

#### Baháʼí religion

![The Lotus Temple](../../files/images/india/6F2C0837-7EF4-431E-A05C-8FDD6F16A7C8.jpg)

One of the most random things we did during our trip was to visit the "Lotus Temple" in Delhi. This temple, built in the 1980s, is a place of worship for people that practice [Baháʼí Faith](https://en.wikipedia.org/wiki/Bah%C3%A1%CA%BC%C3%AD_Faith), a religion started in the 1800s "that teaches about the unity and equality of people" and brings together practices and rituals from almost all religions.

It was a weird site, and our guide was keen for us to see it. He wanted us to see something different and modern. The temple stood big, in the middle of a 20+ acre garden, its marble petals forming a Lotus flower. I didn't like it, it seemed fake and too grandiose.

Although we didn't go inside, because for that you had to listen to a tutorial about the religion and pay a fee, we were surprised to see so many visitors. All over the temple there were people taking pictures. It turned out that most of the people visiting don't practice "Bahá'ínism" but are there to see the Lotus as [this is a religious symbol in Hinduism and other religions](https://en.wikipedia.org/wiki/Sacred_lotus_in_religious_art).

#### White Cars

[Most cars in India are white.](https://economictimes.indiatimes.com/auto/why-1-in-every-2-cars-sold-in-india-is-painted-white/keeping-it-light-and-hot/slideshow/56808733.cms) According to the Internet there are some factors behind this:

- White reflects light. This helps keep the car cool during the summer months.
- White cars are easier to resell.
- White cars are cheapest compared to other cars of the same model.

This was almost a cultural shock because in Western countries, people don't usually stop to think about the color of the car, you choose what you like. Only in Venezuela I'd seen people discussing the light-reflecting benefits of a white car.


### Parting thoughts

We went to India for a wedding. I am grateful to my friends, Francisco and Rhea, for letting us experience a true Bollywood wedding. We even had the chance to dance and perform on stage. The food was outstanding, the venue was beautiful. It was a one of a lifetime experience. Most of all, it was awesome to reconnect with so many friends!

Thankfully our Indian experience didn't stop there. The wedding was just the beginning of a kaleidoscope of experiences. We had food that we never thought could taste so good, we visited places that felt almost magical and mystical. India's history is vast and diverse, it seems impossible to experience and visit it all.

But we also experienced the less nice parts of India. After all, it is a country that is in a struggle to develop and become a world power. Parts of it seem super cosmopolitan and modern —especially in Mumbai— while others seem to remain in an old century. I never saw such a contrast between rich and poor, between people that have it all and people that don't know what it is to have something. Poverty was in every corner we visited.

And, the weirdest part, was that we felt at home in so many ways. The bureaucracy, the traffic, the humor and positive attitude of most people reminded us of Venezuela. Even the roads and scenery outside of the cities brought back memories of childhood trips around my country. This was something I really didn't expect. Countries so far apart but, in some deep and strange manner, also very close and familiar.

I left with mixed feelings. When I'm asked if I'll ever go back, I don't have a clear answer. A part of me doesn't have an incentive to go back. The pollution was too much for me to bear and made everything less appealing. On the other side, I want to explore more, to keep visiting and learning about the culture, the lifestyle, the religions and, of course, the food. Luckily, I have more Indian friends, so perhaps I'll have other good excuses to go back at some point. And, if you ask me if you should go, I'd say yes. Everything about India is so different, yet familiar —I couldn't stop relating to scenes and places in Venezuela— that you should try to go, at least once in your life. Immerse yourself in the new and different and expand your horizons.

Lastly I'm grateful for my friends who got married and provided the perfect excuse for this trip and did everything they could to make us feel comfortable and at ease. We experienced India, and Mumbai in particular, in ways not common for tourists. I'm also thankful for the team at [\@wanderingthru](https://www.instagram.com/stories/wanderingthru/) who made the tour part of the trip an amazing experience. They took care of everything from start to end, it wouldn't have been the same without their planning and tips to go around.

Namaste,
