---
title:  My Favorite Books of 2019
created: 01/15/20
tags: books,reading
kind:      article
published: 2020-01-15
author:    Alberto Sadde
style:     post.css
---

![](https://cdn.pixabay.com/photo/2016/11/29/03/09/beach-1866992_960_720.jpg)

At the end of every year I try to rate the books I read in the past 12 months and revisit them to see what I learned from them and stuck with me.

Below are my favorite books of 2019. I mostly read to learn more about technology, learning and living better.

## [What Technology Wants](https://amzn.to/2Rh8inX) **by Kevin Kelly**

In this book Kevin Kelly explores the ideas of inevitability of progress and technology. It can be viewed as his philosophical treatise on the matter.

The most important concepts I got from the book are:

- Technium: this is the set of culture, history and technology that humanity has created. We can view it as an organism with its own life.
- Inevitability: Given the physical laws of the Universe are set there are only a finite number (even though very big) of possibilities at any point in time. This means that technological innovations are inevitable. If we replay history then, according to this theory, we will end up in roughly similar situations. There is a certain determinism to the Universe.

After reading it I found myself going back to the ideas of the book very often and started seeing many things through this lens. For example, in "Why We Sleep" Mathew Walker writes that

> When a theme repeats itself in evolution, and independently across unrelated lineages, it often signals a fundamental need.

We can replace fundamental by inevitable and we get Kelly's ideas.

## [Idea Makers](https://amzn.to/387RtCx) by Stephen Wolfram

Stephen Wolfram is one of the most accomplished scientists of our time and someone that we should all aspire to emulate. His life has been one of deep work and curiosity. Curiosity that has led him to create a new way of doing mathematics on computer and many other computational discoveries.

Idea Makers is a compilation of essays that Wolfram wrote about historical and modern characters that had an impact on his life and work. Each essay is an exploration into the mind and work of a particular character. I really enjoyed the chapters about Steve Jobs, Richard Crandall and Srinivasa Ramanujan (if you want to know more about Ramanujan check out *[The Man Who Knew Infinity](https://amzn.to/2urc4Ds)).*

To put it simply, always let curiosity guide you and enjoy the work that you do. If you don't then try to change. Life is short and there's no time to waste.

## [Range](https://amzn.to/387RtCx) by David Epstein

It is sobering and reassuring to read a well researched piece on the advantages of being a generalist. This book by David J Epstein argues that the normal and better path to a successful life is to be a jack of all trades, to have range, in other words, to be someone that can adapt quickly, change opinions in the face of data and let go of dogma and experience when new situations arise.

There is much to learn from these concepts especially in an era that values hyper-specialization in many fields. If you want to set yourself up for the future and become **antifragile** then you're better off learning multiple skills and subjects rather than siloing yourself in a very specific field.

## [Rules for Radicals](https://amzn.to/2snWJ5M) by Saul D. Alinsky

Being radical is good. So often we relate that word with something bad (terrorists are radical) but being radical, especially being a *radical realist* is one of the best things you can do. This book is about that. Understand where the world is, how it operates and try to change it despite everything

> That we accept the world as it is does not in any sense weaken our desire to change it into what we believe it should be – it is necessary to begin where the world is if we are going to change it to what we think it should be.

This book is radical in more than one way. The ideas that Alinsky presents can be taken as life lessons, not only lessons for organizing revolutionary movements.

This passage from the book is one that I've been pondering and re-reading since I first read it:

> **“If we think of the struggle as a climb up a mountain, then we must visualize a mountain with no top. We see a top, but when we finally reach it, the overcast rises and we find ourselves merely on a bluff. The mountain continues on up. Now we see the”real" top ahead of us, and strive for it, only to find we’ve reached another bluff, the top still above us. And so it goes on, interminably.

Knowing that the mountain has no top, that it is a perpetual quest from plateau to plateau, the question arises, “Why the struggle, the conflict, the heartbreak, the danger, the sacrifice. Why the constant climb?” Our answer is the same as that which a real mountain climber gives when he is asked why he does what he does. “Because it’s there.” Because life is there ahead of you and either one tests oneself in its challenges or huddles in the valleys in a dreamless day-to-day existence whose only purpose is the preservation of an illusory security and safety. The latter is what the vast majority of people choose to do, fearing the adventure into the unknown. Paradoxically, they give up the dream of what may lie ahead on the heights of tomorrow for a perpetual nightmare - an endless succession of days fearing the loss of a tenuous security."**

## [Good to Great](https://amzn.to/2tbax4g) by Jim Collins

Good to great attempts to distill the principles that make certain companies attain greatness in a sustained manner over the years. Collins and his team identified certain traits that all the *great* companies have in common.

The most notable one is the need for a **true leader**, a team player that has skin in the game, has no ego and is always a team player ready to take the blame for the ultimate fate of the company. Since I read this book I catalogue all companies I read about based on how their leadership and culture is.

Another concept that can be very well used for personal development is the **[flywheel effect](https://www.jimcollins.com/concepts/the-flywheel.html),** a virtuous cycle of steps that fuel each other step by step until momentum is big enough that the cycle takes care of itself. Collins found that all great companies identified their flywheel and worked always around it. In your personal life you can attempt and do the same. Identify your personal flywheel and work on it until it just spins on its own (and remember there's no magic formula, only hard and smart work).

## [Leonardo Da Vinci](https://amzn.to/2TBDaCv) by Walter Isaacson

This biography by Walter Isaacson is spectacular. We all know who Leonardo was but this book goes deep into his work like nothing I had read before. Leonardo was the ultimate tinkerer, scientist and hobbyist. His mind was child-like, always curious and always ready to be fascinated by the world. We all can learn from this. Keep a *beginner's mind.*

## [Extreme Ownership](https://amzn.to/36XGeN1) by Jocko Willink and Leif Babin

Focused on military leadership this book will teach you how to take responsibility for your actions in your own life. Go read it.

## [Stillness is the Key](https://amzn.to/35YNMxA) by Ryan Holiday

Holiday is one of my favorite authors, you can read more [here](http://aesadde.xyz/links#ryan-holiday).

The faster and farther we want to go, the slower and calm we need to be. Drawing from ancient wisdom from around the world —from the Romans to the Hindus and Chinese— Ryan argues that stillness is a keystone habit that we must cultivate in our lives. Only by keeping our heads cool, and finding pleasure in the small things, we will be able to live a better, more fulfilling life.

Finally, remember that what is important is not the number of books you read in a given period of time, but the quality of those books. It is better to revisit a few good books and re-read them. You never really read the same book twice, the content stays the same but you change over time and so does your perception and your reactions to the words.

