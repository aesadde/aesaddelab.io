---
title:  Random Fun 2
published: 07/07/19
tags: newsletter
kind:      article
author:    Alberto Sadde
style:     post.css
---

Hello folks!

Thanks for the feedback on the previous email.

Here we go again!

### What I am watching 📼

My parents and some friends suggested me to watch [Bolívar][bolivar], a series
about the life of Simón Bolívar. To my surprise the series is very historically
accurate and, barred some overly romantic  [*telenovela*][novela] scenes, very
entertaining and well produced. It even led me to read more about Venezuelan and
Latin American history. (If you have any book recommendations, please shoot me
an email). Be warned though, it is very long (more than 60 episodes) so you
might be glued to your TV for a while.

### Nerd Corner 🤓

In the last email I mentioned that I had set my phone to [grayscale][gray] in an
attempt to break free from my phone addiction.

While I am still using my phone a couple of hours a day (as tracked  by the
iPhone's [Screen Time app][screen]), after two weeks into this experiment I've
noticed some things:

- Staring at my phone's screen makes me sleepy, no matter the time of the day.
- Some photos look much cooler this way.
- But most Instagram pictures look exactly the same. "Stories", in particular,
    lost all their appeal and I don't click on them that often.
- Some apps don't really work in grayscale. For example, in the Google Maps
    app all roads look the same to me while navigating. I've made some
    wrong turns while driving recently 😅.
- I pretty much stopped watching videos on the phone, they all look boring.
- When browsing the Internet I can concentrate more on text since all the
    other graphics look flat and boring.

Overall I feel that I use my phone much less but I am not convinced this is
a sustainable approach. I'll experiment with more ways to cut my screen time in
the following weeks, including playing with [Mark Manson's attention
diet][diet].

### Also interesting 🤯

- This [Twitter thread][perro] about a "stray" dog made me laugh for a good while
😂
- The Silicon Valley is one big bubble. People often communicate using terms that
  outside of here don't make sense. This [article][sv] explains most of
  these terms. Be warned, some might sound silly and weird.

- I'm always trying to do more with less. This ["Minimalist" workout
  routine][workout] looks promising for quick workouts a couple of times a week.
- If you're looking for summer reading ideas, [this huge list][list] is for you.
  I've picked up a few books from it already!


<!-- Links -->
[bolivar]: <https://www.netflix.com/title/80220422>
[iphone]: ./assets/iphone.PNG{ height=100px }
[perro]: <https://twitter.com/manadigiur/status/1144773064485408768>
[sv]: <https://www.theguardian.com/us-news/2019/jun/26/how-to-speak-silicon-valley-decoding-tech-bros-from-microdosing-to-privacy>
[novela]: <https://en.wikipedia.org/wiki/Telenovela>
[diet]: <https://markmanson.net/attention-diet>
[workout]:
<https://getpocket.com/explore/item/the-minimalist-s-strength-workout>
[gray]: <https://gogray.today/#go-gray>
[list]: <https://ideas.ted.com/teds-giant-summer-reading-list-151-books-to-dive-into-right-now/>
[screen]: <https://support.apple.com/en-us/HT208982>
