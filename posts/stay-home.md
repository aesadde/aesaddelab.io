---
title: "Stay Home"
subtitle:  "How to stick to your routine during lockdown"
created: 2020-03-23
tags: routines, hobbies
kind: article
published: 2020-03-23
author: Alberto Sadde
style: post.css
---

![](https://images.unsplash.com/photo-1577548237604-a930065ac1ae?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80)

The coronavirus pandemic has brought unprecedented change to many of us. What once was a dream—that of working from home whenever we pleased—has turned into a nightmare of sorts. All over the Internet people are complaining that they are eating all their supplies and will be fatter by the time the quarantine ends. Netflix usage has gone through the roof to the point that [the EU government has asked the company to lower the quality](https://techcrunch.com/2020/03/19/keep-calm-and-switch-to-sd/) of the streams to reduce the strain on the networks. Many are stuck in this vicious cycle and are blaming the current pandemic for it.

I've also been hit by this new situation. Today marks my 16th day at home. I've had ups and downs, like most of you I'm sure. During the first week of my lockdown, I struggled. I overslept a few days and struggled to keep a consistent routine of work and exercise. But, since California has a “shelter-in-place” mandate until at least the 7th of April I thought it better to adapt to this new reality. I decided to use this time at home to my advantage. After roughly two weeks, I'm starting to see results.

Here's how I did it.

First, we must internalize this new normal, however long it lasts. The current pandemic has upended our lives. Some have it worse than others but everybody has been affected by it. My new normal is to wake up, sit down at a desk to work—and socialize— over Zoom calls, eat, and repeat day after day. But we can't let the pandemic overtake everything. We must use this time to keep building our habits and our routines. Now, more than ever, we need to be constant and consistent. After all, as the stoics said, it is under stressful times that our true character comes to light. Seneca, the ancient philosopher, would warn us that "**if you are lucky enough to never experience any sort of adversity, we won't know how resilient you are”.** We should take this time to see how much we can improve and learn.

> "We must use to our favor our enemy's forces."

### Routines

I'm sure you had some sort of routine before the lockdown. You probably woke up roughly at the same time, ate at the same times and worked for the same amount of time. Rather than completely get rid of your routine, you should try to stick to it and adapt it where it's relevant. If your routine involved something that you can't do anymore like getting your coffee at your local coffee shop or going to the gym before going to work, try to replace it or find a creative way to go around it. My routine before the lockdown looked roughly like this:

- 6 - 6.10 Wake up, drink water, take vitamins.
- 15-20 min Meditation
- 30 min Writing
- 10 min Brew coffee
- Freshen up, get dressed
- 30-35 min Commute to work
- Work, work, work
- 30-35 min Commute from work
- 30-45 min Run
- Cook and eat dinner. Relax, Read.
- 10.30 - 11 PM Go to bed.

Because of the lockdown, I am not commuting to work anymore so I had to adjust my schedule. I struggled with the changes for a couple of days but I am back on track now:

- 7 - 7.10 Wake up, drink water, take vitamins.
- 15-20 min Meditation
- 30 min - 60 min Writing
- 10 min Brew coffee
- Freshen up, get dressed
- Work, work, work
- 30-45 min run
- Cook and eat dinner. Relax, Read.
- 11 - 11.30 PM Go to bed.

Since I save around an hour a day by not having to commute to and from work, I've changed my bed/wake up time to a later time. I've found this to be positive since I've been staying up late working on my projects, reading and helping at [Code For Venezuela](https://codeforvenezuela.org). The first week of the epidemic in Venezuela was particularly stressful and we worked around the clock with other to launch a [chatbot service and other tools for Medicos Por La Salud](https://www.codeforvenezuela.org/covid19bot).

### **The Internet**

If there's one thing we can be grateful for during this pandemic, it is the Internet. While we might need to be physically distant from one another, we don't need to be "socially" distant. Technology is our friend. We can build peer groups to hold us accountable and get the human support that we need during these times of social isolation.

I am a living example of this.I've started SPAR! once again. SPAR! is an app that lets you challenge a group of friends to do an activity. You must record daily or weekly check-ins or risk paying a penalty for every missed update. We are in the middle of doing max push-ups for 60 seconds every day and it's been working wonders. I hope to keep up the momentum even after the pandemic ends. Last year, thanks to SPAR! I recorded more than a hundred days doing push-ups!

Other than SPAR! I've also built a “writing accountability” group with some friends from Write Of Passage. We've been meeting over Zoom for one hour every morning to write. We write for roughly 50 minutes and then comment on how we felt and discuss our work. I feel I've improved more in the past week than in the last 6 months of writing.

The coronavirus has accelerated the rate at which services and activities move online. So if your salsa class moved to a Zoom call, join it. Forget about the fear of looking ridiculous dancing alone in front of a camera. Embrace the moment and keep learning. We can't let the situation change our routines and mentality.

### **Side Projects and Hobbies**

If you're stuck at home and find yourself with more free time on your hands, this is the opportunity to work on your hobbies and side projects. You don't have an excuse not to. Simply forget about plans and start doing. On my end, I've been reading and writing more. There are always books to read, so there's no excuse to pick a new one and get started.

I've also been diving deep into [my home automation project](https://twitter.com/aesadde/status/1141060454292156416?s=20). I recently got a Raspberry Pi and migrated all my automation services there. On top of this, I have also been slowly changing the light switches and light bulbs to automate them and control them via an app 🤓.

### **Family**

Finally, take the time to connect with your family and friends. I've spoken to more friends these past few weeks than I had in the past few years. I've reconnected with many High School friends and college mates. And I also talk with my parents a couple of times per day. We are all bored at home with some more free time.

We are all in this together so reach out and take care of one another.
