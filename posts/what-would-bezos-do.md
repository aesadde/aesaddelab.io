---
title: "What Would Bezos Do?"
subtitle:
created: 2020-08-13
tags: mental models, decisions
kind: article
published: 2020-08-13
author: Alberto Sadde
style: post.css
---

You've probably heard people say "No regrets!" when their decision didn't turn out the way they wanted. This is complete BS. Of course, there were regrets. Otherwise, they wouldn't be thinking about them.

**What people need is a systematic way to minimize regrets.**

We tend to optimize for our present selves. But it's our future selves who will deal with the consequences—and regrets—of our current decisions. In the present, there's always FOMO—the fear of missing out. We have so much choice, and we tend to overcommit. Regrets are inevitable.

I once committed myself to three different projects and a camping trip all at the same time. I felt like Jim Carey in the movie Yes Man!

<br>
<center>
![](https://media.giphy.com/media/jQ7LXZHOawLvujQuR5/giphy.gif){height=30%}
</center>
<br>

Even ultra-successful people like Sam Altman—former YC president— [agree](https://blog.samaltman.com/the-days-are-long-but-the-decades-are-short):

> *Regret is the worst, and most people regret far more things they didn't do than things they did do.*

So, how do we minimize regrets? Turn the problem upside down.

[Nassim Taleb](https://aesadde.xyz/links/#nassim-taleb) says that the best way to make decisions is by elimination, a process he calls *via negativa*. Rather than thinking, "What do I want to do now?", think, "What will I regret in the future if I **don't do** now?"

<br>
<center>
![](https://media.giphy.com/media/39xDerRCIoX2WeUVBz/giphy.gif){width=50%}
</center>
<br>


Taleb put into words what Amazon CEO Jeff Bezos explained in [a 1995 interview.](https://www.youtube.com/watch?v=jwG_qR6XmDQ) When he was deciding between starting Amazon or staying in his comfortable Wall Street Job, Bezos devised a process, **"the regret minimization framework.”** It helps him make important decisions in three simple steps:

- First, project yourself into the future. Think like your future self in 10, 20, 60 years.
- Second, think about the decision with respect to regrets. Do you regret not having made that decision, or gone through that experience?
- Finally, based on your answer, come back to the present and act accordingly.

Using this principle every time we face a decision allows us to turn FOMO into JOMO, the joy of missing out. The regret minimization framework is the right tool to disengage our autopilot and optimize our decisions for our future selves.

By using it, we can start saying yes to the things that we care about, and jump into projects without doubts, much like [Haruki Murakami](https://aesadde.xyz/books/2020-02-17-What-I-Talk-About-When-I-Talk-About-Running/) did when he decided to become a writer:

> I *had to give everything I had. If I failed, I could accept that. But I knew that if I did things half-heartedly and they didn’t work out, I’d always have regrets.*

Now, when faced with lots of options and new opportunities, I try to think like Bezos. Will 90-year-old me look back and regret starting too many projects? Will I regret not giving it all in one project because  had to spare energy for the other ones?

So, when faced with your next decision, think, **“What would Bezos do?”**

You won’t regret it!
