---
title: Nerd Corner
style: join.css
description: "Nerd Corner"
---

## Nerd Corner 🤓

In early 2019 I decided to experiment with writing and new ways of connecting with people. Nerd Corner was born.

Nerd Corner is not your usual “hey this is what I'm reading and listening”. Each week the objective is to really *nerd out* on different topics, connect ideas, and learn.

Each email is split into three sections:

1. What I’m reading: a quick summary of what I’ve been reading. Reading is my main funnel to get new insights and ideas. Past editions include notes and summaries of books, articles, and essays on which I expand more

2. Nerd Corner: the main section of the mail. Here I share my thoughts and findings of different topics. This is my space to explore with you timeless ideas, new scientific ideas, or just cool gadgets, apps, and computer tricks!

3. Cool Finds: The Internet is huge! In this section, I do my best to sift through the noise and uncover three very cool links. These can range from news articles, essays, to videos, Twitter, and Instagram posts!

In any given Nerd Corner edition you might find some new research on food technology, a Twitter thread on complexity theory, a vintage topographic map of Hawaii, a review of a cool meditation app, who knows! There's lots of really interesting stuff out there. My goal is to uncover the best of the best, get new insights, and share them with you!

<center>
⬇️ Subscribe below ⬇️
</center>

<center>
<iframe src="https://aesadde.substack.com/embed" width="512" height="250" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe>
</center>


## Why “Nerd Corner”?

According to the Oxford English Dictionary, a nerd is "a single-minded expert in a particular technical field: a computer nerd.” While the origin of the word is unknown, we know it originated around the 1950s with the advent of the first computers.

I’m really passionate about computers, science, and technology. While I’m not a “single-minded expert” in any of these fields, I love the idea of having a weekly slot devoted to learning and uncovering new ideas.

Simply put, Nerd Corner is my excuse to learn more about widely different topics, improve my writing, clarify my ideas, and connect with the most interesting people on the Internet.
