---
title:  01
created: 08/21/19
source:  2019-08-21-Uncommon-Grounds
tags: quotes, religion, cofee
---

"The voodoo priest and all his powders were as nothing compared to espresso,
cappuccino, and mocha, which are stronger than all the religions of the world
combined, and perhaps stronger than the human soul itself."

- Mark Helprin
