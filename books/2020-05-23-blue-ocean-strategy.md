---
title: "Blue Ocean Strategy"
subtitle: How to Create Uncontested Market Space and Make the Competition Irrelevant
created: 05/23/2020
started: 04/13/2020
ended: 04/24/2020
author: W. Chan Kim and Renée Mauborgne
publisher: Harvard Business School Press
rating: 5/10
thumbnail: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.ethos3.com%2Fwp-content%2Fuploads%2F2014%2F07%2FBlue-Ocean-Strategy.jpg&f=1&nofb=1
link:
blurb: >
  A thorough study of why and how companies should create their own markets and escape from the competitive “red oceans”.
---

### Overview

Blue ocean: a new market space that hasn’t been created yet. If you create you will own it and dominate it by definition.

Red ocean: The known market space. This is were most companies find themselves competing in.

If you want to become a successful company then you must create your own blue ocean and escape competition.

### Summary

The book is divided in three parts:

1. Blue Ocean Strategy
2. Formulating Blue Ocean Strategy
3. Executing Blue Ocean Strategy

Part (1) defines what blue oceans are and uses Cirque du Soleil as the main example to explain it. Cirque du Soleil succeeded because in the world of circuses it didn’t try to compete with others but saw an opportunity in creating a complete new market by getting rid of performance animals, and targeting a different audience than traditional circuses.

Useful figure:

<center>
![](../../files/images/blue-ocean-1.png)
</center>

Part (2) is all about formulating your Blue Ocean Strategy. The example used in this case is [[yellow tail]](https://en.wikipedia.org/wiki/Yellow_Tail_(wine)), an Australian wine company that by simplifying its wine offering and focusing on only a few types dominated the US American wine market for a long time.

The key here is to focus on the strengths you already have, look across your market boundaries, and identify how you can provide value to new customers.

Above all understand that

> Effective blue ocean strategy should be about risk minimization and not risk taking.

Useful figure:

<center>
![](../../files/images/blue-ocean-2.jpg)
</center>

Part (3) deals with the more practical aspects of going from planning to action. It goes into a lot of details of how leaders and teams can align to execute the new strategy.

### (Incomplete) List of Blue Ocean Companies

- **Cirque Du Soleil:** created a new market in the entertainment industry. Circuses stopped being kids-focused.
- **[yellow tail]**: simplified the wine offering, became one of the most popular wine sellers in the US. Impossible to miss their yellow kangaroo logo when buying wines at a store.
- **Novo Nordisk**: Stopped focusing on doctors, and started focusing on users (patients). Created the NovoPen for diabetics.
- **NABI:** (a then) Hungarian bus company. Focused on public transport. Created better and more efficient buses that city councils afford.
- **Borders and Barnes and Noble**: added cafés and a whole new experience to the bookstores. Borders went bankrupt circa 2010.
- **Swatch:** made radical changes to their production methods. Created cheap and fashionable high-quality Swiss watches.

### Final Thoughts

Together with “Build to Last” by Jim Colins, this book represents one of the earliest attempts at successfully study how companies can become the leaders in a particular market not by beating the competition but by creating a completely new market that they can dominate entirely.

Thinking in terms of blue oceans vs red oceans is useful to understand what your offering is and should drive your company’s strategy.

Another important point that should have been explained more in depth is that of *value innovation*. The point is that innovation for the sake of it alone is not enough. Customers don’t care if you’re using the latest technology or creating some innovative tech if you’re not providing value to them.

> Unless technology makes buyers’ lives dramatically simpler, more convenient, more productive, less risky, or more fun and fashionable, it will not attract the masses no matter how many awards it wins.

At the time of publishing, most of the ideas in this book were revolutionary. But with the benefit of hindsight there are now better alternatives to learn about companies and their strategies:

- Zero to One by Peter Thiel
- [Good to Great by Jim Collins](https://aesadde.xyz/books/2019-07-21-Good-To-Great/)

