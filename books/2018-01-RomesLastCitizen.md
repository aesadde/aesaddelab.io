---
title: Rome's Last Citizen
subtitle: The life and legacy of Cato, mortal enemy of Caesar
author: Rob Goodman and Jimmy Soni
started: 01/31/2018
ended: 02/19/2018
publisher:
rating: 6/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51BPsHCs7yL._SX328_BO1,204,203,200_.jpg
link:
blurb: >
    Goodman and Soni are some of my favorite biographers. Here they recount the life of Cato.
    It does a good job in depicting the man and the morals and also Rome at that time.
---

I picked of this book after reading Goodman's and Soni's latest biography "A
Mind at Play" that tells the story fo Claude Shannon, one of the greatest minds
in the 20th century.  I enjoyed it very much. Many historical notes were
already familiar to me since we studied this period very thoroughly in Italian
school.

Overall, Cato comes across as a strange character, stoic on some ways, very
much flawed as any other human in other ways.

What follows are a list of highlights from the book that resonated with me.
There are many notes related to Politics, Socialism, Populism, etc. since they
are still relevant today and many things are similar to current events in
Venezuela.

