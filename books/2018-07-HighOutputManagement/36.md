---
title:  36
created: 11/05/18
source:  2018-07-HighOutputManagement
categories: one on one, meetings
---

<!-- Front -->

I feel that a one-on-one should last an hour at a minimum. Anything less, in my
experience, tends to make the subordinate confine himself to simple things that
can be handle quickly.

<!-- Back -->

