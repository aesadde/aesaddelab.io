---
title:  33
created: 11/05/18
source:  2018-07-HighOutputManagement
categories: delegation
---

<!-- Front -->

Delegation  without follow-through is *abdication*. You can never wash your
hands of a task. Even after you delegate it, you are still responsible for its
accomplishment, and monitoring the delegated task is the only practical way for
you to ensure a result.

<!-- Back -->

