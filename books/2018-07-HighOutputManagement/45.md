---
title:  45
created: 11/05/18
source:  2018-07-HighOutputManagement
categories: planning, decisions
---

<!-- Front -->

We should also be careful not to plan too frequently, allowing ourselves time
to judge the impact of the decisions we made and to determine whether our
decisions were on the right track or not.

<!-- Back -->

