---
title:  22
created: 11/05/18
source:  2018-07-HighOutputManagement
categories: recruiting, hiring
---

<!-- Front -->

Most companies recruit new college graduates to fill anticipated needs--rather
than recruiting only when a need develops, which would be foolish because
college graduates are turned out in a highly seasonal fashion.

<!-- Back -->

