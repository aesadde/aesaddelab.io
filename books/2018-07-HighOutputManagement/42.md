---
title:  42
created: 11/05/18
source:  2018-07-HighOutputManagement
categories: communication, fear
---

<!-- Front -->

One thing that paralyzes both knowledge and position power possessors is the
fear of simply *sounding dumb*. For the senior person, this is likely to keep
him from asking the questions he should ask. The same fear will make other
participants merely think their thoughts privately rather than articulate them
for all to hear; at best they will whisper what they have to say to a neighbor.

<!-- Back -->

