---
title:  16
created: 11/05/18
source:  2018-07-HighOutputManagement
categories: rules, testing,
---

<!-- Front -->

A common rule we should always try to heed is to detect and fix any problem in
a production process at the *lowest-value* stage possible.

<!-- Back -->

