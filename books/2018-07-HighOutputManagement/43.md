---
title:  43
created: 11/05/18
source:  2018-07-HighOutputManagement
categories: decisions, managers
---

<!-- Front -->

What decision needs to be made?

When does it have to be made?

Who will decide?

Who will need to be consulted prior to making the decision?

Who will ratify or veto the decision?

Who will need to be informed of the decision?

<!-- Back -->

