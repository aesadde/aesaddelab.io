---
title:  53
created: 11/05/18
source:  2018-07-HighOutputManagement
categories: incentives, motivation
---

<!-- Front -->

Perhaps the emergence of the new, humanistic approaches to motivation can be
traced to the declines in the relative importance of manual labor and the
corresponding rise in the importance of so-called knowledge workers.

Motivation is closely tied to the idea of *needs*, which cause people to have
*drives*, which in turn result in *motivation*.

<!-- Back -->

