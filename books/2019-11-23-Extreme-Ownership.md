---
title: Extreme Ownership
subtitle: How U.S. Navy SEALs Lead and Win
started: 10/25/19
ended: 11/04/2019
author: Jocko Willink and Leif Babin
kind: book
rating: 7/10
blurb: >
  Leadership principles from the battlefield. Willink and Babin fought in
  Iraq. They share their hard-tested lessons from the war and how they can be
  applied to business
thumbnail: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.upkeepmedia.com%2Fwp-content%2Fuploads%2F2019%2F02%2FExtreme-Ownership-by-Jocko-Willink-200x300.jpg&f=1&nofb=1
---

If you only have a few minutes, check out the section I wrote about this book in [one of my weekly emails](https://us3.campaign-archive.com/?e=&u=3d9e1a5fac6e2b16b24b4808a&id=80d3b1162e)!

The book is about practical lessons on leadership drawn from the experiences both Willink and Babin had as Navy SEALs deployed in Iraq for many years. Each chapter starts with an anecdote from their time in Iraq to showcase a leadership principle that they then apply to real businesses that they've helped through their company Echelon Front, a consulting business that provides leadership advice to companies.

The book is very actionable. There are many lessons we can all learn from. The book also presents a refreshing and sobering view about the Iraq war. Regardless of what one might think about the war itself, going to a far away land and put your life at risk every day is not an easy feat.

## Responsibility

The central idea of the book is about responsibility. When one is in a leadership position, taking ownership of everything that happens is necessary for the team or the organization to operate. Everything starts and ends with leaders. In war this is key to survive, for us at home and at work taking ownership of our actions will lead to better outcomes, less confusion and more output over the long run.

>These leaders cast no blame. They made no excuses. Instead of complaining about challenges or setbacks, they developed solutions and solved problems. They leveraged assets, relationships, and resources to get the job done. Their own egos took a back seat to the mission and their troops. These leaders truly led.

>Taking ownership for mistakes and failures is hard. But doing so is key to learning, to developing solutions, and, ultimately, to victory. Those who successfully implement these principles run circles around the rest of the world.

>Leaders must own everything in their world. There is no one else to blame

>I was responsible for everything in Task Unit Bruiser. I had to take complete ownership of what went wrong. That is what a leader does—even if it means getting fired. If anyone was to be blamed and fired for what happened, let it be me.

>The leader is truly and ultimately responsible for everything.

>On any team, in any organization, all responsibility for success and failure rests with the leader. The leader must own everything in his or her world. There is no one else to blame. The leader must acknowledge mistakes and admit failures, take ownership of them, and develop a plan to win.

## Ego

Coupled with the idea of taking ownership is the need for removing Ego from the equation. In order to be a true leader, Willink and Babin argue, one must check the ego.
This goes very well with Ryan Holiday's book Ego Is The Enemy.

>The best leaders checked their egos, accepted blame, sought out constructive criticism, and took detailed notes for improvement.

>They are silent professionals and seek no recognition. We take this solemn responsibility to protect them with the utmost seriousness.

>Our egos don't like to take blame. But it's on us as leaders to see where we failed to communicate effectively and help our troops clearly understand what their roles and responsibilities are and how their actions impact the bigger strategic picture.

>Implementing Extreme Ownership requires checking your ego and operating with a high degree of humility. Admitting mistakes, taking ownership, and developing a plan to overcome challenges are integral to any successful team.

## Trust

Exceptional teams not only have a leader that takes extreme ownership, they also must build trust and must feel empowered to make decisions. This is what Willink and Babin call Decentralized Command. Only by building trust and demanding the highest standards from all team members, it is that teams can be fast and effective. In war this can be the difference between life and death. In business it can give you a competitive advantage. A tightly coupled team move faster and works better.

>Each member demanded the highest performance from the others. Repetitive exceptional performance became a habit. Each individual knew what they needed to do to win and did it. They no longer needed explicit direction from a leader.

## Communication

We've all be in a situation that ended badly because of a lack of communication. Many times leaders believe they are communicating properly but they don't stop to listen to the team. In war communication issues can very quickly lead to death. So it is no wonder that Willink and Babin spend quite some time discussing how they learned this and how the implemented effective communication strategies in their Task Unit.

>In order to convince and inspire others to follow and accomplish a mission, a leader must be a true believer in the mission.

>In many cases, the leader must align his thoughts and vision to that of the mission. Once a leader believes in the mission, that belief shines through to those below and above in the chain of command. Actions and words reflect belief with a clear confidence and self-assuredness that is not possible when belief is in doubt.

>If you don't understand or believe in the decisions coming down from your leadership, it is up to you to ask questions until you understand how and why those decisions are being made. Not knowing the why prohibits you from believing in the mission.

>If your boss isn't making a decision in a timely manner or providing necessary support for you and your team, don't blame the boss. First, blame yourself. Examine what you can do to better convey the critical information for decisions to be made and support allocated.

In summary, there are many parallels between business and military teams. The stakes might not be as high in business but cultivating the right mindset and the correct behaviors can mean all the difference.

## Related

* Ego is the Enemy by Ryan Holiday
* [Good to Great By Jim Collins](./2019-07-21-Good-To-Great.html)
