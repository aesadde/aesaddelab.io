---
started: 06/11/19
ended: 06/11/19
title:  How to Live on Twenty Four Hours a Day
subtitle:
author: Arnold Bennett
publisher: Franklin Classics
rating: 7/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51VY1VuQ5eL.jpg
link:
blurb: >
    A short self-help book from the early 1900s. Some ideas and human challenges are timeless.
    "History doesn't repeat itsefl, but it rhymes".
---

This short book, written in 1908 tries to explain and distinguish between simply
"existing" and actually *living*. Bennett argues for a more deliberate use of
our time if we really want to live.

What struck me is how similar life today is to life in the early 1900s so much
of the advice in the book is still relevant today. History is cyclical the
stoics might say...
