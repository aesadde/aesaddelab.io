---
title: "Seeking Wisdom"
subtitle: From Darwing To Munger
created: 06/24/2020
started: 04/25/2020
ended:  05/21/2020
author: Peter Bevelin
publisher: Post Scriptum AB
rating: 10/10
thumbnail: https://cdn.shopify.com/s/files/1/0074/1094/2067/articles/61qbFGP2sFL-1_1600x.jpg?v=1539054419
link:
blurb: >
  This is a book about tools and mental models for better thinking. Inspired by the wisdom of Charles Munger and Warren Buffett, the books is a very good attempt at condensing timeless wisdom in a readable, and actionable manner. It changed my life.
---

### **Overview**

Evolution didn’t prepare us for that the world is now today. For most of our history, we’ve been hunter-gatherers. So, we still have hardwired in us many of the mechanisms that made us survive in ancient and wild environments. But we lack the mental models and adaptations needed not only to survive but to thrive in the modern world filled with numbers and information that we find ourselves in.

The book is Bevelin’s quest to improve his thinking process. He was clearly inspired by Charles Munger and Warren Buffett and uses them as guides, quoting, and using them as examples throughout the book. But to think that the book is only about these two characters is wrong. Most pages condense lots of wisdom from all ages.

By attempting to learn how to improve our thinking process and what influences our thoughts and decisions, Bevelin wrote a masterpiece on decision-making.

### **WE ARE WHAT WE THINK**

The first part of the book is devoted to understanding how our brains work and how our environment, evolution, and our own thoughts influence our thinking process. This quote summarizes it well:

> *If we learn to understand what works and what doesn’t work and find some framework for reasoning, we will make better judgements.* ***We can’t eliminate mistakes, but we can prevent those that can really hurt us**.*

The key lessons from this part are:

- To understand that we are what we think. As Marcus Aurelius said, *“Our life is what our thoughts make it.”*
- We are hardwired to avoid harm first. Fear is a very powerful emotion. In the past, it served us well to flee from the jaws of a predator. But in modern times we must realize that this is just the first reaction. If we want to think better we need to get past our initial emotions and think.
- Cultural evolution is as important as “genetic” or Darwinian evolution. Cultural evolution allows us to learn and pass to the next generation not only what our DNA encodes but what we have learned collectively. The amount of information and feedback is much greater. As Bevelin suggests, *"Unlike biological evolution, cultural evolution is not inherited. We don’t inherit our parents’ habits. We learn from them."*

    Rather than repeating our ancestors’ mistakes we must learn from History and improve.

### **BIASES**

Armed with examples and guided by real-life examples (mostly) from Charles Munger and Warren Buffet, Bevelin makes an exhaustive list of psychological biases and explains the ways in which we misguide ourselves into thinking incorrectly. For each bias, he also offers guidelines to avoid them and questions that we should ask ourselves when we face those situations.

In all Bevelin describes 28 biases some of which I highlight below:

1. **Mere Association**.

    We don’t see things as they are but as we *feel* they are.

    Also, “Don’t shoot the messenger”. The carrier of bad news is not the source of bad news.

2. **Rewards and punishment**.

    Understand how rewards and incentives work.

    > *"We do not improve the man we hang; we improve others by them"*

3. **Incentives**. Always understand what really drives people.

    > “Never ask the village barber if you need a haircut”.

    > "My doctor gave me six months to live. When I told him I couldn’t pay the bill, he gave me six more months."

4. **Self-serving bias and over-optimism**.

    > “An optimist is a person who sees a green light everywhere, while the pessimist sees only the red stoplight. The truly wise person is colorblind” - Dr. Albert Schweitzer.

    > “It is remarkable how much long-term advantage people like us have gotten by trying to be consistently not stupid, instead of trying to be very intelligent. There must be some wisdom in the folk saying: 'it’s the strong swimmers that drown'." - Charlie Munger

5. **Self-deception and denial**.

    > “…their judgement was based more upon blind wishing than upon any sound prediction; for it is a habit of mankind to entrust to careless hope what they long for, and to use sovereign reason to thrust aside what they do not desire.” - Thucydides, Ancient Greek historian

6. **Consistency.**

    Our reputation matters we often keep course just because we said so even when the situation demands change.

    > "The more public a decisions is, the less likely it is that we will change it.”

    > “There is nothing wrong with changing a plan when the situation has changed” - Seneca

7. **Deprival syndrome.**
Prohibition has the opposite effect.

    > “To forbid us something is to make us want it” - Michel de Montaigne

8. **Status quo - Do nothing syndrome.**

    The paradox of optionality is that in the face of too many options we often opt to do nothing. This is decision paralysis.

    > "The more emotional a decision is or the more choices we have, the more we prefer the status quo.”

9. **Impatience.**

[“Impatience with action, patience with results” - Naval](https://aesadde.xyz/posts/patience/)

    > “I conceive that pleasures are to be avoided if greater pains be the consequence, and pains to be coveted that will terminate in greater pleasures.” - Michel de Montaigne

10. **Envy and jealousy.**

    Keep in mind “We envy those who are near us in time, place, age or reputation.”

11. **Distortion by contrast comparison.**

    “We judge stimuli by differences and changes not absolute magnitudes”

12. **Anchoring.**

    We often unknowingly use certain values as *anchors* for future calculations.

    Adapt the information you are working with to the reality you’re in.

13. **Over-influence by recency**

    > "Drama and danger sells. **The media capitalizes on fear because there is money in it**. Major accidents, such as airplane crashes or shark attacks, grab people’s attention and make headlines regardless of their probability."

14. **Omission.** 

    Always consider alternative explanations and look for the missing information.

15. **Reciprocation.**

    > "Example is better than law. For where the laws govern, the people are shameless in evading punishment. But where example governs, the people have a sense of shame and improve” - Confucius

16. **Liking**

    > “Love your enemies for they will tell you your failures” - Benjamin Franklin

17. **Social proof**

    We need to think for ourselves.

    > “It is easy in the world to live after the world’s opinion; it is easy in solitude to live after our own; but **the great man is he who in the midst of the crowd keeps with perfect sweetness the independence of solitude**”. - Ralph Waldo Emerson

18. **Authority**

    > “In questions of science, the authority of a thousand is. not worth the humble reasoning of a single individual” - Galileo Galilei

    > “**Techniques shrouded in mystery clearly have value to the purveyor of investment advice.** After all, what witch doctor has ever achieved fame and fortune by simply advising 'Take two aspirins?’” - Warren Buffett

19. **Sensemaking**

    The best example of this is the Hindsight bias.

    > “Everything seems stupid when it fails.” - Fyodor Dostoevsky

    In hindsight, everything seems obvious. But we should look at earlier decisions in the context of their own time.

20. **Reason-respecting**

    We favor explanations and information. You can get ahead by simply attaching a reason (even if wrong or inexistent) to a favor you ask.

    > "We want explanations and the word “because” imply an explanation." - Bevelin

21. **Believing first and doubting later**

    We are prone to believe something when we first hear it, especially if the messenger seems trustworthy or comes from a position of authority.

    Try to always be skeptical: doubt first and believe later, only if there’s evidence supporting the argument.

22. **Limitations of Memory**

    Our memories are not fail proof. We can’t trust them to base our decisions on them. If you want to remember something better practice it. Learning is an activity.

    Aristotle said:

    > “For the things we have to learn before we can do them, we learn by doing them.” This means we need to practice what we have learnt in various situations.

23. **Do-something syndrome**

    There’s a difference between being effective and efficient. Always favor the first, being efficient or good at something unnecessary is stupid.

    Henry David Thoreau said:

    > “It is not enough to be busy: so are the ants. The question is: What are we busy about?”

    Don’t confuse activity with results. There is no reason to do a good job with something you shouldn’t do in the first place.

24. **Confusion and say-something syndrome**

    We often have an urge to say something, to fill in the empty space. Avoid this. It is better to not say anything than to say something stupid. Think then act.

    > “We have two ears and one mouth so that we can listen twice as much as we speak.” ― Epictetus

### **WE’RE BAD WITH NUMBERS**

Bevelin continues to explore how we are fooled by reality and how we can better think and reduce the mistakes we make.

Mathematics and physics are two fundamental tools that help in this regard.

First, we must understand that numbers without context don’t mean much. Everything depends on scales, sizes, and limits. Second, we must learn about inversion. Many problems are best solved backward or bottom-up rather than top-down.

The best analogy for this comes from World War II. Allied airplanes were being taken down by German guns in disproportionate amounts, clearly, there was a failure in the airplanes. The solution: “Look at where the bullet holes are and put extra armor every place else”. This is inversion in action. Clearly, the airplanes that were not returning were being shot in different parts than those who did make it back.

Third, we’re bad with probabilities and randomness. We mistake the probable for the possible. Or, as Benjamin Franklin said: “He that waits upon fortune, is never sure of a dinner.”

Fourth, understand that small changes in conditions can result in widely different outcomes. This is the core of chaos theory—“the butterfly effect”—and why, for example, weather forecasting is hard. Tiny changes in the conditions of a system can have disproportionate consequences. We are not wired to deal with these huge changes.

### **HOW TO THINK BETTER**

Bevelin devotes the last part of the book to advice on how we can learn to do fewer mistakes, avoid falling trap of the mentioned psychological biases, and deal with randomness and probabilities.

The key, Bevelin points out, is not trying to learn to avoid mistakes, that’s impossible. The objective must always be to learn to make fewer mistakes. And, it turns out the only way to achieve this is to become a lifelong learner and have patience. Always act thinking about a long-term horizon. Or as Naval would say “Play long-term games with long-term people”.

With respect to learning, we should follow Leonardo Da Vinci’s advice

> “Just as iron rusts from disuse, and stagnant water putrifies, our when cold turns into ice, so our intellect wastes unless it is kept in use”.

Never stop learning.

Understand that learning is an act, it is not something we can attain just by reading books or watching TV. Learning is not passive. Always have a bias towards action.

> “I forget what I hear; I remember what I see; I know what I do” - Chinese proverb

Understand that the quest to think better and more clearly is an infinite game. There’s no mountain top, there’s no end.

Above all, **be patient**.

Warren Buffett and Charles Munger explained this clearly:

> “In allocating capital, activity does not correlate with achievement. Indeed, in the fields of investments and acquisition, frenetic behavior is often counterproductive. **If you feel like you have to invest every day, you’re going to make a lot of mistakes. It isn’t that kind of business. You have to wait for the fat pitch**.”

Charles Munger adds:

> “**A few major opportunities, clearly recognizable as such, will usually come to one who continuously searches and waits, with a curious mind, loving diagnosis involving multiple variables**. And then all that is required is a willingness to bet heavily when the odds are extremely favorable using resources available as a result of prudence and patience in the past

Finally, always choose **alive time over dead time**. Follow what Benjamin Franklin repeatedly said to himself:

> "To apply myself industriously to whatever business I take at hand, and not divert my mind from my business by any foolish project of growing suddenly rich; **for industry and patience are the surest means to plenty**.”

—

Seeking Wisdom is a true pearl of a book, one that I know I’ll keep close and re-read constantly.
