---
title:  13
created: 11/03/18
source:  2018-06-20-A-Philosophy-Of-Software-Design
categories: programming, strategic programming
---

<!-- Front -->

Your primary goal must be to produce a great design, which also happens to
work. This is strategic programming.

<!-- Back -->

