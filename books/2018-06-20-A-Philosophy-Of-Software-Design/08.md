---
title:  08
created: 11/03/18
source:  2018-06-20-A-Philosophy-Of-Software-Design
categories: collaboration, complexity
---

<!-- Front -->

Your job as a developer is not just to create code that you can work with
easily, but to create code that others can also work with easily.

<!-- Back -->

