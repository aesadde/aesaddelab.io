---
started: 07/30/2019
ended: 08/12/2019
created: 10/02/2019
title: The Lessons of History
author: Will and Ariel Durant
publisher: Simon & Schuster Paperbacks
rating: 7/10
kind: book
link: https://www.amazon.com/Lessons-History-Will-Durant/dp/143914995X
thumbnail: http://www.age-of-the-sage.org/history/quotations/durant-lessons_of_history.jpg
blurb: >
    Short run through all of history from different perspectives. It will leave you wanting to read more History books!
---

This book felt like a sprint through history. It left me wanting to read more
history books!
