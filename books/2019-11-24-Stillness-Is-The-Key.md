---
title: Stillness Is The Key
subtitle:
created: 11/24/19
started: 10/06/2019
ended: 10/11/2019
author: Ryan Holiday
publisher: Portfolio/Penguin
rating: 8/10
thumbnail: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimg.elephantjournal.com%2Fwp-content%2Fuploads%2F2019%2F08%2FStillness-Is-the-Key_cover.jpg&f=1&nofb=1
link:
blurb: >
  All philosophies have a word and a practice for stillness, to some degree. Holiday shows why and how stillness is important. The teachings and wisdom he shares are relevant to us and important if we wish to be a good and fulfilling life.
---

This book felt like a summary of lots of ideas that Holiday has put forward in his weekly and daily emails and his other books, [The Obstacle Is The Way]() and Ego is the Enemy.

I enjoyed it a lot. It is filled with practical and deep advice. It connects diverse philosophies and schools of thought to make you understand one thing: stillness, the art of being at ease, at peace even in the most difficult situations is a necessary condition to produce good work and live a good life.

Nothing that Holiday says is new or revolutionary in itself, but he has the capacity to condense hundreds of years of collective wisdom and provide advice that is both actionable and timeless.

I also enjoyed the format of the physical book. It is smaller in size than most books and everything about it, the cover, the typography, the table of contents, gave me a sense of tranquility. It reminded me of the attention to detail of Robert Greene's books, most notably [The 48 Laws of Power](). The typography of that book is one that I haven't found anywhere else.

>The struggle is great, the task divine—to gain mastery, freedom, happiness, and tranquility. - Epictetus

## Related

- [Shawn Green](https://en.wikipedia.org/wiki/Shawn_Green). I didn't know much about him but he's one of the greatest baseball players to ever play the game. I will be reading his book [The Way of Baseball: Finding Stillness at 95 mph]()

- [Essentialism](./2018-12-03-Essentialism.html). Pair the two books and you have a very good combination of wisdom and practical advice that you can start using any time.

- [Deep Work](./DeepWork.html). Deep work is the art of cultivating time for uninterrupted thought. Stillness makes for a very good pairing. Without one we can't achieve the other.
