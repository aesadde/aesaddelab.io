---
started: 03/15/19
ended: 03/26/19
title: The Effective Executive
subtitle: The Definitive Guide to Getting the Right Things Done
author: Peter F. Drucker
publisher: HarperCollins
rating: 9/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/61-3dvLEfBL.jpg
link: 
blurb: >
    I am not a manager but I am convinced that this is the single 
    most important book in the history of management.
---

Pretty much highlighted the whole book.

Many lessons to digest and apply. To be effective one needs to measure. To be
effective one needs to be deliberate. To be effective one needs to slow down,
take the time to work through complex stuff and make decisions based on
fundamental principles and not on external pressures.

Even though the book is about management at corporations, most of the lessons
should be applied  also at a more personal level. We should all strive to be
effective.

