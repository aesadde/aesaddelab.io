---
title:  Doing Good Better
subtitle: "How Effective Altruism Can Help You Help Others, Do Wrk that Matters
and Make Smarter Choises About Giving Back"
created: 09/02/19
started: 10/02/18
ended: 10/05/18
rating: 9/10
author: William MacAskill
publisher: Avery (Penguin Random House)
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51kJY4CdlaL._SY445_QL70_.jpg
link:
blurb: >
    When we invest money we try to be rational and think in terms of yield
    and ROIs. But when it comes to donating money we are mostly driven by
    emotion. This book tries to change that. The more rational and thoughtful
    we are about how and where we can make and impact, the more effective it
    can be.
---

I learned about the [Effective Altruism][altruism] an [80,000 hours][hours]
projects while I was a student at Oxford University. Since then I've always
checked their sites for useful information about career progress and how to
better help and contribute to causes that I care about.

This book is an introduction to the Effective Altruism mentality. It demystifies
many commonly held myths about charities and donations by using scientific
reasoning. I enjoyed it 100% and whenever I want to contribute to a charity that
I care about I consult this book first!

<!-- Links -->
[hours]: <https://80000hours.org/>
[altruism]: <https://www.effectivealtruism.org/>
