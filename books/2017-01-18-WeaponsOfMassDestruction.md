---
title: Weapons of Mass Destruction
subtitle: How Big Data Increases Inequality And Threatens Democracy
author: Cathy O'Neil
started: 09/11/16
finished: 13/11/16
publisher:
rating: 7/10
thumbnail: https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1456091964l/28186015.jpg
link:
blurb: >
    Algorithms have biases, the biases of their implementors. These are magnified with
    "big data".
---


Problems of big data (wrong models, spurious data)

Models that rule the world and change the objectives and priorities of
institutions (New Corp University rankings for example).

Models are not always the best answer to everything. New approaches are good
if tested and have a feedback loop. Many policies like walk patroling are
still better, in many instances than data-driven and automated approaches.

We still have a lot to learn.
