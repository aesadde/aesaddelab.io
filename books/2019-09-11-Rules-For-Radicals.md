---
started: 07/26/2019
ended: 07/30/2019
created: 09/11/2019
title: Rules For Radical
subtitle: A Pragmatic Primer for Realistic Radicals
author: Saul D. Alinsky
publisher: Vintage Books - Random House
rating: 10/10
link: https://www.amazon.com/Rules-Radicals-Practical-Primer-Realistic/dp/0679721134
thumbnail: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FI%2F41urB0oB9kL._SY291_BO1%2C204%2C203%2C200_QL40_.jpg&f=1&nofb=1
blurb: >
    This book is one of the best books I've read this year. The book is a lesson on
    political organization but can be seen as a lesson for life. Power, for example
    is not about what you have but about what your opponent thinks you have.
---

This book is one of the best books I've read this year. The book is a lesson on
political organization but can be seen as a lesson for life. Power, for example
is not about what you have but about what your opponent thinks you have. The
world needs to be taken as is, with *radical realism*. Only in this way you will
be able to change the things you want to change. Focus and direction are other
important components. Without focus and clarity an organizer can't do much in
the way of organizing. The same can be said for life in general. Finally,
without optimism, *radical optimism*, life can't be lived.
