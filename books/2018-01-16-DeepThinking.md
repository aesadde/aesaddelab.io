---
started: 01/04/2018
ended: 01/16/2018
title: Deep Thinking
author: Garry Kasparov
publisher: Perseus Books, LLC
rating: 8/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51kanGO-2VL._SX327_BO1,204,203,200_.jpg
blurb: >
    Kasparov is perhaps the greates chess grandmaster of our time.
    In this book he recounts his life as a grandmaster and tries to
    describe what he thinks the machine-driven future holds for us.
---
