---
title:  12
created: 09/20/17
source:  2017-09-20-AGuideToTheGoodLife
categories: principles, philosophy
---

<!-- Front -->
We should periodically pause to reflect on the fact that we will not live
forever and therefore that this day could be our last.
-- Highlighted May 9, 2017

<!-- Back -->

