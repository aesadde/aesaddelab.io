---
title:  23
created: 09/20/17
source:  2017-09-20-AGuideToTheGoodLife
categories: life, philosophy
---

<!-- Front -->
Possessing wealth, he observes, won’t enable us to live without sorrow and
won’t console us in our old age.  -- Highlighted Jun 6, 2017

<!-- Back -->

