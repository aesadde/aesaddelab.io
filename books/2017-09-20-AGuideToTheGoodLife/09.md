---
title:  09
created: 09/20/17
source:  2017-09-20-AGuideToTheGoodLife
categories: philosophy, principles
---

<!-- Front -->
“It is difficulties that show what men are. Consequently, when a difficulty
befalls, remember that God, like a physical trainer, has matched you with
a rugged young man.” -- Highlighted May 7, 2017

<!-- Back -->

