---
title:  02
created: 09/20/17
source:  2017-09-20-AGuideToTheGoodLife
categories: goals, philosophy
---

<!-- Front -->
If you lack an effective strategy for attaining your goal, it is unlikely that
you will attain it. Thus, the second component of a philosophy of life is
a strategy for attaining your grand goal in living
-- Highlighted May 5, 2017
<!-- Back -->

