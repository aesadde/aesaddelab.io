---
title:  11
created: 03/30/17
source:  2017-03-30-TheMeaningOfItAll
categories: truth, science
---

<!-- Front -->
Prejudices have a tendency to make it harder to prove something, but when
something exists, it can nevertheless often lift itself out.
<!-- Back -->
