---
title:  18
created: 03/17/19
source:  2019-03-17-The-Mythical-Man-Month
categories: management, training
---

<!-- Front -->
Managers need to be sent to technical refresher courses, senior technical
people to management training. Project objectives, progress and management
problems must be shared with the whole body of senior people.
<!-- Back -->

