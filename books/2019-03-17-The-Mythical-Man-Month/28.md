---
title:  28
created: 03/17/19
source:  2019-03-17-The-Mythical-Man-Month
categories: management
---

<!-- Front -->
How to grow great designers? [...]:
* Systematically identify top designers as early as possible. The best are
  often not the most experienced.
* Assign a career mentor to be responsible for the development of the prospect,
  and keep a careful career file.
* Devise and maintain a career development plan for each prospect, including
  carefully selected apprenticeships with top designers, episodes of advanced
  formal education, and short courses, all interspersed with solo design and
  technical leadership assignments.
* Provide opportunities for growing designers to interact with and stimulate
  each other.
<!-- Back -->

This should be done across the board, not only for designers.
