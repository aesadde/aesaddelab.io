---
title:  07
created: 03/17/19
source:  2019-03-17-The-Mythical-Man-Month
categories: systems, design
---

<!-- Front -->
The architect of a system, like the architect of a building, is the user's
agent. It is his job to bring professional and technical knowledge to bear in
the unalloyed interest of the user, as opposed to the interests of the
salesman, the fabricator, etc.
<!-- Back -->

