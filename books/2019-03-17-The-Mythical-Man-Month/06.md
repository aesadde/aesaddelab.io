---
title:  06
created: 03/17/19
source:  2019-03-17-The-Mythical-Man-Month
categories: design, systems, planning
---

<!-- Front -->
Conceptual integrity is _the_ most important consideration in system design. It
is better to have a system omit certain anomalous features and improvements,
but to reflect one set of design ideas, than to have one that contains many
good but independent and uncoordinated ideas.
<!-- Back -->

