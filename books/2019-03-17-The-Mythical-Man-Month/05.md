---
title:  05
created: 03/17/19
source:  2019-03-17-The-Mythical-Man-Month
categories: time, management, planning
---

<!-- Front -->
Failure to allow enough time for system test, in particular, is peculiarly
disastrous. Since the delay comes at the end of the schedule, no one is aware
of schedule trouble until almost delivery date. Bad news, late and without
warning, is unsettling to customers and managers
<!-- Back -->

