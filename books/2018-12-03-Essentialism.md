---
started: 12/03/18
ended: 12/17/18
title: Essentialism
subititle: The Disciplined Pursuit of Less
created: 11/03/19
tags:
author: Greg McKeown
publisher: Virgin Books
kind: book
rating: 10/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/41AnqHS6KZL._SX331_BO1,204,203,200_.jpg
link:
blurb: >
    I read it while struggling with a lot of projects and ideas.
    The solution: say no more often than you say yes. Think deeply about what you want to achieve.
    First remove the unnecessary things. Then execute.
---


If you're struggling with your projects, have too many decisions to make or find
yourself always saying yes to everything then this book is a must read!
