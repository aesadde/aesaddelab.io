---
title: The Score Takes Care of Itself
subtitle: My Philosophy of Leadership
created: 05/10/20
started: 04/08/2020
ended: 04/12/2020
author: Bill Walsh
publisher: Portfolio
rating: 10/10
thumbnail: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fm.media-amazon.com%2Fimages%2FI%2F51JoBs6v3-L._SL210_.jpg&f=1&nofb=1
link: https://amzn.to/2Wl1w4j
blurb: >
  This book condenses the philosophy of leadership that
  Bill Walsh—one of the most successful head coaches in NFLs history—developed
  and applied throughout his career. It is a philosophy rooted in excellence
  and sacrifice. It shows that there are really no short-cuts to being and
  doing great. Put in the work and "the score will take care of itself".
---

### The Standard of Performance

At the core of Walsh' philosophy was his "Standard of Performance", a set of rules that he developed and honed during the first 25 years of his career. This standard became his North Start, within it everything was allowed. Outside it, nothing.

He summarized the standard as follows (emphasis mine):

> Exhibit a ferocious and intelligently applied **work ethic** directed at continual improvement; demonstrate **respect for each person** in the organization and the work he or she does; be **deeply committed to learning and teaching**, which means increasing my own expertise; be fair; **demonstrate character**; honor the direct connection between details and improvement, and relentlessly seek the latter; show self-control, especially where it counts most—under pressure; **demonstrate and prize loyalty**; **use positive language** and have a positive attitude; take pride in my effort as an entity *separate* from the result of that effort; be willing to **go the extra distance** for the organization; deal appropriately with victory and defeat, adulation and humiliation (don't get crazy with victory nor dysfunctional with loss); **promote internal communication** that is both open and substantive (especially under stress); seek poise in my.self and those I lead; put the team's welfare and priorities ahead of my own; **maintain an ongoing level of concentration and focus that is abnormally high**; and **make sacrifice and commitment their organization's trademark**.

The Standard of Performance became famous after Bill applied it and transformed the San Francisco 49ers from a losing organization to one of the best professional sports organizations of his time. Across the book Bill Walsh comes across as a very disciplined but thoughtful and inspiring leader. He demanded and inspired excellence.

### Leadership

Bill's philosophy was bigger than football. The principles he used and applied around his Standard of Performance can be applied in other professions to create an environment of excellence and great leadership.

In fact, this is what he taught at Stanford Business School [TKK link] once he retired from football.

He believed that great leaders were not egotistical and always put the interest of the organization or team above their own. Bill's ideal of a great leader is similar to the "Level 5" leaders that Jim Collins describes in [Good to Great](https://aesadde.xyz/books/2019-07-21-Good-To-Great/). In Bill's words, a great leader is one that "creates a self-sustaining organization able to operate at the highest levels even when he or she leaves".

### Planning

> You have to know where you're going to come out before you go in.

Bill's philosophy around planning can be summarized as "leave nothing to chance". He was a ruthless planner. Throughout his tenure the 49ers had plans for everything. They would study games and obsess over plays and details. They planned each game down to a few seconds constructing huge playbooks to guide them through any surprise that would arise.

The goal of obsessing over planning was for Bill the only way to be ready and prepared. He didn't like to leave anything to chance. His was a very *stoic* philosophy: focus on what you can control and the score will take care of itself.

### Learning + Teaching

> The more you know, the higher you go". To advance in any profession, I believe it is imperative to understand all aspects of that profession, not just one particular area: Only *expertise* make you an expert.

As described in his Standard of Performance, Bill knew that great leaders are great learners and also great teachers. He exemplified this. He wanted to be seen, above all, as a good teacher. He always prepared and made sure that he could communicate clearly.

### Communication

Bill's philosophy of leadership and teaching was rooted in good communication. He expected everybody to communicate constantly and clearly. Key to the team's success was for everybody to understand not only what they needed to do but also *why* they needed to do it:

> Quality collaboration is only possible in the presence of quality [[communication]]; that is, the free-flowing and robust exchange of information, ideas, and opinions. And "having big ears"—the skill of being a great listener—is the first law of good communication. (The second law is "When you're not listening, ask good questions")

### Fairness

Key to Bill's philosophy of excellence was to treat everyone fairly. This was unorthodox in his time. Back then, coaches were used to treat players like animals and squeeze every little bit of their energy during training and during games.

In contract, Bill developed a philosophy rooted in camaraderie and fairness. Everyone in the organization would be treated fairly and with respect and would treat others the same way. Nobody was above anybody else.

This also served him to instill a sense of belonging in every member of the organization. From receptionists to players and coaches, everybody was proud of being a 49er. Their work was important and played a role in the team's overall success.

### Example

We need to live what we preach. Bill Walsh trusted actions more than words.

By leading the 49ers in their path to excellence he showed to the world of professional sports and organizations what is possible by applying a Standard of Performance. To this day, there is no American Football organization with track record as incredible as the San Francisco 49ers that Bill Walsh helped create.

### Lists

According to his players and coaches, Bill had lists and checklists for everything. This was part of his obsession for planning and excellence.

Here are a few that stood out from the book:

#### FIVE DOs FOR GETTING BACK INTO THE GAME

1. Do expect defeat.

2. Do force yourself to stop looking backward and dwelling on the professional "train wreck" you have just been in.

3. Do allow yourself appropriate recovery—grieving—time.

4. Do tell yourself, "I am going to stand and fight again," with the knowledge that often when things are at their worst you're closer than you can imagine to success.

5. Do begin planning for your next serious encounter.

—

#### FIVE DON'TS

1. Don't ask "Why me?"

2. Don't expect sympathy.

3. Don't bellyache.

4. Don't keep accepting condolences.

5. Don't blame others.

—

#### Lessons from the Bill Walsh Offense

1. Success doesn't care which road your take to get to its doorstep.

2. Be bold. Remove fear of the unknown—that is, change—from your mind.

3. Desperation should not drive innovation."What assets do we have right now that we're not taking advantage of?"

4. Be obsessive in looking for the upside in the downside.

—

#### Be a Leader —Twelve Habits plus one

1. Be yourself.

2. Be committed to excellence.

3. Be positive.

4. Be prepared. (Good luck is a product of good planning) No leader can control the outcome of the contest or competition, but you *can* control how you prepare for it.

5. Be detail-oriented. Do not make the mistake of burying yourself alive in those details.

6. Be organized.

7. Be accountable.

8. Be near-sighted and far-sighted.

9. Be fair.

10. Be firm. I would not budge an inch on my core values, standards, and principles.

11. Be flexible

12. Believe in yourself.

13. Be a leader.

#### Checklist to avoid burnout

1. Do not isolate yourself.

2. Delegate abundantly.

3. Avoid the destructive temptation to *define* yourself as a person by the won-lost record, the "score", however you define it.

4. Shake it off. "I've got a 24-hour rule. You only let it bother you for 24 hours and it's over".

### Football

In the end, the book is also about Football and the NFL of the 1970s and 1980s. I learned a few things about the sport as a whole. Below are some things that I learned and surprised me:

- ["The Drive"](https://www.youtube.com/watch?v=SrcPy3HiiUk). The 49ers went the length of the field with under three minutes remaining on the clock to score the winning touchdown of Super Bowl XXIII. Even if you don't understand a lot about football you can appreciate the perfection and focus of the 49ers during those last few minutes. Also, I don't think it's any coincidence that both Jerry Rice and Joe Montana, two of the best players in NFL history, started their careers under Bill Walsh.

- Although not a 49er, Lawerence Taylor is considered the best defensive player of [NFL's history](https://www.youtube.com/watch?v=mHC15HYwJ5Q). For a long time he was the only defensive player to be named MVP of the year. Many players described him as a train or a bull charging in full force when they were sacked by him. Perhaps his most famous tackle is ["The Hit That No One Who Saw It Can Ever Forget"](https://www.youtube.com/watch?v=Z7acc6qwcmQ), it ended Joe Thiesmann's career (warning: it's a crude video).


