---
started: 03/12/2018
ended: 03/29/2018
title:  Zen Mind, Beginnger's Mind
subititle: Informal Talks on Zen Meditation and Practice
created: 11/02/19
author: Shunryu Suzuki
publisher: Shambala
kind: book
rating: 9/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/41%2B4VGlAQhL.jpg
link:
blurb: >
    Everything about this book, from the pages, to the smell, to the content
    gave me a big sense of peace and detachement. 
    It makes for a very nice and meditative read.
---

This was the first book I read about Buddhism and Zen practice. It is a
beautifully written book. It will give you peace and it will help you understand
what meditation is all about.

Below my highlights.
