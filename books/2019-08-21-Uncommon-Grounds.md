---
started:
ended: 04/22/19
title: Uncommon Grounds
subtitle: The History of Coffee and How it Transformed our World
author: Mark Pendergrast
publisher:
rating: 6/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51gpNTOk%2BUL._SX329_BO1,204,203,200_.jpg
link: 
blurb: >
    This as a historical overview of coffee, from its origins all the way to
    Starbucks and third wave coffee.
---

Even though I am coffee fan (and addict some might say), this was my first book
about coffee. I chose to read this one after doing some research. I want to read
about the history of coffee and how it came to as ubiquitous as it is today.

In this regard, the book didn't disappoint. Pendergrast traces all the history
of coffee going into a lot of detail in the history of coffee in North America.
But, the style in which the description is done is boring. The language is flat
and it feels almost like a simple recount of events rather than a story.
