---
started: 12/18
ended: 02/09/19
title: Titan
subtitle: The Life of John D. Rockefeller Sr.
author: Ron Chernow
publisher: Vintage Books
rating: 8/10
thumbnail: https://pictures.abebooks.com/isbn/9780751526677-uk.jpg
link: 
blurb: >
    Rockerfeller lead an impressive life. I had this idea of him being a
    despicable man that would do anything for money. The book paints a more
    complete and compelling picture. He did indeed amass the biggest fortune
    of his time but was also a very big philantropist.
---

Although longer than needed and slow at times, I enjoyed the book very much.
Ron Chernow manages to give us the full picture: Rockefeller the most
successful businessman of his time and father of the oil industry as we know
it; Rockefeller the man, the stoic and loving father and Rockefeller the
philanthropist.





