---
started:  05/19/2019
ended: 05/29/2019
title:  Leonardo Da Vinci
subtitle:
author: Walter Isaacson
publisher: Simon & Schuster
rating: 9/10
thumbnail: https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Feverythingnonfiction.com%2Fwp-content%2Fuploads%2F2017%2F12%2FLeonardo.jpg&f=1&nofb=1
link:
blurb: >
    Neither Leonardo nor Isaacson need introductions.
    This book is a wonderful biography. Go read it. Stay curious.
---

This is the first book written by Walter Isaacson that I read. It lived up to
expectations. The book is beautifully written. I liked in particular that
Isaacson is not afraid to give his opinion on certain matters especially when
historians don't seem to agree. This is something that makes the whole reading
more fluid and brings us closer to what goes on in the writer's mind.

In terms of the content, it is hard to beat Leonardo. Everybody already knows
about him, about his inventions, about all the different specialties and fields
that he went in. Everybody knows about his paintings and experiments. What the
book really manages to do well is to make one understand that his genius was not
pure luck. It was very hard work and a curiosity like we have never seen again.

Leonardo believed everything was interconnected. Science and art where two sides
of the same coin and his work, as reflected in the book, show that.

Below are passages that I want to keep re-reading. But you should grab the book
and give it a go. You'll learn more about anatomy, art and observation that you
did in the past.



