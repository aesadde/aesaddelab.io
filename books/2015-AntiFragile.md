---
title: Antifragile
author: Nassim Taleb
publisher:
started:
ended:
kind: book
rating: 10/10
thumbnail: https://cdn11.bigcommerce.com/s-ezptpblrxu/images/stencil/500x659/products/854/1422/Antifragile3__90889.1568057209.jpg?c=2
link:
blurb: > 
    Antifragility is the property of things and systems that gain from disorder.
    This book is wonderful. It changed the way I see the world.
---

This book is amazing.
I have not finished it but it is so nice and opened my eyes to so many different
aspects of life that I want to really understand it!

Antifragile by definition is the property of things that gain from disorder, it
is the opposite of fragile.
Once we know what this concept means we start seeing the world from a different
perspective. We understand why some things break and others don't. We start
understanding how some situations become possible. We need to stop trying to
predict everything instead we need to let go of everything and learn to deal
with mystery and randomness. This is the only way to thrive and live a full
life.
