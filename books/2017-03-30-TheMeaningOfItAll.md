---
started:
ended:
title: The Meaning Of It All
subtitle: Thoughts of a Citizen-Scientist
author: Richard P. Feynman
publisher:
rating: 10/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51OZWVtR7CL._SY291_BO1,204,203,200_QL40_.jpg
link:
blurb: >
    This little book is a transcription of a series of lectures Feynman gave at the
    University of Washington in Seattle. It is a nice and quick read in it's
    entirety.
---

This little book is a transcription of a series of lectures Feynman gave at the
University of Washington in Seattle. It is a nice and quick read in it's
entirety.

As with everything that has do to with Feynman, the book is funny, he had such
a great sense of humour and yet he could get across some important ideas and
principles for everybody to ponder and think through.

Throughout the lectures Feynman tries to convey the message that the world
would be better of by being more scientific. He argues and explains that while
science doesn't have all the answers, it doesn't hold the keys to the ultimate
truth, it can be used to make the world a better place and it can help us live
better and more fruitful lives.

What is remarkable about his thesis and statements is that most of what he said
in 1967, forty years ago, is still very much relevant today. You would think
that by now - we are in 2017 after all - we would be living in a scientific
age. But, despite all the scientific and technological advances that we are
seeing, and the ever increasing speed at which these advances are happening,
the world still has a lot to learn.
