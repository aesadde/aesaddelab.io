---
title: The Power Of Habit
author: Charles Duhigg
started:
ended:
publisher:
books: My Life in Advertising, Evolutionary Theory of economic change, The Purpose Driven Life
rating: 9/10
thumbnail: https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1545854312l/12609433._SY475_.jpg
link:
blurb: >
    The science behind habit formation. You'll understand how
    the mechanisms work. It is helpful to get replace bad habits with good ones.
---

The Power Of Habit uncovers the mysteries behind our habits. Habits are what
make most of our daily lives yet until recently not much was known about them.
As this book explains, there are clear patterns that all habits share: a cue
and rewards loop that makes some people exercise and makes other people drink
or gamble every day.

Duhigg explains how this mechanism works giving real life examples of gamblers
and alcoholics, and even gives some explanations as to why certain civic
movements are successful while others fail, all this citing and explaining
recent and relevant research in areas such as neuroscience and psychology.

All in all, I really enjoyed this book and completely changed the way I think
about habits. I am now more mindful about them and always try to change them
and make better choices.



