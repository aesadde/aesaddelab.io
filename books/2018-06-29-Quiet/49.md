---
title:  49
created: 08/18/18
source:  Quiet
categories: culture
---

<!-- Front -->
Israelis, says the researchers, "are not likely to view (disagreement) as
a sign of disrespect, but as a signal that the opposing party is concerned and
is passionately engaged in the task".
<!-- Back -->

