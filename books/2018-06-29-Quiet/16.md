---
title:  16
created: 08/18/18
source:  Quiet
categories: quotes
---

<!-- Front -->
"There comes a time that people get tired of being trampled over by the iron
feet of oppression", he tells them "There comes a time when people get tired of
being pushed out of the glittering sunlight of life's July and left standing
amidst the piercing chill of an Alpine November".

-- Martin Luther King Jr.
<!-- Back -->

