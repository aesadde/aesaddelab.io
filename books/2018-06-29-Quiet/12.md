---
title:  12
created: 08/18/18
source:  Quiet
categories: introverts, physiology
---

<!-- Front -->
High-reactive introverts sweat more, low-reactive extroverts sweat less. Their
skin is literally "thicker", more impervious to stimuli, cooler to the touch.
<!-- Back -->

