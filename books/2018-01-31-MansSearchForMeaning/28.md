---
title:  28
created: 07/10/18
source:  MansSearchForMeaning
categories:  meaning, suffering
---

<!-- Front -->
Suffering ceases to be suffering at the moment it finds a meaning, such as the
meaning of a sacrifice.
<!-- Back -->

