---
title:  36
created: 07/10/18
source:  MansSearchForMeaning
categories: attitude, optimism
---

<!-- Front -->
A positive attitude enables a person to endure suffering and disappointment as
well as enhance enjoyment and satisfaction. A negative attitude intensifies
pain and deepens disappointments, it undermines and diminishes pleasure,
happiness and satisfaction' it may even lead to depression or physical illness.

<!-- Back -->

