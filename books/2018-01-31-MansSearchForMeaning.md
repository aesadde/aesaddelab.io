---
started: 01/22/2018
ended: 01/31/2018
title: Man's Search For Meaning
author: Viktor E. Frankl
publisher: Beacon Press
rating: 10/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/41jNJ1FrUlL._SX307_BO1,204,203,200_.jpg
link:
blurb: >
    This book will change your life. 
---
