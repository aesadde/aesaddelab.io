---
started:
ended:
title: The Obstacle is the Way
author: Ryan Holiday
publisher:
kind: book
rating: 10/10
thumbnail: https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fcareermetis.com%2Fwp-content%2Fuploads%2F2015%2F12%2FCover.jpg&f=1&nofb=1
link:
blurb: >
    This is the best introduction to Stoicism. Holiday makes a fantastic job
    in distilling timless stoic ideas from Marcus Aurelius, Seneca, Epictetus, and other 
    ancient stoic philosophers. 
---


This book taught me about *amor fati*, love your destiny, love everything that happens.

In every situation there are things under our control —our emotions, actions and reactions— but there are many others that we don't have any control over. So, we should focus on the things that we have control over and accept, almost *love* the things that happen and which we can't control. If we do this, we will more tranquil and we will be able to focus on the things that matter.
