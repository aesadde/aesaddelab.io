---
title:  08
created: 07/31/19
source:  2019-07-31-Leonardo-Da-Vinci
categories: analogy, systems
---

<!-- Front -->

He applied classic analogy between the microcosm of the human body and the
macrocosm of the earth: cities are organism that breathe and have fluids that
circulate and waste that needs to move. He had recently begun studying blood and
fluid circulation in the body. Thinking by analogy, he considered what would be
the best circulation system for urban needs, ranging from commerce to waste
removal.

<!-- Back -->

