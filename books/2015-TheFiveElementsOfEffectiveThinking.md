---
title: The Five Elements of Effective Thinking
author: >
  Edward B. Bruger,
  Michael Starbird
started: 07/2015
ended:
publisher:
rating: 5/10
thumbnail: https://g.christianbook.com/dg/product/cbd/f400/156668.jpg
link: 
blurb: >
  The authors propose a framework of five elements for better thinking and decision-making process:
  understand deeply, make mistakes, raise questions, follow ideas, change.
---

Very short book but assertive.

It opens your mind to what you need to do to think better.
Using the framework the highlight it becomes simpler to think about stuff
and to distill and analyze complex topics.

I look forward to go over it in the future to properly internalise their ideas
and apply them.
