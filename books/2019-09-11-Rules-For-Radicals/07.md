---
title:  07
created: 09/11/19
source:  2019-09-11-Rules-For-Radicals
tags: radical realism, objectives
---

In the world as it is, the stream of events surges endlessly onward with death
as the only terminus. One never reaches the horizon; it is always just beyond,
ever beckoning onwards; it is the pursuit of life itself. This is the world as
it is. This is where you start.

[[Viktor Frankl, Man's Search for Meaning]]
