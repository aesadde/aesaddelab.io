---
title: Slipstream Time Hacking
author: Ben Hardy
started:
finished:
publisher:
rating: 6/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51d5tqt2dvL.jpg
link:
blurb: >
    A self-help book. Compelling ideas of how to accelerate time, and attempt
    to 10x your results. 
---

This book was a fast and easy read. While if definitely another self-help book,
there are some good pieces of advice and analogies that are worth remembering and
keeping in mind from time to time.

Above all, what this small book made me realise how, by changing a few habits, working
smarter and not necessarily harder, we can upgrade our life, enjoy more, be less busy
and start accomplishing our goals.
