---
title: The Five Most Important Questions
kind: book
author: Peter Drucker
started: 08/06/15
ended: 14/06/15
rating: 5/10
thumbnail: https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1439432438l/24541749._SY475_.jpg
link:
blurb: >
    This book makes us ask questions that, although simple and obvious in
    appearance, open avenues of self-exploration and criticism that we would not
    have found in any other way.
---

This book makes us ask questions that, although simple and obvious in
appearance, open avenues of self-exploration and criticism that we would not
have found in any other way.

As an individual, this book helped me gain a clearer view of the questions to
ask when dealing with new projects and ideas. It gives you a framework to work.
The questions are very specific and help you gain more insight about your ideas.
In particular - What is your mission? - is the most profound question you can ask
when starting an organization, a new project or simply assessing your life's
goals.

This is the first book about management that I read (it was my sister that
pointed this out to me, I just read it as part of an entrepreneurship course I
attended while at York). I think it does a good job at summarising everything
that I need to get me started on any new endeavour.

If anything, this books teaches you to ask questions, the right questions and
dig deeper, explore what is that you are trying to achieve. By doing this
self-assessment you can go along way and discover things you did not know where
there.
