---
title:  31
created: 06/28/19
source:  2019-06-29-Rework
categories: environment, team, hiring
---

<!-- Front -->
You need an environment where everyone feels safe enough to be honest when
things get tough. You need to know how far you can push someone. You need to
know what people really mean when they say something.

So hire slowly. It's the only way to avoid winding up at a cocktail party of
strangers.
<!-- Back -->
