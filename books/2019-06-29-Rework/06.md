---
title:  06
created: 06/28/19
source:  2019-06-29-Rework
categories: time, perfectionism
---

<!-- Front -->
The *perfect* time never arrives. You're always too young  or old or busy or
broke or something else. If you constantly fret about timing things perfectly,
they'll never happen.
<!-- Back -->
