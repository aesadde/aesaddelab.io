---
title:  11
created: 06/28/19
source:  2019-06-29-Rework
categories: priorities, essentialism
---

<!-- Front -->
There's the stuff you *could* do, the stuff you *want* to do, and the stuff you
*have* to do. The stuff you *have* to do is where you should being. Start at the
epicenter.
<!-- Back -->
<!-- Must have vs nice to have -- Bill Purcell -->
