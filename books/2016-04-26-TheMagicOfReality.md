---
started: 2015
ended: 04/26/2016
title: The Magic Of Reality
author: Richard Dawkins
publisher:
rating: 8/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51dcC45R67L._SX326_BO1,204,203,200_.jpg
link:
blurb: >
    A nice little book that tries to explain in very simple terms most things that
    science can now explain.
---

A nice little book that tries to explain in very simple terms most things that
science can now explain.
What are miracles, earthquakes, are we alone...? Are all questions answered.

While I'm not the biggest fan of Dawkins since I think he is  a bit of an arrogant guy because by being atheist and a scientist he believes he is better than other people, the book is well written and I loved the chapter introductions with the different myths from different civilizations.

This book is probably to be read by children so they can start asking questions and see how beautiful Nature is and how reality works.

To be paired with "A Daemon Hunted World: Science as a candle in the night" by Carl Sagan.
