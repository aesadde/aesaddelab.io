---
started:
ended:
created: 10/12/2019
title: The Bed Of Procrustes
author: Nassim Taleb
publisher: Random House
rating: 10/10
kind: book
thumbnail: https://images-na.ssl-images-amazon.com/images/I/71kTa2IZfNL.jpg
link:
blurb: >
    Aphorisms. Lots of them. It is good to read only one page a day.
---

I love everything that Taleb writes. I find his arguments compelling, filled
with ancient wisdom and very actionable.

This book, *The Bed of Procrustes* is exactly like that. The style is totally
different from his other books in *Incerto*. The book is a collection of
aphorisms, or maxims, that distill a lot of the ideas and knowledge shared in
the other three volumes.

Below is the list of my favorite ones.
