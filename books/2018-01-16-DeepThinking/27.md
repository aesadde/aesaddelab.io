---
title:  27
created: 03/08/18
source:  DeepThinking
categories: chess, machine
---

<!-- Front -->

In an ironic but inevitable turn, machines use opening books based on human
knowledge, but increasingly Grandmasters employ engines to assist them with
their opening preparation.


<!-- Back -->

Man-machine symbiosis.

