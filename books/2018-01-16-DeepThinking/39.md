---
title:  39
created: 03/08/18
source:  DeepThinking
categories:
---

<!-- Front -->

In 1990, Ken Thompson of Belle was openly recommending the game Go as a more
promising target for real advances in machine cognition.

<!-- Back -->

Ken Thompson is incredible to me. We owe  so much of modern computers to him
and here we has, ahead of his time playing with chess playing machines and
already thinking of the next problem to tackle!
