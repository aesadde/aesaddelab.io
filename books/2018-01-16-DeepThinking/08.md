---
title:  08
created: 03/07/18
source:  DeepThinking
categories:
---

<!-- Front -->
We confuse performance -- the ability of a machine to replicate or surpass the
results of a human -- with method, how those results are achieved.

<!-- Back -->

Sometimes the best performance comes with the simpler or most brute force
approach

