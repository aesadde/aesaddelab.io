---
started: 02/11/18
ended: 02/14/19
title: Digital Minimalism
subtitle: Choosing a Focused Life in a Noisy World
author: Cal Newport
publisher: Portfolio/Penguin
thumbnail: https://images-na.ssl-images-amazon.com/images/I/81Zvj0ewCfL.jpg
link: 
blurb: > 
    This book follows the same trend of Newport's two previous books (So Good They
    Can't Ignore you and Deep Work). Newport tries to argue for a more focused and
    intentional life and to do this, he argues, one needs to use tools (digital
    tools in this case) mindfully.
---

This book follows the same trend of Newport's two previous books (So Good They
Can't Ignore you and Deep Work). Newport tries to argue for a more focused and
intentional life and to do this, he argues, one needs to use tools (digital
tools in this case) mindfully.

The book opens with explanations about what Newport believes to be "Digital
Minimalism" and how we can reclaim our time back by backing off from social
media and other attention-grabbing tools by means of a "digital declutter"
practice. The second part of the book is filled with practical advice on how to
adopt and maintain the life of a digital minimalist.

Overall, I think this book is a nice and easy read. It will definitely make you
rethink how you should use your smartphone and will probe you to think
carefully about how and why you use social media.

