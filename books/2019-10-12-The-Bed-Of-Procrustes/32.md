---
title:  32
created: 10/12/19
source:  2019-10-12-The-Bed-Of-Procrustes
tags: right, correctness
---


Your duty is to scream those truths that one should shout but that are merely
whispered.


<!-- Do the right thing. Your duty. -->
