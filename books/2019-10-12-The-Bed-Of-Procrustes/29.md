---
title:  29
created: 10/12/19
source:  2019-10-12-The-Bed-Of-Procrustes
tags:
---


The calamity of the information age is that the toxicity of data increases much
faster than its benefits.

<!-- Too much noise. More and more filters arise every day to combat this. It might -->
<!-- not be enough though. -->
