---
title:  16
created: 01/26/20
source:  2020-01-26-The-Control-of-Nature
tags: religion
---

In Iceland, three dates that everybody knows are 874, when settlement first occurred, 930, when the parliament was founded, and 1000, when a great and prolonged debated resulted in the nation's accepting Christianity. During the deliberations, word reached the parliament that red-hot lava was advancing toward the farm of a prominent and influential Christian. To the heathens, this was a sign. One of them said, "The gods are angry."
The opposition replied, "Then what angered the gods when the lava we are standing on was flowing?"


