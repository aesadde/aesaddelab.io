---
title:  29
created: 01/27/20
source:  2020-01-26-The-Control-of-Nature
tags:
---

In the long dry season, and particularly in the fall, air flows southwest toward Los Angeles from the Colorado Plateau and the Basin and Range. Extremely low in moisture, it comes out of the canyon lands and crosses the Mojave Desert. As it drops in altitude, it compresses, becoming even dryer and hotter. It advances in gusts. This is the wind that is sometimes called the foehn. The fire wind. The devil wind. In Los Angeles, it is known as Santa Ana.


