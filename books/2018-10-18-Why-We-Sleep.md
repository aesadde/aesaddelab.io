---
started: 12/03/18
ended: 12/17/18
title: Why We Sleep
subititle:
created: 11/03/19
tags:
author: Mathew Walker
publisher: Virgin Books
kind: book
rating: 10/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/81w6RZ6xm1L.jpg
link:
blurb: >
    A comprehensive overview of the evolutionary need for sleep.
    The book can be a bit alarming at times. It will make you want to sleep more.
---

Note: Many claims in the book have been debunked. While I still think it is
a very good piece of writing with good tips for better health overall,
I wouldn't take everything that the book says at face value (one never should do
this). Read [this
article](https://guzey.com/books/why-we-sleep/#appendix-what-do-you-do-when-a-part-of-the-graph-contradicts-your-argument-you-cut-it-out-of-course)
for more information.
