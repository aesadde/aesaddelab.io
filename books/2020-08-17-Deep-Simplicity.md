---
started:
ended: 06/21/2020
title: Deep Simplicity
Subtitle: Bringing order to Chaos and Complexity
author: John Gribbin
publisher: Random House LLC
rating: 8/10
thumbnail: /files/images/deep_simplicity.jpg
blurb: >
  Life happens at the edge of chaos. Simple mathematical and physical rules are enough to explain how, by chance, we got here.
---

### Overview

By tracing the history and the development of mathematical and physical laws since the times of Galileo Galilei, John Gribbins takes us on a journey through the history of Chaos Theory and, in doing so, he tells us how life on Earth became possible.

He starts with Newton's laws of motion and gravitation. While they provided us a way to explain why planets move the way they do,

> there is nothing in Newton's laws to reveal the direction of time, and the laws work just as well if time is running the other way.

It wasn't until the work on heat transfer by Fourier that the ideas of entropy started to emerge. Entropy gave scientists the first indication that time was not reversible, the Universe's entropy is ever-increasing giving time a very precise direction.

> On the macroscopic scale, the Universe operates in an irreversible way. You can never put things back the way they used to be.

It was Poincaré who also started to notice how sensitive certain natural systems are to initial conditions: a tiny change at the beginning can produce massively different results in a non-linear fashion. His work later serves Lorenz to realize how unpredictable weather patterns are. He summarized his discoveries in his seminal work "Does the Flap of a Butterfly's Wings in Brazil Set Off a Tornado in Texas?”

### Chaos Theory

Here's where things start getting interesting. The rise of Chaos Theory, according to Gribbins, can explain how life on Earth became and is still possible.

On one hand, chaos explains how life on Earth first started, complexity needs just a few ingredients:

> Simple laws, nonlinearity; and sensitivity to initial conditions, and feedback are what makes the world tick.

On the other, the absence of it—keeping entropy almost constant— explains how life on this planet is still possible:

> Although chaos in the Solar System may have been responsible for our own existence (through the impact of the asteroid that finished off the dinosaurs), it is the absence of chaos in the changing obliquity of the Earth that has, thanks to the presence of the Moon, allowed life on Earth to evolve under more or less stable climatic circumstances for billions of years.

Moreover, Nature doesn't need complex laws to generate the rich living systems that we find on Earth. As Stuart Kaufmann suggests,

> We are the natural expression of a deeper order.

After all, DNA's structure is very simple. It is the ways in which we can aggregate and combine it that makes humans and all other living organisms possible.

For example in “How the Leopard Gets Its Spots”, Murray found

> that not only the spots of the leopard, but also the stripes of the zebra, the blotches on a giraffe, and even the absence of patterning on the coat of a mouse or an elephant, can all be explained by the same simple process, involving diffusion of actuator and inhibitor chemicals across the surface of the developing embryo at a key stage during its growth.

So chaos gave rise to life on Earth but entropy and energy transfer means that all open systems must, at some point, get to equilibrium. And here equilibrium means stillness, the end of life. Then, if entropy is ever-increasing in the Universe, how can has life survived on Earth for millions of years?

### Gaia

Enter, James Lovelock's Gaia Hypothesis, one of the most eye-opening and intriguing concepts explained in the book. It argues that all living systems on Earth act in unison, in a sort of self-regulated network that keeps entropy in check and allows life to continue to be possible. In Gribbin's words it can be summed up as follows:

> A process co-evolution, in which all the species involved in a network change together when on of the changes, will naturally push complex ecosystems from either extreme toward the interesting region of self-organized criticality, in the phase transition on the edge of chaos. If a group of organisms is locked into a stable strategy, a mutation involving one of the species is likely to open up the network, allowing it to evolve. Evolution by natural selection will ensure that a change that is detrimental to the species involved will be washed away over the generations; but a change that is beneficial will spread, and by spreading it will open up more networks, pushing the system toward the edge of chaos.

If this theory is correct, then, as we've seen, only very simple ingredients are in fact needed to make life possible.

> Chaos and complexity combine to make the Universe a  very orderly place, just right for life-forms like us. As Stuart Kauffman has put it, we are “at home in the Universe.” But it isn't that the Universe has been designed for our benefit. Rather, we are made in the image of the Universe itself.
