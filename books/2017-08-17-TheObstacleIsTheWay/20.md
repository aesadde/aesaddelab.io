---
title:  20
created: 08/10/16
source:  The Obstacle Is The Way
categories: obstacles, perception
---

<!-- Front -->

You will discover, time and time again, that what matters most is not what these obstacles are but how we see them, how we react to them, and whether we keep our composure.

<!-- Back -->

