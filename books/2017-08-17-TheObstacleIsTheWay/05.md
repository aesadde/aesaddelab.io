---
title:  05
created: 08/07/16
source:  The Obstacle is the Way - Ryan Holiday
categories:  goals, persistence
---

<!-- Front -->

Don't think about the end -- think about surviving. Making it from meal to
meal, break to break, checkpoint to checkpoint, paycheck to paycheck, one day
at a time.

<!-- Back -->

This goes in line with what Cal Newport says about focusing in reasonable bursts
one day at a time.
