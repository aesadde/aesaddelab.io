---
title: Idea Makers
subtitle: Personal Perspectives on the Lives & Ideas of Some Notable People
created: 12/17/19
started: 11/17/19
ended: 11/24/19
author: Stephen Wolfram
publisher: Wolfram Media
rating: 10/10
thumbnail: ../../files/images/idea-makers.jpeg
link: https://amzn.to/2twNlgF
blurb: >
  Idea Makers is a wonderful collection of essays in which Wolfram explores the lives of people that have influenced his life and work. It contains stories of historical figures like Ada Lovelace and Liebniz and also stories about modern figures with whom he interacted (Richard Feynman, Steve Jobs). I enjoyed this book very much. The personal and curious style really captivated me.
---

This book was one of the best books I read this year.

The exposition is clear and refreshing. Wolfram writes with clarity and speaks from experience, relating many of the stories to his own work on [celullar automata](https://en.wikipedia.org/wiki/Cellular_automaton) and [Mathematica](https://www.wolfram.com/mathematica/).

The set of characters that he chose to wrote about is also peculiar and fun. There are essays about historical figures that had an influence in his work but that he never met. Other stories are about important figures that he interacted with and others still that are about less famous people that had an impact on him during their life.

Takeaways
---------

- Always be curious and do work that you enjoy. This was a theme across all characters in the book. They all seemed to deeply enjoy their work and their intellectual endeavors.

- Surround yourself with interesting people. This is one of the best ways to learn. As the saying goes "Surround yourself with wise men and you will look like one".

- Have a bias towards action. Experiment and feed your curiosity as much as you can.
