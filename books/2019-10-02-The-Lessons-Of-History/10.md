---
title:  10
created: 10/02/19
source:  2019-10-02-The-Lessons-Of-History
tags:
---

History is, above all else, the creation and recording of that heritage;
progress is its increasing abundance, preservation, transmission, and use.


