---
title:  07
created: 10/02/19
source:  2019-10-02-The-Lessons-Of-History
tags: complex systems
---

Democracy is the most difficult of all forms of government, since it requires
the widest spread of intelligence, and we forgot to make ourselves intelligent
when we made ourselves sovereign. Education has spread, but intelligence is
perpetually retarded by the fertility of the simple.
