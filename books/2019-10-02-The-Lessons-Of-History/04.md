---
title:  04
created: 10/02/19
source:  2019-10-02-The-Lessons-Of-History
tags:
---

"Racial" antipathies have some roots in ethnic origin, but they are also
generated, perhaps predominantly, by differences of acquired culture -- of
language, dress, habits, morals, or religion. There is no cure for such
antipathies except a broadened education.
