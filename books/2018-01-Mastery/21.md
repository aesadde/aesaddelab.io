---
title:  21
created: 01/10/18
source:  Mastery
categories: time, planning
---

<!-- Front -->

Take time for wise planning, but don't take forever.


"Whatever you can do, or dream you can --begin it"
"Boldness has genius, power and magic in it" -- Goethe

<!-- Back -->

