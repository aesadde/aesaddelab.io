---
title: What I Talk About When I Talk About Running
subtitle:
created: 02/17/20
started: 01/15/20
ended: 01/29/20
author: Haruki Murakami
publisher:
rating: 9/10
thumbnail: ../../files/images/murakami-running.png
link: https://amzn.to/2SADS1A
blurb: >
  Haruki Murakami describes his life using his running habit as a guiding principle.
  The book is as much as running as it is about life.
---

This book was very inspiring. To read, first hand, what it is like to run and build up the strength—mostly mental— that is needed to pretty much every day for more than two decades, is impressive.

I picked up this book to read more about running, to understand what goes through one’s mind while running a marathon and being under a lot of physical pain. But this book is much more than that. Through the running lens, Murakami paints a picture of his whole life. For him, running is a metaphor to writing, both are activities that require patience, endurance, focus and a lot of stubbornness.

<div id="note"> A gentleman shouldn’t go on and on about what he does to stay fit.
      At least that’s how I see it.
 [Talking about marathons]
</div>

 <div id="note"> If you don’t keep repeating a mantra of some sort to
      yourself, you’ll never survive.
</div>
 <div id="note"> Pain is inevitable.
      Suffering is optional.
</div>
 <div id="note"> I don’t know why but the older you
      get the busier you become.
[ As we become older we should try to go the opposite way. Look for ways to avoid distractions and be alone with yourself. Perhaps this is why Murakami runs so much, it is one of the only moments he is alone and avoiding being busy]
</div>
 <div id="note">
      Running without a break for more than two decades has also made me
      stronger, both physically and emotionally.
[Just the sheer willpower to force yourself to run over this timestamp is impressive]
</div>

 <div id="note"> I am much more interested in whether I reach the goals that
      I set for myself, so in this sense long-distance running is the perfect
      fit for a mindset like mind.
 [Remember, your battle and struggle is against yourself. Don’t compare yourself to others. We are all in different universes]
</div>
 <div id="note"> What’s crucial is whether your
      writing attain the standards you’ve set for yourself. Failure to reach
      that bar is not something you can easily explain away. When it comes to
      other people, you can always come up with a reasonable explanation, but
      you can’t fool yourself. In this sense, writing novels and running full
      marathons are very much alike.
[Hold yourself to the highest standards. “Be tolerant with others and strict with yourself.” – Marcus Aurelius]
</div>
<div id="note">
    Dostoyevsky, for instance, wrote two of his
      most profound novels in the last few years before his death at age sixty.
      Domenico Scarlatti wrote 555 piano sonatas during his lifetime, most of
      them when he was between the ages of fifty-seven and sixty-two.
[On late bloomers. This is very related to Range]
</div>
<div id="note">
      I find spending an hour or two every day running alone, not speaking to
      anyone, as well as four or five hours alone at my desk, to be neither
      difficult not boring.
[Cultivating alone time is essential for Deep Work]
</div>
<div id="note"> The thoughts that occur to me while I’m
      running are like clouds in the sky. Clouds of all different sizes. They
      come and they go, while the sky remains the same sky as always. … The sky
      both exists and doesn’t exist. It has substance and at the same time
      doesn’t. And we merely accept that vast expanse and drink it in.

[Meditation]
</div>
<div id="note"> The fact that I am <span
      style="font-style: italic;"> me </span> and no one else is one of my
      greatest assets. Emotional hurt is the price a person has to pay in order
      to be independent.
</div>
<div id="note"> I had to give everything I had. If
      I failed, I could accept that. But I knew that if I did things
      halfheartedly and they didn’t work out, I’d always have regrets.
[Bezos’s regret minimization framework]
</div>
<div id="note"> Running has
      a lot of advantages. First of all, you don’t need anybody else to do it,
      and no need for special equipment. You don’t have to go to any special
      place to do it. As long as you have running shoes and a good road you can
      run to your heart’s content.
</div>
<div id="note"> I’m struck by how, except
      when you’re young, you really need to prioritize in life, figuring out in
      what order you should divide up your time and energy. If you don’t get
      that sort of system set by a certain age, you’ll lack focus and your life
      will be out of balance. I place the highest priority on the sort of life
      that lets me focus on writing, not associating with all the people around
      me.
[Lifestyle design]
</div>
<div id="note"> As I continued to
      run, my body started to accept the fact that it was running, and I could
      gradually increase the distance. The main thing was not the speed or
      distance so much as running every day, without taking a break.
</div>

<div id="note"> Being active every day makes it easier to hear the inner voice.
</div>
<div id="note"> I never take two days off in a row.
[Routine]
</div>
<div id="note"> Let’s face it: Life is basically unfair. But even in
      a situation that’s unfair, I think it’s possible to seek out a kind of
      fairness.
[Stoicism]
</div>
<div id="note"> The body is an
      extremely practical system. You have to let it experience intermittent
      pain over time, and then the body will get the point.

      [Similar to intermittent fasting, and other dieting practices. Certain
      non-linear responses make you antifragile].
</div>
<div id="note"> On the
      highway of life you can’t always be in the fast lane.
      [Stilness]
</div>
<div id="note"> If I used being busy as an excuse not to run,
      I’d never run again.
</div>
<div id="note"> If you don’t have any fuel even the
      best car won’t run.
</div>
<div id="note"> If I’m asked what the next most
      important quality is for a novelist [first one if talent], that’s easy
      too: focus—the ability to concentrate all your limited talents on
      whatever’s critical at the moment.

[Deep Work]
</div>

<div id="note"> After focus, the next most important thing for a novelist is, hands
      down, endurance. If you concentrate on writing three or four hours a day
      and feel tired after a week of this, you’re not going to be able to write
      a long work.
</div>
<div id="note"> You can compare it to breathing. If
      concentration is the process of just holding your breath, endurance is
      the art of slowly, quietly breathing at the same Tim you’re storing air
      in your lungs. Unless you can find a balance between both, it’ll be
      difficult to write novels professionally over a long time.
</div>

<div id="note">
      <div id="note"> Add a stimulus and keep it up. And repeat. Patience is a must in
      this process, but I guarantee the results will come.
</div>
<div id="note">
      Exerting yourself to the fullest within your individual limits: that’s
      the essence of running, and a metaphor for life—and for me, for writing
      as well.
</div>
<div id="note"> It’s important to push your body to its limits,
      but exceed those and the whole thing’s a waste.
</div>
<div id="note"> This is
      my body, with all its limits and quirks. Just as with my face, even if
      I don’t like it it’s the only one I get, so I’ve got to make do. As I’ve
      grown older, I’ve naturally come to terms with this. You open the fridge
      and can make a nice—actually even a pretty smart— meal with the
      leftovers.
[Stoicism]
</div>
<div id="note"> To deal with
      something unhealthy, a person needs to be as healthy as possible.
</div>

<div id="note"> You have to wait until tomorrow to find out what tomorrow will
      bring.
</div>
<div id="note"> The end of the race is just a temporary marker
      without much significance. It’s the same with our lives. Just because
      there’s an end doesn’t mean existence has a meaning.
</div>
<div id="note"> Just
      as I have my own role to play, so does time. And time does its job much
      more faithfully, much more accurately, than I ever do.
      [Time is the ultimate goal, the ultimate tester]
</div>
<div id="note"> You can
      replace your breath any number of times, but not your knees. There are
      the only knees I’ll ever have, so I’d better take good care of them
</div>
<div id="note"> Learning something essential in life requires physical
      pain.
</div>
<div id="note"> I don’t care what others say—that’s just my nature,
      the way I am. Like scorpions sting, cicadas cling on trees, salmon swim
      upstream to where they were born, and wild ducks mate for life.
</div>

<div id="note"> It doesn’t matter how old I get, but as long as I continue to live
      I’ll always discover something new about myself. No matter how long you
      stand there examining yourself naked before a mirror, you’ll never see
      reflected what’s inside.
</div>
<div id="note"> One by one, I’ll face the tasks
      before me and complete them as best I can. Focusing on each stride
      forward, but at the same time taking a long-range view, scanning the
      scenery as far ahead as I can. I am, after all, a long-distance runner.
</div>
