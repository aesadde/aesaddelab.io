---
started: 05/30/2019
ended: 60/10/2019
title:  Good to Great
subtitle: Why Some Companies Make the Leap... and Others Don't.
author: Jim Collins
publisher: Harper Collins
rating: 10/10
thumbnail: https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fprojectlifemastery.com%2Fwp-content%2Fuploads%2F2012%2F11%2Fgood-to-great-book-review.jpeg&f=1&nofb=1
link:
blurb: > 
    Insightful ideas about why great companies become great. It is more about the team than the individual.
    Less ego, more curiosity and open communications.
    Many of the ideas exposed here can be used in your own life as well.
---

I first heard about Jim Collins on the Tim Ferriss' podcast. He came across as
a very humble and passionate person and only a few minutes into the podcast
I was already ordering some of his books.

Good to Great is the first of Jim Collins' books that I read. It was difficult
to put it down. The research that Jim and his team conducted seems very simple
in retrospect but also very thorough.

The flywheel concept, which is distilled throughout the book is something that
I think we all should incorporate into our daily lives. It is not something that
belongs only to the business world.

As usual, what follows are my notes an highlights of the book. These might not
make sense to you. I used them mostly to come back to them when I need to
remember something from the book.
