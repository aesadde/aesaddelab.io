---
title:  06
created: 10/24/19
source:  2019-10-20-Ultralearning
tags: plan
---


The core of the ultralearning strategy is intensity and a willingness to
prioritize effectiveness.
