---
title:  03
created: 11/03/19
source:  2018-03-12-Zen-Mind-Beginners-Mind
tags: practice, mastery
---

The mind of the beginner is empty, free of the habits of the expert, ready to
accept, to doubt, and open to all possibilities. It is the kind of mind which
can see things as they are, which step by step and in a flash can realize the
original nature of everything.


