---
title:  05
created: 11/03/19
source:  2018-03-12-Zen-Mind-Beginners-Mind
tags: mastery
---


When we have no thought of achievement, no thought of self, we are true
beginners. Then we can really learn something.

