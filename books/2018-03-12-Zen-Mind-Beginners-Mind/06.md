---
title:  06
created: 11/03/19
source:  2018-03-12-Zen-Mind-Beginners-Mind
tags: life, purpose, meaning
---


Doing something is expressing our own nature. We do not exist for the sake of
something else. We exist for the sake of ourselves.

