---
title:  04
created: 06/28/19
source:  2019-06-28-The-Greatest-Empire
categories: stoicism, reason
---

<!-- Front -->
The Stoics believe that humans are entirely capable of understanding the
universe, and also that human happiness depends on our ability to think
properly. The ideal is to align our own minds with the rational will of the
universe.
<!-- Back -->
