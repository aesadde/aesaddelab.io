---
title:  15
created: 06/28/19
source:  2019-06-28-The-Greatest-Empire
categories: stoicism
---

<!-- Front -->
The Stoics categorized the bad kinds of emotion into four general types: pleasure, pain, desire, and fear. Excessive emotions of these types are, they thought, the most important reason why people may fail to achieve appropriate spiritual tranquility.
<!-- Back -->
