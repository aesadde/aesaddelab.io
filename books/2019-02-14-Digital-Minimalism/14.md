---
title:  14
created: 02/24/19
source:  2019-02-14-Digital-Minimalism
categories: loneliness, psychology
---

<!-- Front -->

The more time you spend "connecting" on these services, the more isolated you're likely to become.

<!-- Back -->
