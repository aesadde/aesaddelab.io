---
started: 09/21/19
ended: 10/06/19
created: 11/16/19
title:  What Technology Wants
subtitle:
author: Kevin Kelly
publisher:
rating: 10/10
kind: book
link: https://www.amazon.com/What-Technology-Wants-Kevin-Kelly/dp/0143120174
thumbnail: https://upload.wikimedia.org/wikipedia/en/thumb/1/17/What_Technology_Wants%2C_Book_Cover_Art.jpg/220px-What_Technology_Wants%2C_Book_Cover_Art.jpg
isbn-13: 978-0143120179
isbn-10: 0143120174
blurb: >
    Bold ideas about human progress and where technology is going. You will see progress and society with a new lens after you read it.
---

I first learned about Kevin Kelly on the [Tim Ferriss podcast]. The book is a
treatise, almost philosophical, about how Kelly sees technology
and humanity as a whole.

As the title, suggests, Kelly tries to understand what is technology, where it
is going, and why. What does it want? The whole book is an almost philosophical
treatise. It is packed with historical and scientific anecdotes that will make
you think a lot.

The key idea though is the idea of the *technium*, a term coined by Kelly that
describes the set of humanity's creations: culture, history, techonoligies.

How we got here? The Universe has established physical rules that limit the
number of possibilities. So, in a sense, how we got here was inevitable, the
rules of the game are already established. Kelly argues that if we replay
history back we will see the same things happen again, at least at a macro
scale: Einstein might not be Einstein in another history but the principles of
Relativity would have been discovered nonetheless.

Kelly sees the technium as a complex system with a life of its own. This is a
compelling idea especially when you see the rate of progress of humanity. In
just a minuscule fraction of the history of the Universe we've evolved from
[startdust](https://www.youtube.com/watch?v=tLPkpBN6bEI) to almost
[cyborgs](https://www.theverge.com/2016/6/2/11837854/neural-lace-cyborgs-elon-musk).

