---
title:  04
created: 12/17/19
source:  2019-12-17-Idea-Makers
tags: gödel, theorem
---

Kurt Gödel
----------

So here, then, is a statement within mathematics that is unprovable by mathematics: an "undecidable statement". And its existence immediately shows that there is a certain incompleteness to mathematics: there are mathematical statements that mathematical statements cannot reach.

...

If we study a system that is capable of universal computation we can no longer expect to "outcompute".

...

This is why it can be so difficult to predict what computers will do—or to prove that software has no bugs.

...

One might think of undecidability as a limitation to progress, but in many ways it is instead a sign of richness. For with it comes computational irreducibility, and the possibility for systems to build up behavior beyond what can be summarized by simple formulas. Indeed, my own work suggests that much of the complexity we see in nature has precisely this origin. And perhaps it is also the essence of how from deterministic underlying laws we can build up apparent free will.

