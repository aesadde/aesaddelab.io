---
started: 10/11/19
ended: 10/22/19
created: 11-17-2019
title: Il Deserto Dei Tartari
subtitle:
author: Dino Buzzati
publisher: Oscar Moderni
rating: 6/10
kind: book
link:
thumbnail: https://images-na.ssl-images-amazon.com/images/I/91DH8%2Bw3z-L.jpg
isbn-13:
isbn-10: 978-88-04-66804-6
blurb: >
  Giovanni Drogo is sent on assignment to the border of the Kingdom. There, life
  is monotonous, time passes unnoticed and the soldiers get used to it. Drogo
  dies waiting for an enemy that will never arrive.
  I found this book melancholic and a sort of wake-up call. Time advances
  relentlessly. If we don't act now, if we don't get busy living there won't
  anything left for us but the past.
---

  I bought this book after reading that [Nassim
  Taleb](../../../links.html#nassim-taleb) used to divide people between those
  who had read the "Tartars' Steppe" and those who hadn't. It is also a book
  that appears on ["Le Monde's 100 books of the
  century"](https://en.wikipedia.org/wiki/Le_Monde%27s_100_Books_of_the_Century).

  I also wrote about this in one of my [weekly
  emails](https://us3.campaign-archive.com/?u=3d9e1a5fac6e2b16b24b4808a&id=e78fcb4b6e).
  You should check it out!

  "Il Deserto dei Tartari" was the first book I've read in Italian in a long
  time. I found it surprisingly easy to read and I think that the language also
  plays a role in the tone of the novel. I'm sure it wouldn't transmit me that
  feeling of nostalgia if I had read it in English.

  The lesson I drew from the book is one about time. It passes even if we're not
  paying attention. Many times we put things off because we have more time,
  because we are young, because there's always tomorrow. This is not true. [We
  could leave life right now
  ](https://dailystoic.com/you-could-leave-life-right-now-let-that-determine-what-you-do-and-say-and-think/).

