---
title:  11
created: 10/30/19
source:  2019-10-30-What-Technology-Wants
tags: history, evolution, future
---

A world without technology had enough to sustain survival but not enough to
transcend it. Only when the mind, liberated by language and enabled by the
technium, transcended the constraints of nature 50,000 years ago did great
realms of possibility open up. There was a price to pay for this transcendence,
but what we gained by this embrace was civilization and progress.


