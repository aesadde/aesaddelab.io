---
title:  32
created: 10/30/19
source:  2019-10-30-What-Technology-Wants
tags: action, progress
---

The principle of constant engagement is called the Proactionary Principle.
Because it emphasizes provisional assessment and constant correction, it is a
deliberate counter-approach to the Precautionary Principle.


