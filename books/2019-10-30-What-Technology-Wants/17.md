---
title:  17
created: 10/30/19
source:  2019-10-30-What-Technology-Wants
tags: sympatheia, connectedness
---

The atoms in our bodies were produced in the dust of a star. We are much older
than we look.


