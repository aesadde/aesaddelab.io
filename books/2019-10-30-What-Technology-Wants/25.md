---
title: 25
created: 10/30/19
source:  2019-10-30-What-Technology-Wants
tags: free will, determinism, sympatheia
---


Despite life's magnificent diversity, it is chiefly repeating, billions of
billions of times, solutions that worked before.
