---
title:  02
created: 10/30/19
source:  2019-10-30-What-Technology-Wants
tags:
---


I continue to keep the cornucopia of technology at arm's length so that I can
more easily remember who I am.


Use technology but not be used by it. We must live by design not by default as
[[Greg McKeown]] from [[Essentialism]] says.

