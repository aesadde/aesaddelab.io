---
title:  08
created: 10/30/19
source:  2019-10-30-What-Technology-Wants
tags: diet, lifestyle
---

They had a varied menu of tubers, vegetables, fruit, and meat. Based on studies
of bones and pollen in their trash, so did the early Sapiens.

"Paleolithic rhythm" —a day or two on, a day or two off.

When food is there, all work very hard. When it is not, no problem; they will
sit around and talk while they are hungry. This very reasonable approach is
often misread as tribal laziness, but it is in fact a logical strategy if you
rely on the environment to store your food.

..when they spoke of shortages, or even hunger, they meant a shortage of meat,
and a hunger for fat, and a distaste for periods of hunger.
