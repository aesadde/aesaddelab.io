---
title:  01
created: 10/30/19
source:  2019-10-30-What-Technology-Wants
tags:
---

Online networks unleashed passions, compounded creativity, amplified generosity.
At the very cultural moment when pundits declared that writing was dead,
millions began writing online more than they ever had written before. Exactly
when the experts declared people would only bowl alone, millions began to gather
together in large numbers.


The Internet has given us [[optionality]]. As the world moves online we have
more choices. The options are infinite. This is a double-edged sword. With the
many benefits also come a lot of risks.

