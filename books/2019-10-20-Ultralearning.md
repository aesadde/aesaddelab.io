---
started: 08/14/19
ended: 08/18/19
title:  Ultralearning
subititle: >
  Master Hard Skill, Outsmart the Competitionn and Accelerate Your Career
created: 10/20/19
tags:
author: Scott H. Young
publisher: Harper Business
kind: book
rating: 6/10
link: https://www.amazon.com/Ultralearning-Master-Outsmart-Competition-Accelerate/dp/B07ST3Z1Q6/ref=sr_1_2?crid=1B0SETKNPO4P7&keywords=ultralearning&qid=1573944015&sprefix=ultralearning%2Caps%2C195&sr=8-2
thumbnail: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.scotthyoung.com%2Fblog%2Fwp-content%2Fthemes%2Fshy-theme%2Fimages%2Ful-sp-cover-2.png&f=1&nofb=1
blurb: >
  Tips and tricks to do learn more in an effective way. A lot of learning techniques are compressed in this book. Highly reccommended if you are learning new things.
---

I have been following Young's experiments and learning advice for more than 5
years, since my days in college. I have managed to incorporate some of his tips
into my own learning projects so I was very happy when he published this book.

The book distills and condenses a lot of knowledge from his blog. It is very
easy to read and most of the tips and tricks that he explains are backed up by
research in Psychology and actual experiments that he or a few test subjects
have carried out.

If you are embarking on a new *learning project* I encourage you to give this
book a read and try to apply some of the principles. The key is to experiment
and see what works for you. Don't take things for granted!

To summarize, the book presents a set of principles for ultralearning.

### Meta learning.

  This is the first step in the process and should be carried out with care.

  Here is where you plan what you want to learn and how.

  Questions to ask are:

  Why? Is this something instrumental for your career, a means to an end,
  curiosity?

  What? Topics, concepts, etc. you want to learn.

  How? Depends on "what". Here you need to adapt the choose the topics you want
  to learn. You need to also decide on timing, how much you want/can spend
  learning?

  Feedback. How are you going to get it? This is perhaps the most important bit
  in my experience. Good feedback can skyrocket your learning.

### Focus

  Learning something new is hard. Try to avoid distractions. The key idea here
  is to use the environment in your favor. For example study in a library.

### Directness

  Learn by doing.

  Practice as much as you can. Get your hands dirty.

### Drills

  Determine your weakest points and drill on those. Repeat.

  Practice mindfully and deliberately (see [Deep
  Work](./2016-04-30-DeepWork.html) by Cal Newport).


### Retrieval

  Actively review the material. This should be something you do before every
  session.

  Use flashcards and spaced repetition.

  Have some free recall sessions in which you try to recall as much of the
  material as possible.

### Feedback

  Don't fear it.

  Avoid negative/personal feedback. This will hurt you and not help you in your
  learning process.

  Try to find corrective feedback. This type of feedback helps you amend your
  mistakes and move forward.

### Retention

 Forgetting is exponential.

 We tend to forget the latest things we have learned. Make sure you practice
 them.

 Keep in mind that memories erode over time, this is why habits like spaced
 repetition are key.

 Vivid memories decay more slowly. For more about memory check "Moonwalking with
 Einstein".


### Intuition

  Intuition is really a lot of experience packed together.

  Try to think in [first principles](https://fs.blog/2018/04/first-principles/).

  Try hard problems. Set yourself a "struggle timer" and attempt difficult
  problems before reading the solutions.

  Use the Feynman technique: attempt to explain things in the simplest way
  possible. If you get stuck, go back to the material and repeat. You don't
  really know something until you are able to explain it, or write it. Remember,
  "writing is thinking". Use this to your advantage.


### Experimentation

 Always have a bias towards action. Trial and error is Nature's most useful
 learning tactic.


Finally, as usual, below are my highlights from the book.
