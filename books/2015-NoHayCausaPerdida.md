---
title: No Hay Cause Perdida
author: Álvaro Uribe
started: 05/29/2015
ended: 06/01/2015
publisher:
rating: 6/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51CjOfyC1wL._SX346_BO1,204,203,200_.jpg
link: 
blurb: >
    Engaging inside view of Uribe's time as president of Colombia. 
    Regardless of what you think of him, he was a key figure in setting
    Colombia on a path to success.
---
No Hay Causa Perdida, por Álvaro Uribe Vélez.

Este libro trata de la vida dentro y fuera de la política de Álvaro Uribe, un
personaje que ha estado siempre rodeado de críticas y polémicas. Para muchos él
cambió a Colombia para siempre, para otros él fue simplemente un autócrata más
en Latino América, un caudillo que no pudo perpetuarse en el poder.

Dejando estás polémicas atrás, el libro es muy bueno. Uno puede leer y entender
muchas de las decisiones de Uribe en la presidencia. Entender como logró llegar
a la presidencia. Su camino sin duda fue admirable, un ejemplo a seguir.

Uribe siempre trabajó duro. Nunca se dejó influenciar por el poder y el dinero,
o así al menos el escribe. Nadó siempre contra la corriente cuando sentía que
hacía lo correcto. Admirable de verdad.

Lo más interesante de su carrera política es que para algunos sus políticas eran
neoliberales, de derecha (los tratados de libre comercio con EEUU, programas de
austeridad y recortes del gasto público). Para otros Uribe fue un socialista
más. Sus programas sociales, y el "impuesto de seguridad" cobrado sólo a
ciudadanos de altos recursos son vistos como ejemplo de su izquierdismo.
Yo creo que en nuestro continente nada se puede aplicar de las maneras
tradicionales. Por ser tan diversa, América Latina ha de buscar sus propias
definiciones y soluciones a nuestros problemas y reto. Ningún concepto o idea
puede aplicarse de la manera tradicional. Después de todo, ni siquiera las
tradiciones religiosas son tradicionales: nuestras fiestas son siempre una
mezcla de culturas y religiones.
Del mismo modo, hemos de buscar soluciones propias, no tradicionales a nuestros
retos y problemas. Uribe y Colombia dan fe de esto. Después de décadas de
soluciones tradicionales fallidas, Colombia logró salir de la oscuridad gracias
al trabajo duro de sus ciudadanos y gracias a políticas y planes originales.
