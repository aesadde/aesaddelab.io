---
title: A Guide to the Good Life
subtitle: The Ancient Art of Stoic Living
author: William B. Irvine
started:
finished:
publisher:
rating: 9/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/41GaZyCpZHL._SX355_BO1,204,203,200_.jpg
link:
blurb: >
    This is very easy to read and actionable book on Stoicism. 
    I reccommend it as an introductory book on the subject.
---

Other than Marcus Aurelius' "Meditations" and Seneca's "Letters From a Stoic",
this is the first book that I read on Stoic living and I enjoyed it from start
to finish.  It is short and has a lot of good tips to incorporate Stoic living
and thinking into your daily life.
