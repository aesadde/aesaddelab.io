---
title:  13
created: 11/03/19
source:  2018-10-18-Why-We-Sleep
tags:
---

[Deep sleep]

This feature, termed "atonia" (an absence of tone, referring here to the
muscles), is instigated by a powerful disabling signal that is transmitted down
the full length of your spinal cord from your brain stem.




