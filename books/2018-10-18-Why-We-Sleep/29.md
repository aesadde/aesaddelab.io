---
title:  29
created: 11/03/19
source:  2018-10-18-Why-We-Sleep
tags: paradox, sleep deprivation
---

Wakefulness is low-level brain damage, while sleep is neurological sanitation.

Without sufficient sleep, amyloid plaques build up in the brain, especially in
deep-sleep-generating regions, attacking and degrading them. The loss of deep
NREM sleep caused by this assault therefore lessens the ability to remove
amyloid from the brain at night, resulting in greater amyloid deposition. More
amyloid, less deep sleep, less deep sleep, more amyloid, and so on and so forth.


