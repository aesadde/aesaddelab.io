---
title:  24
created: 11/03/19
source:  2018-10-18-Why-We-Sleep
tags: proverb, quote
---


Practice, *with sleep*, makes perfect.

