---
title:  34
created: 11/03/19
source:  2018-10-18-Why-We-Sleep
tags: eyes, memory, learning
---

Humans are predominantly visual creatures. More than a third of our brain is
devoted to processing visual information.


