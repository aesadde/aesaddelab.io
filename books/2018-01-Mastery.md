---
started: 01/2018
ended: 01/2018
title: Mastery
author: George Leonard
publisher: Penguin Random House LLC
rating: 8/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/81TpFhK-pUL.jpg
blurb: >
    Mastery is about loving the plateu, about keeping a beginner's mind.
---
Compré este libro porque lo recomendaron en el podcast the Tim Ferriss.
El que recomendó el libro era Terry Laughlin, el creador de una muy buena
técnica para aprender a nadar "Full Immersion Swimming".


La verdad que el libro es muy bueno, corto pero preciso, muy al punto.

Lo más importante del libro es entender a *amar* el "plateau", esa fase
del aprendizaje en donde nos sentimos estancados, que no avanzamos. Es ahí,
cuando menos lo esperamos, que suceden los grandes avances, que nos damos
cuenta de lo que vamos aprendiendo y de como vamos mejorando.

La vida al final es una sucesión de muchos "plateaus".
