---
title:  03
created: 09/04/19
source:  2019-09-04-The-Undercover-Economist
tags: monopoly, competition, effort
---

A company with stiff competition will be less profitable than a company with
incompetent rivals.


<!-- #zero-to-one this is related to the notion of avoiding or escpaing
competition. Peter Thiel and Jim Collins have some things to say about this. -->




