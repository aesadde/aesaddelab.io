---
started: 09/15/19
ended: 09/19/19
created: 10/13/19
title:  How to Take Smart Notes
subtitle: >
  One Simple Technique to Boost Writing, Learning and Thinking — for
  Students, Academics and Nonfiction Book Writers
author: Sönke Ahrens
publisher: CreateSpace
rating: 8/10
kind: book
link: https://www.amazon.com/How-Take-Smart-Notes-Nonfiction/dp/1542866502/ref=sxts_sxwds-bia?keywords=How+To+Take+Smart+notes&pd_rd_i=1542866502&pd_rd_r=28088045-4e91-433e-aee1-54ad43d5d3d2&pd_rd_w=63hDX&pd_rd_wg=7Yvf4&pf_rd_p=1cb3f32a-ccfd-479b-8a13-b22f56c942c6&pf_rd_r=2NQX5CY7RANR0ZTJFJKX&psc=1&qid=1573946109
thumbnail: https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1528273195i/34576082._UY200_.jpg
blurb: >
  Learning and remembering things is always hard. In school we are taught a very rigid way of taking and organizing notes, mainly by topic and date. This book describes Niklas Luhman's Zettelkasten, a relational approach based on links between concepts and ideas. 
  This is related to the notecard system used by Ryan Holiday and other authors.
---

Ever since I read about Ryan Holiday's notecard system I have been experimenting
with different ways of remembering what I read and keeping notes about my
bibliography. I've been through multiple iterations, most of them digital, and
none of them hace satisfied me. I have always felt that my notes were not
actionable enough.

Doing some research online, I discovered the concept of a *Zettelkasten*,
literally slip box. A system used by Luhman a German sociologist that attributes
to this system the ability to publish 70+ books during his life. Intrigued, I
did some more research and found this book, *How to Take Smart Notes*.

The book describes in a very actionable way how to use a *Zettelkasten* for
academic research and nonfiction writing. I found it very insightful. I've since
adapted some of it suggestions to my own notecards system with some success. I
find it that I can now think better about what I read and link topics in a much
better yet flexible manner.

As always, below are the passages that I found important.

