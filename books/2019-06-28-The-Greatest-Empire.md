---
started: 03/06/19
ended: 03/14/19
title: The Greatest Empire
subtitle: A Life of Seneca
author: Emily Wilson
publisher: Oxford University Press
rating: 6/10
link:
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51PdiMFNuuL._SX330_BO1,204,203,200_.jpg
blurb: >
    A holistic biography of Seneca. Wilson doesn't does an excellent job in
    describing the controversies in Seneca's life. On one hand we see him
    writing letters to his friends arguing in favor of the Stoic ideals. On
    the other he advises Nero, the Roman emperor and tyrant of his time.

---

Seneca is one of the most famous stoic philosophers of all times. Not because he
was perfect and embodied the "Stoic ideal" but, quite the opposite, because he
was a flawed man with his own struggles and flaws.

This book does a superb job at describing him from all angles. It depicts him
as a man with many issues and contradictions (remember he was an advisor to
Nero, one of the most brutal and inhuman Roman emperors). It does so by matching
important events to some of this writings, but in doing so, some times the book
feels slow and filled with some unnecessary detail.

Overall though, the story seems very accurate and real. It helped remember many
events of Roman history that I had studied in high school and also helped bring
back to mind old memories of philosophical discussions with Michele Rozzi, my
wonderful History and Philosophy professor.

As usual, below are the passages I highlighted.

For another Stoic character check [Rome's Last
Citizen](./2018-01-RomesLastCitizen.md), the biography of Cato.
