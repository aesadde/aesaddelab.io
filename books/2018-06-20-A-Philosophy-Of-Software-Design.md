---
started: 06/20/2018
ended: 06/24/2018
title: A Philosophy of Software Design
author: John Ousterhout
publisher: Yaknyam Press
rating: 7/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51t2s-WNb3L._SX405_BO1,204,203,200_.jpg
link:
blurb: >
    Every software engineer should understand the difference between Tactical
    and Strategic programming.
---

I came to know about this book in a Hacker News post and decided to order it as
soon as it was published.

The book proved very useful since the first chapter. Since I read it I've
applied many of its concepts and many times while coding I find myself thinking
about `Tactical` vs. `Strategic` programming, two concepts that really stuck
with me and have helped me write better code.

Tactical programming refers to writing and shipping code fast without stopping
too much to think about design. This might be good for fast prototyping but it
is not the correct approach when building products. Instead we should use
strategic programming: plan ahead and design the system before writing any
code.

If you want to know more there's also a [Google Talk by the author!](https://www.youtube.com/watch?v=bmSAYlu0NcY)

