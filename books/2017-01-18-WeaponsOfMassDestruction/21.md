---
title:  21
created: 01/18/17
source:  WeaponsOfMassDestruction
categories: data, privacy
---

<!-- Front -->

"Much of the proxy data ollected, whther step counts or sleeping patterns, is
not protected by law, so it would theoretically be perfectly legal. And it
would make sense. As we've seen, they routinely reject applicants on the basis
of credit scores and personality tests. Health scores represent a natural - and
frightening - next step."

<!-- Back -->

This is a bit paradoxical since it would seem that being healthy is beneficial
not only to ourselves but to society as well. If anything, using health data we
can start developing good habits and maybe then avoid being ruled out by these
algorithms

