---
title:  17
created: 01/17/17
source:  WeaponsOfMassDestruction
categories: inequality
---

<!-- Front -->

"Under the inefficient status quo, workers had not only predictable hours but
also a certain amount of downtime. You could argue that they benefited from the
inefficiency: some were able to read on the job, even study. Now, with software
choreographing the work, every minute should be busy. And these minutes will
come whenever the program demands it, even if it means clopening from Friday to
Saturday"

<!-- Back -->

Not all new is good just for the sake of it. New systems need prove they are
robust and bring more benefits than past methods.

