---
title: Brideshead Revisited
author: Evelyn Waughn
started: 02/12/15
ended: 05/12/15
publisher:
kind: book
rating: 5/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51yrqamJWWL._SX342_BO1,204,203,200_.jpg
link:
blurb: >
    This is an amazing novel set up in the period between the two wars.
---

First time in a long while that I read some fiction.
This is an amazing novel set up in the period between the two wars.
The author does an incredible job in describing scenes and characters'
characters.

The novel engaged me, I read furiously for hours, I read about Oxford and it
was so nice to actually know the places where the novel took place.

Other than that, the description of how religious families are often limited
by their beliefs and how sometimes life can be hampered, limited and even
destroyed by religion. I am myself a catholic, probably not a good one, I have
my doubts, my regrets, my fears but I try to follow my life as simple as
possible basing my decisions on simple moral rules not on hard and strict
religious dogma.
