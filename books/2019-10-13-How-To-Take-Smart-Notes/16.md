---
title:  16
created: 10/13/19
source:  2019-10-13-How-To-Take-Smart-Notes
tags: learning, deep work
---


Taking literature notes is a form of deliberate practice as it gives us feedback
on our understanding or lack of it, while the effort to put into our own words
the gist of something is at the same time the best approach to understanding
what we read.
