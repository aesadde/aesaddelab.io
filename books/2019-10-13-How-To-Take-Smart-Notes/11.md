---
title:  11
created: 10/13/19
source:  2019-10-13-How-To-Take-Smart-Notes
tags: pscychology
---


Zeigarnik effect: Open tasks tend to occupy our short-term memory - until they
are done.
