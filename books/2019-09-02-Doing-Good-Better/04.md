---
title:  04
created: 09/02/19
source:  2019-09-02-Doing-Good-Better
tags: feedback, investing
---

One difference between investing in a company and donating to a charity is that
the charity world often lacks appropriate feedback mechanisms. Invest in a bad
company, and you lose money; but give money to a bad charity, and you probably
won't hear about its failings. Buy a shirt that's advertised as silk when it's
really polyester, and you'll realize pretty quickly; but buy coffee that has a
Fairtrade stamp on it, and you never know whether doing so helped people, harmed
them, or did nothing.


