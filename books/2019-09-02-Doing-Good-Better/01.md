---
title:  01
created: 09/02/19
source:  2019-09-02-Doing-Good-Better
tags: science, data, reason
---

I believe that by combining the heart and the head -- by applying data and
reason to altruistic acts -- we can turn our good intentions into astonishingly
good outcomes.
