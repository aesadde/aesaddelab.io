---
title: The Control Of Nature
subtitle:
created: 01/26/20
started: 12/11/19
ended: 01/12/20
author: John McPhee
publisher: Farrar, Straus and Giroux
rating: 7/10
thumbnail: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FI%2F415A-zVbffL._SY291_BO1%2C204%2C203%2C200_QL40_.jpg&f=1&nofb=1
link: https://amzn.to/36oXM3q
blurb: >
  A descriptive and entertaining collection of essays about humanity's constant
  battle against Nature. McPhee tells us about the Americans' efforts to control
  the flow of the Mississipi and the debris flow of the Los Angeles Mountains
  and the battle between the Icelanders and volcanoes.
---

This book is a collection of three essays that were published in the New Yorker between 1988 and 1989.

The first one, Atchafalaya, recounts the story of how the Army Corps of Engineers have been trying to control the flow of the Mississippi for decades. It is these efforts that make most parts of Louisiana habitable and prevents "New Orleans to end up in Yucatán".

The second essay, Cooling the lava, describes the fight that Icelanders waged against [Eldfell](https://en.wikipedia.org/wiki/Eldfell) a volcano that formed during an eruption in 1973 in the island of Heimaey. The eruption threatened life on the island and threatened to bury and destroy the island's harbor. Desperate, the islanders attempted the biggest lava cooling operation in history. For weeks they pumped water out of the ocean and sprayed it over the lava. This was meant to cool the lava and solidify it in an attempt to slow down its march through the island and into the harbor. The islanders succeeded but even then life in Heimaey changed forever. Many locals evacuated and didn't return, the island expanded, and more than 400 houses were destroyed.

The third, and final, essay, Los Angeles against the Mountains, describes the fight that the fight of the citizens of Southern California against nature. The focus is on the debris flows that slide down the mountains after heavy rains and the subsequent fires that hit most of California during the dry season in the fall. This was the essay I related to the most. Not only I live in California and understand all the challenges that it means to live here, but the descriptions of the debris flows brought sad memories of the [Vargas tragedy](https://en.wikipedia.org/wiki/Vargas_tragedy) where, after weeks of continuous rain, the mountains slid towards the sea killing and burying everything in their path.

All in all, this book makes for a very interesting read. I enjoyed reading about different challenges and efforts to control Nature. I learned quite a bit of American geography. But most of all, I enjoyed the book because McPhee is an exemplar journalist and story teller. His choice of words are more than perfect. His descriptions and analogies seem to be always accurate and timely, this really captivated me throughout the book.

### Links

- The original essays are now online on [The New Yorker's site](https://www.newyorker.com/magazine/the-control-of-nature).

- Recently, in 2018, McPhee wrote a fourth essay, it also can be read [online](https://www.newyorker.com/magazine/2018/03/05/in-search-of-new-jerseys-wild-bears).



