---
title:  08
created: 11/24/19
source:  2019-11-24-Stillness-Is-The-Key
tags: help, feedback, mentors
- --

If Zeno and Buddha needed teachers to advance, then we will *definitely* need help. And the ability to admit that is evidence of not a small bit of wisdom!

Find people you admire and ask how they got where they are. Seek book recommendations. Isn't that what Socrates would do?


