---
title:  10
created: 11/24/19
source:  2019-11-24-Stillness-Is-The-Key
tags: comfort, distractions, desire
---

Lust is a destroyer of peace in our lives: Lust for a beautiful person. Lust for an orgasm. Lust for someone other than the one we've committed to be with. Lust for power. Lust for dominance. Lust for other people's stuff. Lust for the fanciest, best, most expensive things that money can buy.


