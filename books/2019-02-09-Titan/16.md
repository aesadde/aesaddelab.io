---
title:  16
created: 02/18/19
source:  2019-02-09-Titan
categories: work
---

<!-- Front -->

At first, he tested them exhaustively, yet once he trusted them, he bestowed
enormous power upon them and didn't intrude unless something radically
misfired. "Often the best way to develop workers -- when you are sure they have
character and think they have ability -- is to take them to a deep place, throw
them in and make them sink or swim".

<!-- Back -->

