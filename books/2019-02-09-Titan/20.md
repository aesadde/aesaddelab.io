---
title:  20
created: 02/18/19
source:  2019-02-09-Titan
categories: markets, business
---

<!-- Front -->

Standard Oil had taught the American public an important but paradoxical
lesson: Free markets, if left completely to their own devices, can wind up
terribly _unfree_.

<!-- Back -->

