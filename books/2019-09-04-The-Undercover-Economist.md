---
title:  The Undercover Economist
subtitle:
created: 09/02/19
started: 06/18/19
ended:
rating: 5/10
author: Tim Harford
publisher: Random House
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51ikHTWMM2L._AC_SY400_.jpg
link: 
blurb: >
    This is an informal introduction to core Economics principles. It took me a while to read it.
    I didn't enjoy the style. There are some useful ideas though.
---

I picked up this book in an effort to read something different from what I
usually read. Been skeptical of Economics as a discipline, let alone a Science,
I was trying to understand a bit more how Economist think.

This book attempts to introduce some core principles of Economics using
day-to-day examples. For example it describes the law of offer and demand by
explaining why coffee is usually overpriced. While this approach was nice, the
style of the author didn't click with me. I found most of the book boring and
not really very engaging.
