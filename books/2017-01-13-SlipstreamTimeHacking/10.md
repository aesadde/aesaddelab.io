---
title:  10
created: 11/22/16
source:  Slipstream Time Hacking
categories: life, goals
---

<!-- Front -->
What if you could have a conversation with your future self?

What vital information would your future-self give you?

What is your vision for the future?

<!-- Back -->

