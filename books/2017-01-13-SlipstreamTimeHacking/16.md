---
title:  16
created: 11/22/16
source:  Slipstream Time Hacking
categories: time, quotes
---

<!-- Front -->

Dieter Utchdorf has said, “What do you suppose pilots do when they encounter
turbulence? A student pilot may think that increasing speed is a good strategy
because it will get them through the turbulence faster. But that may be the
wrong thing to do. Professional pilots understand that there is an optimum
turbulence penetration speed that will minimize the negative effects of
turbulence. And most of the time that would mean to reduce your speed. The same
principle applies also to speed bumps on a road. Therefore, it is good advice
to slow down a little, steady the course, and focus on the essentials when
experiencing adverse conditions.

<!-- Back -->

