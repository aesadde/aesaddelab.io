---
title:  08
created: 11/17/19
source:  2019-11-17-Il-Deserto-Dei-Tartari
tags: time
---

Il tempo intanto correva, il suo battito silenzioso scandisce sempre piú
precipitoso la vita, non ci si puó fermare neanch un attimo, neppure per
un'occhiata indietro. "Ferma, ferma!" si vorrebbe gridare, ma si capisce ch'é
inutile.


