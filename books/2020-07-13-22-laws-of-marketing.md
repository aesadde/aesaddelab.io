---
title: "The 22 Immutable Laws of Marketing"
subtitle: Violate Them at Your Own Risk
created: 07/13/2020
started: 05/22/2020
ended:   05/25/2020
author: Al Ries and Jack Trout
publisher: Harper
rating: 10/10
thumbnail: /files/images/22-laws.jpg
link:
blurb: >
 A concise and practical guide to marketing.
---

### Overview

This book is very short and actionable. It can be read in one sitting. If you're a marketer you've probably read it or know about it since it is one of the most famous and timeless pieces on the art of marketing. If you're not—like me—then you should read it. You will understand what marketing is all about:

- Marketing is a war. Companies are fighting all the time to get into the prospects' minds.
- Marketing is a long-term game. Short-term wins are usually counterproductive. You want to play the long game and become an essential part of the prospects minds. You must get into their memories.
- Marketing costs money, lots of money. This is not surprising. It's the reason why companies like Google and Facebook make billions selling ad space on their sites.
- Marketing is about positioning. Avoid competition. Try to always be first in the market and in the minds. Create your own category (see [Blue Ocean Strategy](https://aesadde.xyz/books/2020-05-23-blue-ocean-strategy/)).

—

The format of the book is quite simple: one chapter per law or one law per chapter with lots of relevant examples. What follows is a brief summary of all the laws.

1. **The Law of Leadership = be first in the market.**

    Example: "Gillette was the first safety razor. Tide was the first laundry detergent. Hayes was the first computer modem. Leaders all."

2. **The Law of Category = if you're not first create a new category in which you can be the first.**

    Example: "Charles Schwab didn't open a better brokerage firm. He opened the first discount broker."

3. **The Law of the Mind = get into the minds of the prospect first.**

    If you're not first in the market you can still win by stealing a piece of the prospects' brains.

    Example: "Apple's problem in getting into its prospects' minds was helped by its simple name. Apple's competitors had complicated names that were difficult to remember: Apple II, Commodore Pet, IMSAI 8080, MITS Altair 8800, and Radio Shack TRS-80. Ask yourself, which names is the simplest and easier to remember?"

4. **The Law of Perception = marketing is a battle of perception and persuasion**

    Example: "The three largest-selling Japanese imported cars in America are Honda, Toyota, and Nissan. Most marketing people think the battle between the three brands is based on quality, styling, horsepower, and price. Not true. It's what people *think* about a Honda, a Toyota, or a Nissan that determines which brand will win."

5. **The Law Of Focus = "Own a word in the customers' minds"**

    Examples:

    Crest -> cavities

    Mercedes -> engineering

    Domino's -> home delivery

    Fedex -> overnight

6. **The Law of Exclusivity = avoid being in a "red ocean".**

    Make your product unique, don't copy, don't mirror.

7. **The Law of the Ladder = understand your position in the market.**

    If you're not first don't waste your energy try to reach the top. According to Laws #1 and #3 first place is already taken by another company in the prospects' mind. Instead use Law #2.

8. **The Law of Duality = In the long run a market is split between two big players.**

    Example: Coca Cola and Pepsi, do you know who's #3, is there one?

9. **The Law of The Opposite = Position yourself as the alternative.**

    Example: "Stolichnaya was able to hang the label "fake Russian vodka" on American vodkas such as Smirnoff, Samovar, and Wolfschmidt by simply pointing out that they came from places like Hartftord (Connecticut), Schenley (Pennsylvania), and Lawrenceburg (Indiana)."

10. **The Law of Division = Markets start uniform and compact then specialize**

    Example: "The way for the leader to maintain its dominance is to address each emerging category with a different brand name, as General Motors did in the early days with Chevrolet, Pontiac, Oldsmobile, Buick, and Cadillac."

11. **The Law of Perspective = Marketing trends are long. Don't sacrifice the long-term for short-term gains.**

    Example: "Donald Trump. The Donald was successful. Then he branched out and put his name on anything the banks would lend him money for. What's a Trump? A hotel, three casinos, two condominiums, one airline, one shopping center. Today Trump is $1.4 billion in debt. What made him successful in the short term is exactly what cause him to fail in the long term"

12. **The Law of Line Extension = More is less**

    "I'd rather be strong somewhere," said a manager, "than weak everywhere."

    Examples:

    Heinz Ketchup. Heinz baby soup?

    Coors beer. Coors water?

    You probably didn't know about any of these. By violating Law #5 these companies spent enormous amount of resources on products that weren't successful.

13. **The Law of Sacrifice = Reduce your product offering.**

    Relates to Law #5 and #6. Opposite to Law #12.

14. **The Law of Attributes = get into the prospects' minds.**

    Related to Law #3

    Use opposite attributes to stand out. Own them.

15. **The Law of Candor = "Honesty is the best policy"**

    "When a company starts a message by admitting a problem, people tend to, almost instinctively, open their minds."

16. **The Law of Singularity.**

    Related to Law #5 and #13.

    "Focus on the single most important weak spot of your competitor. Be bold, don't attack on multiple fronts."

17. **The Law of Unpredictability.**
18. **The Law of Success = don't let success get into your mind.**

    "Ego is the enemy of successful marketing."

19. **The Law of Failure = no company is perfect.**

    Failures are inevitable. Accept, learn and move on.

20. **The Law of Hype = something is wrong.**

    "Hype is hype" usually signals the opposite.

    "Real revolutions don't arrive at high noon with marching bands and coverage on the 6:00 PM news. Real revolutions arrive unannounced in the middle of the night and kind of sneak up on you."

21. **The Law of Acceleration = the law of scarcity**

    "You must control the speed of growth to become a trend, not a fad."

22. **The Law of Resources = Marketing costs a lot of money.**

    Example: "It cost $9,000 a minute to fight World War II. It cost $22,000 a minute fight the Vietnam War. A one-minute commercial on the NFL Super Bowl will cost you $1.5 million."

—

### Great quotes

> "Marketing is a game of mental warfare. It's a battle of perceptions, not products or services."

> "Marketing is a battle of ideas. So if you are to succeed, you must have an idea or attribute of your own to focus your efforts around."

> "Have patience. The immutable laws of marketing will help you achieve success. And success is the best revenge of all."

