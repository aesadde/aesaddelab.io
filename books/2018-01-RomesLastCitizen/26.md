---
title:  26
created: 10/09/18
source:  2018-01-RomesLastCitizen
categories: stoicism, death, suicide
---

<!-- Front -->

...He reached down to where the pain was, beneath the stitches and into the
wound, and tore himself open.

<!-- Back -->

Cato committed suicide by stabbing himself. He was "saved" by his family but
was determined to end his life no matter how.

