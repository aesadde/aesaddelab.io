---
title:  books
created: 10/09/18
source:  2018-01-RomesLastCitizen
categories:
---

<!-- Front -->

Roman Stoicism -- Edward Vernon Arnold (1958)

Julius Caesar -- Philip Freeman (2008)

The Cambridge companion to the Stoics -- Brad Inwood (2003)

<!-- Back -->

