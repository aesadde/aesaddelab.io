---
title:  28
created: 10/09/18
source:  2018-01-RomesLastCitizen
categories:  Cato, example, politics
---

<!-- Front -->

He is the kind of man that every statesman aspires to be: physically tough,
intellectually brave, unflinchingly principled, beloved despite his warts.

<!-- Back -->

