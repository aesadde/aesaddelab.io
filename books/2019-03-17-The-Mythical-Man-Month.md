---
started: 02/14/18
ended: 03/11/19
title: The Mythical Man Month
subtitle: Essays on Software Engineering
author: Frederick P. Brooks, Jr.
publisher: Addison-Wesley
rating: 7/10
thumbnail: https://images-na.ssl-images-amazon.com/images/I/51WIpM70FEL._SX334_BO1,204,203,200_.jpg
link:
blurb: >
    A timeless book on the art and science of making software in big teams.
    This book destroys the myth of the "man-hour", the idea that adding more human power
    to a project will make it better and quicker.
---

As a Software Engineer and Manager (albeit for a short time), I am always
trying to understand how to best manage projects, how to choose the correct
tools, how to assign responsibilities to the team and ultimately how to make
the best possible decisions.

This book was a huge surprise to me. Even though it was written more than 20
years ago, most of what is written is still relevant today. It helped me
understand many issues and obstacles I've found in my own projects.
