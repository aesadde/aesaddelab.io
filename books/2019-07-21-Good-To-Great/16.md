---
title:  16
created: 07/21/19
source:  2019-07-21-Good-To-Great
categories: team, time
---

<!-- Front -->
For every minute you allow a person to continue holding a seat when you know
that person will not make it in the end, you're stealing a portion of his life,
time that he could spend finding a better place where he could flourish.
<!-- Back -->
