---
title:  04
created: 07/21/19
source:  2019-07-21-Good-To-Great
categories:
---

<!-- Front -->
"I never stopped trying to become qualified for the job."
<!-- Back -->

This is humbling. Even at the highest levels of a successful organization there
seems to be some sort of "impostor syndrome". Working hard and improving are
necessary things.
