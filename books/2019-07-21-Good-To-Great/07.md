---
title:  07
created: 07/21/19
source:  2019-07-21-Good-To-Great
categories: leadership
---

<!-- Front -->
The quiet, dogged nature of Level 5 leaders showed up not only in big decisions,
like selling off the food-service operations or fighting corporate raiders, but
also in a personal style of sheer workmanlike diligence.
<!-- Back -->
