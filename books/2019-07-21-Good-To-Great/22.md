---
title:  22
created: 07/21/19
source:  2019-07-21-Good-To-Great
categories: focus, principles
---

<!-- Front -->
...all good-to-great companies attained a very simple concept that they used as
a frame of reference for all their decisions, and this understanding coincided
with breakthrough results.
<!-- Back -->
