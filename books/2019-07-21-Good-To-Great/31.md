---
title:  31
created: 07/21/19
source:  2019-07-21-Good-To-Great
categories: discipline, consistency, patience
---

<!-- Front -->
It is only through consistency over time, through multiple generations, that you
get maximum results.
<!-- Back -->
