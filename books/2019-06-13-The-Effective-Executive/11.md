---
title:  11
created: 06/13/19
source:  2019-06-13-The-Effective-Executive
categories: effectiveness, focus
---

<!-- Front -->
efficiency...is the ability to do things right rather than the ability to get
the right things done.
<!-- Back -->
