---
title:  59
created: 06/14/19
source:  2019-06-13-The-Effective-Executive
categories: goals
---

<!-- Front -->
The needs of large-scale organization have to be satisfied by common people
achieving uncommon performance. This is what the effectibve executive has to
make himself able to do.

<!-- Back -->
