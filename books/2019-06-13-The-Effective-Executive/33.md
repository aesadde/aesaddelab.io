---
title:  33
created: 06/13/19
source:  2019-06-13-The-Effective-Executive
categories: contribution
---

<!-- Front -->
The focus on contribution is the key to effectiveness: in a man's own work --
its content, its level, its standards, and its impacts; in his relations with
others -- his superiors, his associates, his subordinates; in his use of the
tools of the executive such as meetings or reports
<!-- Back -->
