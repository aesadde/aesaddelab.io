---
title:  27
created: 06/13/19
source:  2019-06-13-The-Effective-Executive
categories: plan
---

<!-- Front -->

A well-managed plant, I soon learned, is a quiet place.

A well-managed factory is boring. Nothing exciting happens in it because the
crises have been anticipated and have been converted into routine.

<!-- Back -->
