---
started:
ended:
title: High Output Management
author: Andrew S. Grove
publisher:
rating: 7/10
thumbnail: https://images.penguinrandomhouse.com/cover/9780679762881
link: 
blurb: >
    Prime management advice from Andrew Grove, Intel's CEO. He pretty much invented
    OKRs and quarterly performance reviews. If you ever work at a big
    company, I think you should read this book.
---

This book is one of the most famous books ever written about management in the
workplace. I had read excerpts here and there and even a nice [blogpost] by Ben
Horowitz from a16z but had never taken the time to read it in full. So, as soon
as I started doing more managerial work at AiFi I picked it up and give it
a go, I wasn't disappointed.

Andre Grove is, simply put, one of the fathers of modern day management and the
creator of most of the process that successful companies have implemented over
the past 20 years, [one big example being
Google](https://rework.withgoogle.com/guides/set-goals-with-okrs/steps/introduction/).

Among the many processes and concepts that the book introduces and explains in
detail are OKRs (Objectives and Key Results.) At AiFi we started using OKRs
early on, and this book really explains what they are and how and when are they
useful. So many things became crystal clear and I've been revisiting it very
often.
