{-# LANGUAGE OverloadedStrings #-}

import Control.Monad (forM,forM_)
import Data.Char (isDigit)
import Data.List (intercalate, isInfixOf, sortBy)
import qualified Data.Map as M
import Data.Maybe (fromMaybe)
import Data.Monoid ((<>),mconcat)
import Data.Ord (comparing)
import qualified Data.Set            as S
import Hakyll
import System.FilePath (takeDirectory, (</>), takeBaseName, takeDirectory)
import           System.FilePath.Posix  (takeBaseName,takeDirectory
                                         ,(</>),splitFileName)
import GHC.IO.Encoding
       (setFileSystemEncoding, setForeignEncoding, setLocaleEncoding,
        utf8)
import System.Directory (getDirectoryContents)
import           Text.Pandoc.Options
--
--------------------------------------------------------------------------------
-- Hakyll config ---------------------------------------------------------------
--------------------------------------------------------------------------------
dontIgnoreHtaccess :: String -> Bool
dontIgnoreHtaccess ".htaccess" = False
dontIgnoreHtaccess path        = ignoreFile defaultConfiguration path

hakyllConf :: Configuration
hakyllConf =
  defaultConfiguration
    {ignoreFile = dontIgnoreHtaccess, destinationDirectory = "public", previewHost = "0.0.0.0"}

root :: String
root =
    "https://aesadde.xyz"


siteName :: String
siteName =
    "Alberto Sadde"

-------------------------------------------------------------------------------
-- Main -----------------------------------------------------------------------
-------------------------------------------------------------------------------
main :: IO ()
main = do
  setLocaleEncoding utf8
  setFileSystemEncoding utf8
  setForeignEncoding utf8
  hakyllWith hakyllConf $
-- General Rules ---------------------------------------------------------------
   do
    match "templates/*" $ compile templateCompiler
    match "partials/*" $ compile templateCompiler
    -- build all the tags from all the posts
    tags <- buildTags "posts/**" (fromCapture "tag-pages/*.html")
    tagsRules tags $ \tag pattern -> do
      let title = "Posts tagged " ++ tag
      route $ niceRoute
      compile $ do
        posts <- recentFirst =<< loadAll pattern
        let ctx =
              constField "root" root         <>
              constField "siteName" siteName <>
              constField "title" title <> constField "style" "blog.css" <>
              listField "posts" dateCtx (return posts) <>
              defaultContext
        makeItem "" >>= loadAndApplyTemplate "templates/blog.html" ctx >>=
          loadAndApplyTemplate "templates/default.html" ctx >>=
          removeIndexHtml
    match "files/*" $ do
      route idRoute
      compile copyFileCompiler
    match "files/**" $ do
      route idRoute
      compile copyFileCompiler
    match "CNAME" $ do
      route idRoute
      compile copyFileCompiler
    match "css/*" $ do
      route idRoute
      compile compressCssCompiler
    match "js/*" $ do
      route idRoute
      compile copyFileCompiler
-- Home -----------------------------------------------------------------------
    match "*.md" $ do
      route $ niceRoute
      compile $
        pandocCompiler >>=
        loadAndApplyTemplate "templates/default.html" rootCtx >>= removeIndexHtml

-- Now pages ------------------------------------------------------------------
    match "old-now/*.md" $ do
      route $ niceRoute
      compile $ do
        let oldNowCtx =
              constField "root" root         <>
              constField "siteName" siteName <>
              defaultContext
        pandocCompiler >>= saveSnapshot "content" >>=
          loadAndApplyTemplate "templates/now.html" oldNowCtx >>=
          loadAndApplyTemplate "templates/default.html" oldNowCtx >>= removeIndexHtml
    create ["old-now.html"] $ do
      route $ niceRoute
      compile $ do
        nows <- recentFirst =<< loadAll "old-now/*"
        let oldNewCtx =
              listField "posts" dateCtx (return nows) <>
              constField "title" "Old-Now" <>
              constField "style" "main.css" <>
              constField "root" root         <>
              constField "type" "article" <>
              constField "siteName" siteName <>
              defaultContext
        makeItem "" >>= loadAndApplyTemplate "templates/old-now.html" oldNewCtx >>=
          loadAndApplyTemplate "templates/default.html" oldNewCtx >>= removeIndexHtml
-- Blog -----------------------------------------------------------------------
    match "index.html" $ do
      route $ idRoute
      compile $ do
        posts <- recentFirst =<< loadAll "posts/**"
        let blogCtx =
              listField "posts" postCtx (return posts) <>
              constField "style" "posts.css" <>
              constField "root" root                   <>
              constField "siteName" siteName           <>
              field "taglist" (\_ -> renderTagList tags) <>
              defaultContext
        makeItem "" >>= loadAndApplyTemplate "templates/blog.html" blogCtx >>=
          loadAndApplyTemplate "templates/default.html" blogCtx >>= removeIndexHtml
    match "posts/**" $ do
      route $ niceRoute
      compile $
        pandocCompilerWith pandocReaderOptions pandocWriterOptions >>=
        saveSnapshot "content" >>=
        loadAndApplyTemplate "templates/post.html" postCtx >>=
        loadAndApplyTemplate "templates/default.html" postCtx >>=
        removeIndexHtml

-- Books----------------------------------------------------------------------
    let notes = fromGlob "books/**.md" .&&. complement "books/*.md"
    match notes $ do
      route $ niceRoute
      compile $
        pandocCompilerWith pandocReaderOptions pandocWriterOptions >>=
        removeIndexHtml
    let p = "books/*.md"
    match p $ do
      route $ niceRoute
      compile $
        pandocCompilerWith pandocReaderOptions pandocWriterOptions >>= removeIndexHtml >>=
        saveSnapshot "content" >>=
        loadAndApplyTemplate "templates/book.html" (defaultContext <> bookCtx) >>=
        loadAndApplyTemplate
          "templates/default.html"
          (defaultContext <> bookCtx) >>=
        removeIndexHtml
    create ["books.html"] $ do
      route niceRoute
      compile $ do
        books <- recentlyRead =<< loadAll p
        let booksCtx =
              listField "books" (defaultContext) (return books) <>
              constField "title" "Summaries and Insights from Books I've Read" <>
              constField "style" "posts.css" <>
              constField "root" root         <>
              constField "type" "article" <>
              constField "siteName" siteName <>
              defaultContext
        makeItem "" >>= loadAndApplyTemplate "templates/books.html" booksCtx >>=
          loadAndApplyTemplate "templates/default.html" booksCtx >>=
          removeIndexHtml

-- Sitemap----------------------------------------------------------------------
    create ["sitemap.xml"] $ do
            route idRoute
            compile $ do
                posts <- recentFirst =<< loadAll "posts/**"
                books <- loadAll "books/*"
                let pages = posts <> books
                    sitemapCtx =
                        constField "root" root         <>
                        constField "siteName" siteName <>
                        listField "pages" postCtx (return pages)
                makeItem ("" :: String)
                    >>= loadAndApplyTemplate "templates/sitemap.xml" sitemapCtx

-- CONTEXT
dateCtx :: Context String
dateCtx = dateField "date" "%B %e, %Y" <> defaultContext

yearCtx :: Context String
yearCtx = dateField "date" "%Y" <> defaultContext

postCtx :: Context String
postCtx =
    constField "root" root         <>
    constField "type" "article" <>
    constField "siteName" siteName <>
    dateField "date" "%Y-%m-%d"    <>
    defaultContext

-- replace url of the form foo/bar/index.html by foo/bar
removeIndexHtml :: Item String -> Compiler (Item String)
removeIndexHtml item = return $ fmap (withUrls removeIndexStr) item
  where
    removeIndexStr :: String -> String
    removeIndexStr url = case splitFileName url of
        (dir, "index.html") | isLocal dir -> dir
        _                                 -> url
        where isLocal uri = not (isInfixOf "://" uri)

bookCtx :: Context String
bookCtx =
  constField "root" root         <>
  constField "type" "article" <>
  constField "siteName" siteName <>
  constField "style" "book.css" <>
  listFieldWith "notes" defaultContext getNotesInBook

rootCtx =
  constField "root" root  <>
  constField "siteName" siteName <>
  defaultContext

--------------------------------------------------------------------------------
-- Pandoc Config ---------------------------------------------------------------
--------------------------------------------------------------------------------
myPandocExtensions :: S.Set Extension
myPandocExtensions =
  S.fromList
    [ Ext_footnotes
    , Ext_inline_notes
    , Ext_pandoc_title_block
    , Ext_table_captions
    , Ext_implicit_figures
    , Ext_simple_tables
    , Ext_multiline_tables
    , Ext_grid_tables
    , Ext_pipe_tables
    , Ext_citations
    , Ext_raw_tex
    , Ext_raw_html
    , Ext_tex_math_dollars
    , Ext_tex_math_single_backslash
    , Ext_latex_macros
    , Ext_fenced_code_blocks
    , Ext_fenced_code_attributes
    , Ext_backtick_code_blocks
    , Ext_inline_code_attributes
    , Ext_markdown_in_html_blocks
    , Ext_escaped_line_breaks
    , Ext_fancy_lists
    , Ext_startnum
    , Ext_definition_lists
    , Ext_example_lists
    , Ext_all_symbols_escapable
    , Ext_intraword_underscores
    , Ext_blank_before_blockquote
    , Ext_blank_before_header
    , Ext_strikeout
    , Ext_superscript
    , Ext_subscript
    , Ext_auto_identifiers
    , Ext_header_attributes
    , Ext_implicit_header_references
    , Ext_link_attributes
    , Ext_line_blocks
    ]

pandocWriterOptions :: WriterOptions
pandocWriterOptions =
  defaultHakyllWriterOptions
  { writerHTMLMathMethod = MathML Nothing
  , writerHtml5 = True
  , writerSectionDivs = True
  , writerReferenceLinks = True
  }

pandocReaderOptions :: ReaderOptions
pandocReaderOptions =
  defaultHakyllReaderOptions {readerExtensions = myPandocExtensions}

recentlyRead :: [Item a] -> Compiler [Item a]
recentlyRead items = do
  itemsWithDate <-
    forM items $ \item -> do
      let title = toFilePath (itemIdentifier item)
      return (title, item)
  let itemsWithDate' =
        filter
          (\(x, _) -> or $ map isDigit $ take 4 $ dropWhile (not . isDigit) x)
          itemsWithDate
  return $ reverse (map snd (sortBy (comparing fst) itemsWithDate'))

getNotesInBook :: Item a -> Compiler [Item String]
getNotesInBook (Item it _) = do
  let notesPath = (takeWhile (/= '.') $ toFilePath it) ++ "/**"
  let notes = fromGlob notesPath
  notes' <- loadAll notes
  return notes'

-- replace a foo/bar.md by foo/bar/index.html
-- this way the url looks like: foo/bar in most browsers
niceRoute :: Routes
niceRoute = customRoute createIndexRoute
  where
    createIndexRoute ident =
        takeDirectory p </> takeBaseName p </> "index.html"
      where p=toFilePath ident
