---
published: 2018-09-16
title: Jun 24, 2018 September 16, 2018
author: Alberto Sadde
style: main.css
---

<div id="now-content" class="mainContent">
<a href="./old-now.html">Previous Now Pages</a>

This is a [Now page](http://nownownow.com/about). It details what I am
currently focused and working on.  You can also check my previous [now
pages](./old-now.html)

* I am currently working as a research engineer at [AiFi](http://aifi.io/) an
  early stage startup focused on next generation retail experiences.

* I just started a new learning project to keep up with my software engineering
  and programming skills. In particular I am revisiting the book "The
  Algorithms Design Manual".

* I am also finding more time to keep this site more updated and start sharing
  more thoughts in my blog.
</div>
