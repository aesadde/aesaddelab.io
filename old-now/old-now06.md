---
published: 2019-06-28
title: September 2018 - June 2019
author: Alberto Sadde
style: main.css
---

<div id="now-content" class="mainContent">
<a href="./old-now.html">Previous Now Pages</a>

This is a [Now page](http://nownownow.com/about). It details what I am
currently focused and working on.  You can also check my previous [now
pages](./old-now.html)

* I am experimenting with Instagram. I try to share the books I read and
  passages that I find illuminating during my daily commute on the train.
  Check me out at [\@words.coffee](https://www.instagram.com/words.coffee/?hl=en).

* I just started to share my notes and highlights from books I've read recently.
  This process forces me to review the books and remember more of what I've read.
  You can read more [here](./books.html)

</div>
