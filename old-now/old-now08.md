---
title: old-now08
style: default.css
title: October 2019 - February 2020
published: 2020-02-18
author: Alberto Sadde
style: main.css
---

This section it inspired by Derek Sivers' [Now pages](http://nownownow.com/about). It details what I am
currently focused and working on. You can check my previous now pages [here.](./old-now.html)

- I am learning [AcroYoga](https://www.acroyoga.org/). So far it has been a fantastic experience. What I enjoy the most is the human contact and trust that you need to build in order to fly or base another person

- I am experimenting with a morning routine that enables me to write more and more often. You can read more [here]().
