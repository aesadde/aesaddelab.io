---
title: old-now07
style: default.css
title: June 2019 - October 2019
published: 2019-10-27
author: Alberto Sadde
style: main.css
---

<div id="now-content" class="mainContent">

This is a [Now page](http://nownownow.com/about). It details what I am
currently focused and working on. You can also check my previous [now
pages](./old-now.html)

- Currently I am experimenting with a weekly newsletter that I send with articles
  I found interesting and some details into different projects and hobbies I am
  involved in, I call this section the "nerd corner". If you would like to know
  more, sign up!

- I am trying to keep up with my [2019 reading list](./posts/2019/2019-01-20-papers-1.html)

- I have been experimenting with tracking all aspects of my life including
  weight, sleep, workouts, focused hours, etc. I will write about this soon!

<a href="./old-now.html">Previous Now Pages</a>

