---
title: Start Here
style: default.css
---

<div id="now-content" class="mainContent">

<center>
<img id="self-picture" class="sh-picture" width=500px height=auto src="/files/images/start-here.jpg">

<h2>Hi, I’m Alberto Sadde!</h2>
</center>

Welcome to my (nerd) corner of the Internet! This is my online home where you can find my [latest writings](./), [book summaries](./books) and my [weekly newsletter](./join).

<hr>
<center>
<h2> A few bullets about me</h2>
</center>

* After 9+ years living around Europe (mostly the UK) [I've settled in beautiful San Francisco](https://www.instagram.com/p/CB3MKQQH5FX).

* I work with a cool team of engineers at [Akorda](http://akorda.com/). We’re on a mission to change the way lawyers negotiate contracts.

* I play [underwater rugby](./posts/underwater-what) whenever I get the chance. Come join us!

* I volunteer at [Code For Venezuela](https://www.codeforvenezuela.org/). A non-profit with the mission to help our country through technology. Check our current [projects](https://www.codeforvenezuela.org/projects).

<hr>
<center>
<h2> Now </h2>
</center>

- My goal this year is to develop a running habit by forcing myself to run at least 2 miles 5 times per week. I am not looking for fitness but to build mental strength and beat procrastination. So far I'm on a roll. The trick seems to never take two days off in a row.

- I committed to not read any new books, books that have been published in the last decade. You can check my [2020 reading list](/posts/202001201909_2020-reading-list).

<small>
This section is inspired by Derek Sivers' [Now pages](http://nownownow.com/about). You can check my previous now pages [here.](./old-now)
</small>

<hr>
<center>
<h2> Nerd Corner 🤓 </h2>
</center>

Every week on Monday night I send a newsletter where I share three things:

1. Notes and highlights of the book I am currently reading.

2. A quick post on a "nerd" topic. Topics are very varied. Past editions range from privacy-first apps to diets, to climate change.

3. Cool Finds. These are three links of cool ideas, videos, charts or articles that I collected during the previous week.

</div>
