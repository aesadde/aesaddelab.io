#!/usr/bin/env bash

echo "Starting the web app"
stack build --fast -v
stack exec web watch -v
